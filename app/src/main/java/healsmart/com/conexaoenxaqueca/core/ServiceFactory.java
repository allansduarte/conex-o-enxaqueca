package healsmart.com.conexaoenxaqueca.core;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.Toolbar;

import healsmart.com.conexaoenxaqueca.NotificationService;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.Utils.LoginManager;
import healsmart.com.conexaoenxaqueca.Utils.NavigationDrawer;
import healsmart.com.conexaoenxaqueca.Utils.PaintDraw;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.Utils.Validate;
import healsmart.com.conexaoenxaqueca.domain.Usuario;

/**
 * Created by allan on 25/09/15.
 */
public abstract class ServiceFactory
{
    private static ServiceFactory sInstance;

    public static synchronized ServiceFactory getInstance() {
        if (sInstance == null) {
            throw new RuntimeException("ServiceFactory not initialized!");
        }
        return sInstance;
    }

    public static void iniciar() {
        //verificar necessidade de singleton
        ServiceFactory.sInstance = new Service();
    }

    public abstract Usuario getServiceUsuario();
    public abstract Validate getServiceValidate();
    public abstract LoginManager getServiceLoginManager();
    public abstract Utils getServiceUtils();
    public abstract NavigationDrawer getServiceNavigationDrawer(Activity activity, Toolbar toolbar);
    public abstract NavigationDrawer getServiceNavigationDrawer(Activity activity, Toolbar mToolbar, int drawerContainer);
    public abstract PaintDraw getServicePaintDraw(Context context);
    public abstract SurveyManager getServiceSurveyManager(Context context);
    public abstract NotificationService getNotificationService(Activity activity);
}
