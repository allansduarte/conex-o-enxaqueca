package healsmart.com.conexaoenxaqueca.core;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.Toolbar;

import healsmart.com.conexaoenxaqueca.NotificationService;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.Utils.LoginManager;
import healsmart.com.conexaoenxaqueca.Utils.NavigationDrawer;
import healsmart.com.conexaoenxaqueca.Utils.PaintDraw;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.Utils.Validate;
import healsmart.com.conexaoenxaqueca.domain.Usuario;

public class Service extends ServiceFactory
{
    private Usuario mUsuario;
    private Validate mValidate;
    private LoginManager mLoginManager;
    private Utils mUtils;
    private NavigationDrawer mNavigationDrawer;
    private PaintDraw mPaintDraw;
    private SurveyManager mSurveyManager;
    private NotificationService mNotificationService;

    @Override
    public Usuario getServiceUsuario(){
        if (mUsuario == null) {
            mUsuario = new Usuario();
        }
        return mUsuario;
    }

    @Override
    public Validate getServiceValidate(){
        if (mValidate == null) {
            mValidate = new Validate();
        }
        return mValidate;
    }

    @Override
    public LoginManager getServiceLoginManager(){
        if (mLoginManager == null) {
            mLoginManager = new LoginManager();
        }
        return mLoginManager;
    }

    @Override
    public Utils getServiceUtils(){
        if (mUtils == null) {
            mUtils = new Utils();
        }
        return mUtils;
    }

    @Override
    public NavigationDrawer getServiceNavigationDrawer(Activity activity, Toolbar toolbar){
        if (mNavigationDrawer == null) {
            mNavigationDrawer = new NavigationDrawer(activity, toolbar);
        }
        return mNavigationDrawer;
    }

    @Override
    public NavigationDrawer getServiceNavigationDrawer(Activity activity, Toolbar toolbar, int drawerContainer) {
        if (mNavigationDrawer == null) {
            mNavigationDrawer = new NavigationDrawer(activity, toolbar, drawerContainer);
        }
        return mNavigationDrawer;
    }

    @Override
    public PaintDraw getServicePaintDraw(Context context) {
        if (mPaintDraw == null) {
            mPaintDraw = new PaintDraw(context);
        }
        return mPaintDraw;
    }

    @Override
    public SurveyManager getServiceSurveyManager(Context context) {
        if (mSurveyManager == null) {
            mSurveyManager = new SurveyManager(context);
        }
        return mSurveyManager;
    }

    @Override
    public NotificationService getNotificationService(Activity activity) {
        if (mNotificationService == null) {
            mNotificationService = new NotificationService(activity);
        }
        return mNotificationService;
    }
}
