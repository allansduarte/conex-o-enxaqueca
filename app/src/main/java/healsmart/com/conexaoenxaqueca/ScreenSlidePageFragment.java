/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package healsmart.com.conexaoenxaqueca;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


public class ScreenSlidePageFragment extends Fragment {
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static ScreenSlidePageFragment create(int pageNumber) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);

        return fragment;
    }

    public ScreenSlidePageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_screen_slide_page, container, false);

        final String tituloAbertura = getResources().getStringArray(R.array.abertura_titulo)[mPageNumber];
        final String textoAbertura = getResources().getStringArray(R.array.abertura_texto)[mPageNumber];

        ((TextView) rootView.findViewById(R.id.tituloAbertura)).setText(tituloAbertura);
        ((TextView) rootView.findViewById(R.id.textoAbertura)).setText(textoAbertura);

        LinearLayout indicadoresContent = (LinearLayout) rootView.findViewById(R.id.indicadoresContent);
        for(int i=0; i < indicadoresContent.getChildCount(); i++)
        {
            if(mPageNumber == i)
                indicadoresContent.getChildAt(i).setSelected(true);
            else
                indicadoresContent.getChildAt(i).setSelected(false);
        }

        //Adiciona imagem para background
        int imagemDrawable = 0;
        switch (mPageNumber){

            case 0:
                imagemDrawable = R.drawable.previna_se;
                break;
            case 1:
                imagemDrawable = R.drawable.conexao_medico_paciente;
                break;
            case 2:
                imagemDrawable = R.drawable.entenda_dor_cabeca;
                break;
        }

        rootView.findViewById(R.id.containerRelativeLayout).setBackgroundResource(imagemDrawable);


        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
