package healsmart.com.conexaoenxaqueca;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by allan on 05/01/16.
 */
public class NotificationService {

    private Activity mActivity;

    private int mNotificationId;

    private PendingIntent mPendingIntent;

    public NotificationService(Activity activity) {
        mActivity = activity;
    }

    private NotificationCompat.Builder generateNotificationCrisisInProgress() {
        Context context = mActivity.getApplicationContext();
        Resources resources = context.getResources();

        NotificationCompat.Builder notificatioinBuilder = new NotificationCompat.Builder(context);
        notificatioinBuilder.setTicker(resources.getString(R.string.titulo_crise_andamento));
        notificatioinBuilder.setContentTitle(resources.getString(R.string.titulo_crise_andamento));
        notificatioinBuilder.setContentText(resources.getString(R.string.descricao_crise_andamento));
        notificatioinBuilder.setSmallIcon(R.drawable.ic_stat_bolt_white);
        notificatioinBuilder.setColor(ContextCompat.getColor(context, R.color.laranja));
        notificatioinBuilder.setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher));
        notificatioinBuilder.setAutoCancel(false);
        notificatioinBuilder.setOngoing(true);
        notificatioinBuilder.setContentIntent(getPendingIntent());

        return notificatioinBuilder;
    }

    public void showNotificationCrisisInProgress() {
        Notification n = generateNotificationCrisisInProgress().build();

        NotificationManager nm = (NotificationManager) mActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(getNotificationId(), n);
    }

    public void cancelNotificationCrisisInProgress() {
        NotificationManager nm = (NotificationManager) mActivity.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(getNotificationId());
    }

    public int getNotificationId() {
        return mNotificationId;
    }

    public void setNotificationId(int mNotificationId) {
        this.mNotificationId = mNotificationId;
    }

    public PendingIntent getPendingIntent() {
        return mPendingIntent;
    }

    public void setPendingIntent(PendingIntent mPendingIntent) {
        this.mPendingIntent = mPendingIntent;
    }
}
