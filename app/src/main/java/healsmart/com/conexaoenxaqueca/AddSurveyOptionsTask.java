package healsmart.com.conexaoenxaqueca;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import healsmart.com.conexaoenxaqueca.domain.Bebida;
import healsmart.com.conexaoenxaqueca.domain.Comida;
import healsmart.com.conexaoenxaqueca.domain.Consequencia;
import healsmart.com.conexaoenxaqueca.domain.EsforcoFisico;
import healsmart.com.conexaoenxaqueca.domain.Gatilho;
import healsmart.com.conexaoenxaqueca.domain.LocaisDeDor;
import healsmart.com.conexaoenxaqueca.domain.Local;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivio;
import healsmart.com.conexaoenxaqueca.domain.QualidadeDor;
import healsmart.com.conexaoenxaqueca.domain.Sintoma;
import healsmart.com.conexaoenxaqueca.interfaces.BebidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.ComidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.ConsequenciaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.EsforcoFisicoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.GatilhoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.LocaisDeDorEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.LocalEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.MetodoAlivioEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.QualidadeDorEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SintomaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import io.realm.Realm;

public class AddSurveyOptionsTask extends AsyncTask<Object, Object, Object> implements SurveyConstants, PerfilUsuario
{
    private int mType;
    private Context mContext;

    public AddSurveyOptionsTask(Context context, int type)
    {
        mType = type;
        mContext = context;
    }

    @Override
    protected Object doInBackground(Object... params)
    {
        Realm realm = Realm.getDefaultInstance();

        SharedPreferences preferences = mContext.getSharedPreferences(USUARIO_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        switch (mType)
        {
            case QUESTION_LOCAIS_DOR_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        LocaisDeDor locaisDeDor = (LocaisDeDor) param;

                        LocaisDeDor opcao = realm.where(LocaisDeDor.class).equalTo(LocaisDeDorEntidade.ID, locaisDeDor.getId())
                                                 .findFirst();

                        if (opcao == null)
                            realm.copyToRealmOrUpdate(locaisDeDor);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(LOCAIS_DOR_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_QUALIDADE_DOR_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        QualidadeDor qualidadeDor = (QualidadeDor) param;

                        QualidadeDor opcao = realm.where(QualidadeDor.class).equalTo(QualidadeDorEntidade.ID, qualidadeDor.getId()).findFirst();
                        if (opcao != null) {
                            qualidadeDor = opcao;
                            qualidadeDor.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(qualidadeDor);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(SINTOMAS_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_SINTOMAS_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        Sintoma sintoma = (Sintoma) param;

                        Sintoma opcao = realm.where(Sintoma.class).equalTo(SintomaEntidade.ID, sintoma.getId()).findFirst();
                        if (opcao  != null){
                            sintoma = opcao;
                            sintoma.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(sintoma);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(QUALIDADE_DOR_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_GATILHOS_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        Gatilho gatilho = (Gatilho) param;

                        Gatilho opcao = realm.where(Gatilho.class).equalTo(GatilhoEntidade.ID, gatilho.getId()).findFirst();
                        if (opcao  != null){
                            gatilho = opcao;
                            gatilho.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(gatilho);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(GATILHO_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_GATILHOS_COMIDA_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        Comida comida = (Comida) param;

                        Comida opcao = realm.where(Comida.class).equalTo(ComidaEntidade.ID, comida.getId()).findFirst();
                        if (opcao  != null){
                            comida = opcao;
                            comida.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(comida);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(GATILHO_COMIDA_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_GATILHOS_BEBIDA_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        Bebida bebida = (Bebida) param;

                        Bebida opcao = realm.where(Bebida.class).equalTo(BebidaEntidade.ID, bebida.getId()).findFirst();
                        if (opcao  != null){
                            bebida = opcao;
                            bebida.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(bebida);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(GATILHO_BEBIDA_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_GATILHOS_ESFORCO_FISICO_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        EsforcoFisico esforcoFisico = (EsforcoFisico) param;

                        EsforcoFisico opcao = realm.where(EsforcoFisico.class).equalTo(EsforcoFisicoEntidade.ID, esforcoFisico.getId()).findFirst();
                        if (opcao  != null){
                            esforcoFisico = opcao;
                            esforcoFisico.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(esforcoFisico);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(GATILHO_ESFORCO_FISICO_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_METODO_ALIVIO_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        MetodoAlivio metodo = (MetodoAlivio) param;

                        MetodoAlivio opcao = realm.where(MetodoAlivio.class)
                                                  .equalTo(MetodoAlivioEntidade.ID, metodo.getId())
                                                  .findFirst();
                        if (opcao  != null)
                        {
                            metodo = opcao;
                            metodo.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(metodo);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(METODO_ALIVIO_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_CONSEQUENCIAS_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        Consequencia consequencia = (Consequencia) param;

                        Consequencia opcao = realm.where(Consequencia.class)
                                .equalTo(ConsequenciaEntidade.ID, consequencia.getId())
                                .findFirst();
                        if (opcao  != null){
                            consequencia = opcao;
                            consequencia.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(consequencia);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(CONSEQUENCIA_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;

            case QUESTION_LOCAL_ID:

                try {
                    realm.beginTransaction();

                    for (Object param : params) {
                        Local local = (Local) param;

                        Local opcao = realm.where(Local.class)
                                .equalTo(LocalEntidade.ID, local.getId())
                                .findFirst();
                        if (opcao  != null){
                            local = opcao;
                            local.setDisplay(QUESTION_ACTIVE);
                        }

                        realm.copyToRealmOrUpdate(local);
                    }

                    realm.commitTransaction();

                    editor.putBoolean(LOCAL_OPTIONS_ADDED_TAG, true);
                    editor.apply();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
                break;
        }

        realm.refresh();
        realm.close();

        return null;
    }

    @Override
    protected void onPostExecute(Object result)
    {

        //String msg = "Nome: "+result.getNome();
        //Log.v(TAG, result.getId());

        //Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();

        //new SurveySaveTask(mContext, 3).execute(mCriseId, result);
    }
}
