package healsmart.com.conexaoenxaqueca.widget;

import healsmart.com.conexaoenxaqueca.fragments.LocalizacaoDorFragment;

/**
 * Created by allan on 15/12/15.
 */
public class OnClickLocalizacaoDorWizard implements IClickImageArea {
    final LocalizacaoDorFragment localizacaoDorFragment;

    public OnClickLocalizacaoDorWizard(LocalizacaoDorFragment mainActivity) {
        this.localizacaoDorFragment = mainActivity;
    }

    @Override
    public void onClickArea(int areaID, boolean action) {
        if(action) {
            this.localizacaoDorFragment.addPain(areaID);
        } else {
            this.localizacaoDorFragment.removePain(areaID);
        }
    }
}
