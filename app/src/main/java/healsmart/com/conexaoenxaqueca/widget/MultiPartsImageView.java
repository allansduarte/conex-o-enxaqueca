package healsmart.com.conexaoenxaqueca.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by allan on 14/12/15.
 */
public class MultiPartsImageView extends PhotoView implements PhotoViewAttacher.OnPhotoTapListener {
    private static final Drawable mDrawableMap;
    private LayerDrawable mLayerDrawable;
    private Bitmap mBitmap;
    private int[] mImages;
    private boolean mEnabledTouch;
    private IClickImageArea IOnClickArea;

    static {
        mDrawableMap = new ColorDrawable(0);
    }

    public MultiPartsImageView(Context context) {
        super(context);
        this.mEnabledTouch = true;
    }

    public MultiPartsImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mEnabledTouch = true;
    }

    public Integer[] getSelectedPartsIndexes() {
        List arrayList = new ArrayList(mImages.length);
        for (int i = 0; i < mImages.length; i++) {
            if (this.mLayerDrawable.getDrawable(i + 1) != mDrawableMap) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        return (Integer[]) arrayList.toArray(new Integer[arrayList.size()]);
    }

    public Integer[] getUnselectedPartsIndexes() {
        List arrayList = new ArrayList(mImages.length);
        for (int i = 0; i < mImages.length; i++) {
            if (this.mLayerDrawable.getDrawable(i + 1) == mDrawableMap) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        return (Integer[]) arrayList.toArray(new Integer[arrayList.size()]);
    }

    private Integer[] getParts(Integer[] numArr) {
        Integer[] numArr2 = (Integer[]) numArr.clone();
        for (int i = 0; i < numArr2.length; i++) {
            numArr2[i] = Integer.valueOf(mImages[numArr2[i].intValue()]);
        }
        return numArr2;
    }

    public Integer[] getSelectedParts() {
        return getParts(getSelectedPartsIndexes());
    }

    public Integer[] getUnselectedParts() {
        return getParts(getUnselectedPartsIndexes());
    }

    public void build(int drawableInitial, int drawableMap, int[] images) {
        mBitmap = ((BitmapDrawable) ContextCompat.getDrawable(getContext(), drawableMap)).getBitmap();
        mImages = (int[]) images.clone();
        Drawable[] drawableArr = new Drawable[(images.length + 1)];
        drawableArr[0] = ContextCompat.getDrawable(getContext(), drawableInitial);
        Arrays.fill(drawableArr, 1, drawableArr.length, mDrawableMap);
        mLayerDrawable = new LayerDrawable(drawableArr);
        for (int i3 = 1; i3 <= images.length; i3++) {
            mLayerDrawable.setId(i3, i3);
        }
        setImageDrawable(mLayerDrawable);
        if (this.mEnabledTouch) {
            setOnPhotoTapListener(this);
        }
    }

    public void initialize(int drawableInitial, int drawableMap, int[] images, int[] imagesSelecteds) {
        build(drawableInitial, drawableMap, images);
        for (int drawable : imagesSelecteds) {
            setSelectArea(drawable, true);
        }
    }

    public void setSelectArea(int layerDrawableId, boolean action) {
        Drawable drawable;
        if (mLayerDrawable.getDrawable(layerDrawableId) != mDrawableMap) {
            if (action) {
                return;
            }
        } else if (!action) {
            return;
        } /*else if(layerId > LocaisDeDor.quantidade) {
            return;
        }*/
        if (action) {
            drawable = ContextCompat.getDrawable(getContext(), mImages[layerDrawableId -1]);
            drawable.setBounds(mLayerDrawable.findDrawableByLayerId(layerDrawableId).getBounds());
        } else {
            drawable = mDrawableMap;
        }
        mLayerDrawable.setDrawableByLayerId(layerDrawableId, drawable);
        mLayerDrawable.invalidateDrawable(drawable);
        if (this.IOnClickArea != null) {
            this.IOnClickArea.onClickArea(layerDrawableId, action);
        }
    }

    @Override
    public void onPhotoTap(View view, float x, float y) {

        Bitmap bitmap = mBitmap.copy(Bitmap.Config.ARGB_8888, true);

        int pixel = (bitmap.getPixel((int) (((float) bitmap.getWidth()) * x), (int) (((float) bitmap.getHeight()) * y)) << 8) >> 8;
        if (pixel > 0 && pixel < mLayerDrawable.getNumberOfLayers()) {
            boolean action;
            if (mLayerDrawable.getDrawable(pixel) != mDrawableMap) {
                action = false;
            } else {
                action = true;
            }
            setSelectArea(pixel, action);
        }
    }

    public void setPartSelectionChangedListener(IClickImageArea iClickImageArea) {
        this.IOnClickArea = iClickImageArea;
    }

    protected void onDetachedFromWindow() {
        mBitmap = null;
        mLayerDrawable = null;
        super.onDetachedFromWindow();
    }

    public void setTouchSelectionEnabled(boolean enabled) {
        mEnabledTouch = enabled;
        if (enabled) {
            setOnPhotoTapListener(this);
        } else {
            setOnPhotoTapListener(null);
        }
    }
}
