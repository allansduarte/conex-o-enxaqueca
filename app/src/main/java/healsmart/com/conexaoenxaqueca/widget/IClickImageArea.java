package healsmart.com.conexaoenxaqueca.widget;

/**
 * Created by allan on 15/12/15.
 */
public interface IClickImageArea {
    void onClickArea(int areaID, boolean action);
}
