package healsmart.com.conexaoenxaqueca.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import healsmart.com.conexaoenxaqueca.R;

import static android.graphics.Bitmap.createBitmap;
import static android.graphics.BitmapFactory.decodeResource;

public class HeadCustomView extends View
{
    private Paint mPaint;
    private Context mContext;

    public HeadCustomView(Context context)
    {
        super(context);

        mContext = context;
    }

    public HeadCustomView(Context context, AttributeSet attributeSet)
    {
        super(context, attributeSet);

        mContext = context;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPaint=new Paint();

        Bitmap b;
        b = createBitmap(decodeResource(getResources(), R.drawable.ic_cabeca));

        mPaint.setColor(mContext.getResources().getColor( R.color.verdeClaro));

        float dps = 65;
        float pxs = dps * mContext.getResources().getDisplayMetrics().density;

        float left =  getWidth()/2 - b.getWidth()/2;
        float top = getHeight()/2 - pxs;

        canvas.drawBitmap(b, left, top, mPaint);
    }
}
