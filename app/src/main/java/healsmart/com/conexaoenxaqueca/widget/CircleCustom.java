package healsmart.com.conexaoenxaqueca.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import healsmart.com.conexaoenxaqueca.R;

public class CircleCustom extends View {
    RectF rectF;
    Path path;
    private Paint paint;
    private Context context;

    public CircleCustom(Context context) {
        super(context);
        this.paint = new Paint(1);
        this.rectF = new RectF();
        this.path = new Path();
        setContext(context);
    }

    public CircleCustom(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.paint = new Paint(1);
        this.rectF = new RectF();
        this.path = new Path();
        setContext(context);
    }

    public CircleCustom(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.paint = new Paint(1);
        this.rectF = new RectF();
        this.path = new Path();
        setContext(context);
    }

    private void setContext(Context context) {
        this.context = context;
    }

    protected void onDraw(Canvas canvas) {
        float f;
        float width = (float) getWidth();
        float height = (float) getHeight();
        if (width > height) {
            f = height / 2.2f;
        } else {
            f = width / 2.2f;
        }
        this.paint.setColor(this.context.getResources().getColor(R.color.verdeEscuro));
        this.paint.setStrokeWidth(5.0f);
        this.paint.setStyle(Paint.Style.STROKE);
        width *= 0.5f;
        height *= 0.5f;
        this.rectF.set(width - f, height - f, width + f, f + height);
        this.path.reset();
        this.path.addArc(this.rectF, 0.0f, 360.0f);
        canvas.drawPath(this.path, this.paint);
    }
}