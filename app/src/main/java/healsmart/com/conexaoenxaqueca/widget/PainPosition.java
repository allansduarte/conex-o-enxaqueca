package healsmart.com.conexaoenxaqueca.widget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by allan on 14/12/15.
 */
public enum PainPosition {
    RIGHT_HEAD,
    LEFT_HEAD,
    RIGHT_TEMPLE,
    RIGHT_EYE,
    BETWEEN_EYES,
    LEFT_EYE,
    LEFT_TEMPLE,
    RIGHT_CHEEK,
    LEFT_CHEEK,
    RIGHT_JAW,
    TEETH,
    LEFT_JAW,
    TOP_LEFT_BACK_HEAD,
    TOP_RIGHT_BACK_HEAD,
    LEFT_BACK_HEAD,
    RIGHT_BACK_HEAD,
    LEFT_NECK,
    RIGHT_NECK,
    DEZENOVE,
    VINTE;

    private static final String BUNDLE_NAME_PAIN_POSITION = "conexaoenxaqueca.com.healsmart.conexoenxaqueca.widget.PainLocation";

    public static List<String> getLocalizedNames(Collection<PainPosition> collection, Locale locale) {
        if (collection == null) {
            return new ArrayList();
        }
        List<String> arrayList = new ArrayList(collection.size());
        ResourceBundle bundle = ResourceBundle.getBundle(BUNDLE_NAME_PAIN_POSITION, locale);
        for (PainPosition painPosition : collection) {
            if (bundle.containsKey(painPosition.name())) {
                arrayList.add(bundle.getString(painPosition.name()));
            } else {
                arrayList.add(painPosition.name());
            }
        }
        return arrayList;
    }
}

