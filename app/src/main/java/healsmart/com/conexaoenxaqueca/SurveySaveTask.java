package healsmart.com.conexaoenxaqueca;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;

public class SurveySaveTask extends AsyncTask<Object, Object, Object> implements SurveyConstants
{
    private Context mContext;
    private double mIdQuestion;
    private ProgressDialog progressDialog;

    public SurveySaveTask(Context context, double idQuestion)
    {
        mContext = context;
        mIdQuestion = idQuestion;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setTitle("Aguarde");
        progressDialog.setMessage("Registrando dados");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

    }

    @Override
    protected Object doInBackground(Object... params) {

        if(mIdQuestion == QUESTION_DURACAO_DOR_ID || mIdQuestion == QUESTION_INTENSIDADE_ID)
        {
            //nada
        }
        if(mIdQuestion == QUESTION_MEDICAMENTO_ID)
        {

        }
        if(mIdQuestion == QUESTION_LOCAIS_DOR_ID)//localização
        {

        }
        if(mIdQuestion == QUESTION_QUALIDADE_DOR_ID)
        {

        }

        if(mIdQuestion == QUESTION_SINTOMAS_ID)
        {

        }

        return null;
    }

    @Override
    protected void onPostExecute(Object result)
    {
        progressDialog.dismiss();
        progressDialog=null;
    }

    //private void build
}
