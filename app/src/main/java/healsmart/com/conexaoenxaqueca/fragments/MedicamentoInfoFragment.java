package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.adapters.SpacesItemDecoration;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptions;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptionsAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.MedicamentoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.RecyclerViewOnClickListenerHack;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import io.realm.Realm;
import io.realm.RealmList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MedicamentoInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MedicamentoInfoFragment extends Fragment
        implements RecyclerViewOnClickListenerHack,
        SurveyConstants,
        PerfilUsuario,
        LoaderManager.LoaderCallbacks<Object>{

    private static final int LOADER_ID = 3;
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "TOMOU ALGUM MEDICAMENTO?";
    private static final String TITLE_QUESTION_INDICATOR = "medicamento";

    private View mViewFragment;
    private SurveyOptionsAdapter mAdapter;
    private RecyclerView mContentRecyclerView;
    private List<SurveyOptions> mMedicamentoOptions;
    private List<SurveyOptions> mOptions;

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private Resources mResources;
    private SurveyManager mSurveyManager;
    private Realm realm;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment MedicamentoInfoFragment.
     */
    public static MedicamentoInfoFragment newInstance(SurveyManager surveyManager) {
        MedicamentoInfoFragment fragment = new MedicamentoInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);
        fragment.setArguments(args);
        return fragment;
    }

    public MedicamentoInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mViewFragment = inflater.inflate(R.layout.fragment_medicamento_info, container, false);
        initViews();

        mMedicamentoOptions = new ArrayList<>();
        mResources = getActivity().getResources();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return mViewFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
    }

    private void initViews()
    {
        mContentRecyclerView = (RecyclerView) mViewFragment.findViewById(R.id.content_recycler_view);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mContentRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        mContentRecyclerView.setLayoutManager(gridLayoutManager);

        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration();
        spacesItemDecoration.setBottom(20);
        mContentRecyclerView.addItemDecoration(spacesItemDecoration);

        mSurveyIndicatorTextView.setText(TITLE_QUESTION_INDICATOR);
        mSurveyIndicatorTextView.setVisibility(View.VISIBLE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);

        if(mSurveyManager.isUpdateMode())
        {
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
            mNextButton.setVisibility(View.GONE);
        } else {
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
            mNextButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mAdapter = new SurveyOptionsAdapter(getActivity().getApplicationContext(), new ArrayList<SurveyOptions>());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mAdapter = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mSurveyTitleTextView.setBackgroundColor(mResources.getColor(R.color.verdeClaro));
        mSurveyManager.setMode(MODE_INSERT);
        getLoaderManager().destroyLoader(LOADER_ID);
    }

    public void onClickAddMedicamento(View v, final int position) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_medicamento, null);

        final Dialog dialog = new Dialog(getActivity());
        Button confirmButton = (Button) dialogView.findViewById(R.id.confirmButton);
        Button cancelButton = (Button) dialogView.findViewById(R.id.cancelButton);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateDialogAddMedicamento(dialogView))
                {
                    EditText nomeMedicamentoEditText = (EditText) dialogView.findViewById(R.id.nomeMedicamentoEditText);
                    EditText qtdMedicamentoEditText = (EditText) dialogView.findViewById(R.id.qtdMedicamentoEditText);

                    String nomeMedicamento = nomeMedicamentoEditText.getText().toString();

                    long nrMedicamento = realm.where(Crise.class)
                            .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                            .equalTo(CriseEntidade.MEDICAMENTOS+"."+ MedicamentoEntidade.NOME, nomeMedicamento)
                            .count();

                    if(nrMedicamento > 0){
                        nomeMedicamentoEditText.requestFocus();
                        nomeMedicamentoEditText.setError("O nome deste medicamento já foi adicionado.");
                    } else{
                        SurveyOptions surveyOptions = new SurveyOptions(nomeMedicamentoEditText.getText().toString(), Integer.parseInt(qtdMedicamentoEditText.getText().toString()), R.drawable.ic_frasco);
                        mAdapter.addListItem(surveyOptions, position);//mOptions add

                        mMedicamentoOptions.add(surveyOptions);

                        Medicamento medicamento = new Medicamento();
                        medicamento.setId(ServiceFactory.getInstance().getServiceUtils().generateHash(getActivity().getApplicationContext()));
                        medicamento.setNome(nomeMedicamento);
                        medicamento.setQuantidade(Integer.parseInt(qtdMedicamentoEditText.getText().toString()));
                        medicamento.setFuncionou(false);

                        Crise crise = realm.where(Crise.class).equalTo("id", mSurveyManager.getCriseId()).findFirst();
                        try{
                            realm.beginTransaction();

                            crise.getMedicamentos().add(medicamento);

                            realm.copyToRealmOrUpdate(crise);
                            realm.commitTransaction();
                        }
                        catch(Exception e){
                            realm.cancelTransaction();
                        }
                        realm.refresh();

                        dialog.dismiss();
                    }
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();
    }

    public List<SurveyOptions> buildOptions()
    {
        List<SurveyOptions> listAux = new ArrayList<>();

        if(mMedicamentoOptions != null)
        {
            for(int i = 0; i < mMedicamentoOptions.size(); i++)
                listAux.add(mMedicamentoOptions.get(i));
        }

        listAux.add(new SurveyOptions("Adicionar medicamento", R.drawable.ic_add_option));
        listAux.add(new SurveyOptions("Excluir medicamento", R.drawable.null_option));

        return listAux;
    }

    @Override
    public void onClickListener(View view, int position) {
        int totalItens = mAdapter.getItemCount();
        int deleteBtn = totalItens-1;
        int addBtnPosition = deleteBtn-1;

        if(position == addBtnPosition)
            onClickAddMedicamento(view, 0);
        else if(position == deleteBtn)
            mSurveyManager.setMode(ServiceFactory.getInstance().getServiceSurveyManager(getActivity().getApplicationContext()).alterarEstado(mSurveyManager.getMode(), "CLIQUE NO ÍCONE DE MEDICAMENTO PARA EXCLUIR.", TITLE_QUESTION, mSurveyTitleTextView, view));
        else if(mSurveyManager.getMode() == MODE_DELETE)
            excluirMedicamento(mOptions.get(position).getTitleOption(), position);
    }

    private boolean excluirMedicamento(String nomeMedicamento, int position)
    {
        Crise crise = realm.where(Crise.class).equalTo("id", mSurveyManager.getCriseId()).findFirst();
        try{
            realm.beginTransaction();

            for(int i=0; i < crise.getMedicamentos().size(); i++) {
                Medicamento m = crise.getMedicamentos().get(i);

                if(m.getNome().equals(nomeMedicamento))
                    m.removeFromRealm();
            }

            realm.commitTransaction();
        }
        catch(Exception e){
            realm.cancelTransaction();
        }
        realm.refresh();


        mAdapter.removeListItem(position);
        mOptions = mAdapter.getItens();

        return true;
    }

    private boolean validateDialogAddMedicamento(View v) {

        EditText nomeMedicamentoEditText = (EditText) v.findViewById(R.id.nomeMedicamentoEditText);
        EditText qtdMedicamentoEditText = (EditText) v.findViewById(R.id.qtdMedicamentoEditText);

        return ( !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(nomeMedicamentoEditText, mResources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().hasSizeValid(nomeMedicamentoEditText, 3, "Medicamento deve conter no mínimo 3 caracteres.")
                && ServiceFactory.getInstance().getServiceValidate().maxSizeValid(nomeMedicamentoEditText, 20, "Medicamento deve conter no máximo 20 caracteres.")
                && !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(qtdMedicamentoEditText, mResources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().maxSizeValid(qtdMedicamentoEditText, 2, "Máximo de 2 caracteres."));
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_MEDICAMENTO_ID, mSurveyManager.getCriseId());
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data)
    {

        Crise crise = (Crise) data;

        RealmList<Medicamento> medicamentos = crise.getMedicamentos();

        if(medicamentos != null)
        {
            int drawable = R.drawable.ic_frasco;
            for(Medicamento m : medicamentos)
            {
                SurveyOptions surveyOption = new SurveyOptions(m.getNome(), m.getQuantidade(), drawable);
                mMedicamentoOptions.add(surveyOption);
            }
        }

        mOptions = buildOptions();
        mAdapter.setItens(mOptions);

        mAdapter.setRecyclerViewOnClickListenerHack(this);
        mContentRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {
        mAdapter.setItens(new ArrayList<SurveyOptions>());
    }
}
