package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.adapters.SpacesItemDecoration;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptions;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptionsAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivio;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivioCrise;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.RecyclerViewOnClickListenerHack;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MetodosAlivioFuncionouFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MetodosAlivioFuncionouFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MetodosAlivioFuncionouFragment extends Fragment
        implements RecyclerViewOnClickListenerHack,
        PerfilUsuario,
        SurveyConstants,
        LoaderManager.LoaderCallbacks<Object>
{
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private final static String TITLE_QUESTION = "ALGUNS DOS MÉTODOS ABAIXO ALIVIARAM SUA DOR?";
    private final static String TITLE_QUESTION_INDICATOR = "métodos de alívio - funcionaram";
    private static final int LOADER_ID = 9;

    private View mViewFragment;
    private RecyclerView mContentRecyclerView;
    private List<SurveyOptions> mOptions;
    private List<String> mOptionsSelected = new ArrayList<>();
    private List<String> mOptionsMedicamentoSelected = new ArrayList<>();
    private List<String> mOptionsMetodoAlivioSelected = new ArrayList<>();

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private Realm realm;

    private SurveyOptionsAdapter mAdapter;
    private SurveyManager mSurveyManager;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment MetodosAlivioFuncionouFragment.
     */
    public static MetodosAlivioFuncionouFragment newInstance(SurveyManager surveyManager) {
        MetodosAlivioFuncionouFragment fragment = new MetodosAlivioFuncionouFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public MetodosAlivioFuncionouFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mViewFragment = inflater.inflate(R.layout.fragment_metodos_alivio_funcionou, container, false);
        initViews();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return mViewFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
        realm.refresh();
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getLoaderManager().destroyLoader(LOADER_ID);
    }

    private void initViews()
    {
        mContentRecyclerView = (RecyclerView) mViewFragment.findViewById(R.id.content_recycler_view);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mContentRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        mContentRecyclerView.setLayoutManager(gridLayoutManager);

        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration();
        spacesItemDecoration.setBottom(20);
        mContentRecyclerView.addItemDecoration(spacesItemDecoration);

        mSurveyIndicatorTextView.setText(TITLE_QUESTION_INDICATOR);
        mSurveyIndicatorTextView.setVisibility(View.VISIBLE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);

        if(mSurveyManager.isUpdateMode())
        {
            mNextButton.setVisibility(View.GONE);
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
        } else {
            mNextButton.setVisibility(View.VISIBLE);
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
        }
    }

    public void onChangeSelectedOptions(List<String> selectedMedicamentoOptions, List<String> selectedMetodoAlivioOptions) {
        if (mListener != null)
            mListener.onMetodoAlivioFuncionouOptionsUpdated(selectedMedicamentoOptions, selectedMetodoAlivioOptions);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mAdapter = new SurveyOptionsAdapter(getActivity().getApplicationContext(), new ArrayList<SurveyOptions>());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mAdapter = null;
        mOptionsSelected=null;
    }

    public List<SurveyOptions> buildOptions(List<Medicamento> medicamentoItens, List<MetodoAlivioCrise> metodoAlivioCriseItens){
        List<SurveyOptions> opcoes = new ArrayList<>();
        String itemTitulo;
        int itemDrawable;
        String itemId;

        int sizeMedicamento = (medicamentoItens == null ? 0 : medicamentoItens.size());
        if(sizeMedicamento > 0)
        {
            for(Medicamento item : medicamentoItens)
            {
                itemId = item.getId();
                itemTitulo = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(item.getNome());
                itemDrawable = R.drawable.ic_frasco_80;

                SurveyOptions surveyOptions = new SurveyOptions( itemTitulo, itemDrawable, itemId, TYPE_MEDICAMENTO );
                surveyOptions.setQuantidadeMedicamentoOption(item.getQuantidade());
                opcoes.add(surveyOptions);
            }
        }

        int sizeMetodoAlivio = (metodoAlivioCriseItens == null ? 0 : metodoAlivioCriseItens.size());
        if(sizeMetodoAlivio > 0)
        {
            for(int i = 0; i < sizeMetodoAlivio; i++)
            {
                MetodoAlivio item = metodoAlivioCriseItens.get(i).getMetodoAlivio();

                itemDrawable = getDrawable(item.getNome());

                if(itemDrawable == 0)
                    itemDrawable = R.drawable.option_pattern;

                itemId = item.getId();
                itemTitulo = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(item.getNome());

                SurveyOptions surveyOptions = new SurveyOptions( itemTitulo, itemDrawable, itemId, TYPE_METODO_ALIVIO );
                opcoes.add(surveyOptions);
            }
        }

        return opcoes;
    }

    private int getDrawable(String titulo)
    {
        int drawable = 0;

        if(titulo.equals("repouso"))
            drawable = R.drawable.metodo_alivio_repouso;
        else if(titulo.equals("relaxamento"))
            drawable = R.drawable.metodo_alivio_relaxamento;
        else if(titulo.equals("quarto escuro"))
            drawable = R.drawable.metodo_alivio_quarto_escuro;
        else if(titulo.equals("massagem"))
            drawable = R.drawable.metodo_alivio_massagem;
        else if(titulo.equals("banho"))
            drawable = R.drawable.metodo_alivio_banho;
        else if(titulo.equals("compressas"))
            drawable = R.drawable.metodo_alivio_compressa;

        return drawable;
    }

    @Override
    public void onClickListener(View view, int position)
    {
        boolean selectedOption;
        SurveyOptions surveyOption = mOptions.get(position);

        if(surveyOption.getType() == TYPE_MEDICAMENTO)
        {
            if(!mOptionsMedicamentoSelected.contains(mOptions.get(position).getId()) &&
                    mOptions.get(position).getType() == TYPE_MEDICAMENTO)
            {
                mOptionsMedicamentoSelected.add(surveyOption.getId());
                selectedOption = true;

                surveyOption.setDrawableOption(R.drawable.ic_frasco_press);
                surveyOption.setColor(R.color.laranja);
                mAdapter.changeMedicamentoImage(view, position);
            } else {
                mOptionsMedicamentoSelected.remove(surveyOption.getId());
                selectedOption = false;

                surveyOption.setDrawableOption(R.drawable.ic_frasco_80);
                surveyOption.setColor(R.color.verdeClaro);
                mAdapter.changeMedicamentoImage(view, position);
            }

            mAdapter.setOptionsMedicamentoSelected(mOptionsMedicamentoSelected);
        } else {
            if(!mOptionsMetodoAlivioSelected.contains(mOptions.get(position).getId()) &&
                    mOptions.get(position).getType() == TYPE_METODO_ALIVIO)
            {
                mOptionsMetodoAlivioSelected.add(surveyOption.getId());
                selectedOption = true;
            } else {
                mOptionsMetodoAlivioSelected.remove(surveyOption.getId());
                selectedOption = false;
            }

            mAdapter.setOptionsMetodoAlivioSelected(mOptionsMetodoAlivioSelected);
        }

        view.setSelected(selectedOption);
        onChangeSelectedOptions(mOptionsMedicamentoSelected, mOptionsMetodoAlivioSelected);
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_METODO_ALIVIO_FUNCIONOU_ID, mSurveyManager.getCriseId());
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        Crise crise =  realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                .findFirst();

        RealmList<MetodoAlivioCrise> metodoAlivioList = crise.getMetodos();
        int sizeMetodoAlivioList = (metodoAlivioList == null ? 0 : metodoAlivioList.size());
        if(sizeMetodoAlivioList > 0)
        {
            for(MetodoAlivioCrise metodo : metodoAlivioList)
            {
                String metodoAlivioID = metodo.getMetodoAlivio().getId();
                if(!mOptionsMetodoAlivioSelected.contains(metodoAlivioID) && metodo.isFuncionou())
                {
                    mOptionsMetodoAlivioSelected.add(metodoAlivioID);
                    mAdapter.setOptionsMetodoAlivioSelected(mOptionsMetodoAlivioSelected);
                }
            }
            onChangeSelectedOptions(mOptionsMedicamentoSelected, mOptionsMetodoAlivioSelected);
        }

        RealmList<Medicamento> medicamentos = crise.getMedicamentos();
        int sizeMedicamentos = (medicamentos == null ? 0 : medicamentos.size());
        if(sizeMedicamentos > 0)
        {
            for(Medicamento medicamento : medicamentos)
            {
                if(!mOptionsMedicamentoSelected.contains(medicamento.getId()) && medicamento.isFuncionou())
                {
                    mOptionsMedicamentoSelected.add(medicamento.getId());
                    mAdapter.setOptionsMedicamentoSelected(mOptionsMedicamentoSelected);
                }
            }
            onChangeSelectedOptions(mOptionsMedicamentoSelected, mOptionsMetodoAlivioSelected);
        }

        if(sizeMedicamentos > 0 || sizeMetodoAlivioList > 0)
        {
            mOptions = buildOptions(medicamentos, metodoAlivioList);
            mAdapter.setItens(mOptions);
        }

        mAdapter.setRecyclerViewOnClickListenerHack(this);
        mContentRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {
        mAdapter.setItens(new ArrayList<SurveyOptions>());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onMetodoAlivioFuncionouOptionsUpdated(List<String> selectedMedicamentoOptions, List<String> selectedMetodoAlivioOptions);
    }

}
