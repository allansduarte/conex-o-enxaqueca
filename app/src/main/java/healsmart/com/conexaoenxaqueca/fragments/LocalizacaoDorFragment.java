package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.LocaisDeDor;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.LocaisDeDorEntidade;
import healsmart.com.conexaoenxaqueca.widget.MultiPartsImageView;
import healsmart.com.conexaoenxaqueca.widget.OnClickLocalizacaoDorWizard;
import healsmart.com.conexaoenxaqueca.widget.PainPosition;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LocalizacaoDorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocalizacaoDorFragment extends Fragment {
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "SELECIONE O LOCAL DA SUA DOR:";

    private View mViewFragment;

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private Realm realm;
    private SurveyManager mSurveyManager;

    private static final String DRAWABLE_BASE_NAME = "localizacao_dor_";

    private MultiPartsImageView mMultiPartsImageView;
    private int mDrawableInitial;
    private int mDrawableMap;
    private int[] mImages;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment LocalizacaoDorFragment.
     */
    public static LocalizacaoDorFragment newInstance(SurveyManager surveyManager) {
        LocalizacaoDorFragment fragment = new LocalizacaoDorFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public LocalizacaoDorFragment() {
        // Required empty public constructor

        mDrawableInitial = R.drawable.localizacao_dor_0;
        mDrawableMap = R.drawable.localizacao_dor_map;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mViewFragment = inflater.inflate(R.layout.fragment_localizacao_dor, container, false);
        initViews();

        return mViewFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();

        initializeMultipartView();
        initBehaviorAndEvents();
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
    }

    private void initViews()
    {
        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);

        mMultiPartsImageView = (MultiPartsImageView) mViewFragment.findViewById(R.id.painLocationImageView);
    }

    private void initBehaviorAndEvents()
    {
        mSurveyIndicatorTextView.setVisibility(View.GONE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);

        if(mSurveyManager.isUpdateMode())
        {
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
            mNextButton.setVisibility(View.GONE);
        } else {
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
            mNextButton.setVisibility(View.VISIBLE);
        }
    }

    private void initializeMultipartView()
    {
        mDrawableInitial = R.drawable.localizacao_dor_0;
        mDrawableMap = R.drawable.localizacao_dor_map;
        mImages = new int[(PainPosition.values().length + 1)];
        for (int i = 0; i < mImages.length; i++) {
            mImages[i] = getResources().getIdentifier(DRAWABLE_BASE_NAME + String.format("%d", new Object[]{Integer.valueOf(i + 1)}), "drawable", getActivity().getPackageName());
        }

        Crise crise =  realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                .findFirst();

        RealmList<LocaisDeDor> locaisDeDores = crise.getLocaisDeDores();
        int sizeLocaisDeDores = (locaisDeDores == null ? 0 : locaisDeDores.size());

        int[] selectedImages = new int[(sizeLocaisDeDores + 1)];
        //selectedImages[0] = LocaisDeDor.quantidade;

        if(sizeLocaisDeDores > 0)
        {
            int indice = 1;
            for (LocaisDeDor localDor : locaisDeDores) {
                int i = indice + 1;
                selectedImages[indice] = localDor.getId();
                indice = i;
            }
        }

        mMultiPartsImageView.initialize(mDrawableInitial, mDrawableMap, mImages, selectedImages);

        mMultiPartsImageView.setZoomable(true);
        //mMultiPartsImageView.setDoubleTapZoomEnabled(false);
        mMultiPartsImageView.setMaximumScale(4.0f);
        mMultiPartsImageView.setPartSelectionChangedListener(new OnClickLocalizacaoDorWizard(this));
    }

    public void addPain(int areaId)
    {
        boolean doAction = true;

        Crise crise =  realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                .findFirst();

        LocaisDeDor localDor = realm.where(LocaisDeDor.class)
                                    .equalTo(LocaisDeDorEntidade.ID, areaId)
                                    .findFirst();
        if(localDor == null)
            return;

        RealmList<LocaisDeDor> locaisDeDores = crise.getLocaisDeDores();
        for(int i=0; i < locaisDeDores.size(); i++) {

            if(locaisDeDores.get(i).getId() == areaId) {
                doAction = false;
                break;
            }
        }

        if(doAction)
        {
            try {
                realm.beginTransaction();

                crise.getLocaisDeDores().add(localDor);

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        }
    }

    public void removePain(int areaId)
    {
        boolean doAction = false;
        int index = 0;

        Crise crise =  realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                .findFirst();

        LocaisDeDor localDor = realm.where(LocaisDeDor.class).equalTo(LocaisDeDorEntidade.ID, areaId)
                .findFirst();

        if(localDor == null)
            return;

        RealmList<LocaisDeDor> locaisDeDores = crise.getLocaisDeDores();
        for(int i=0; i < locaisDeDores.size(); i++) {

            if(locaisDeDores.get(i).getId() == areaId) {
                doAction = true;
                index = i;
                break;
            }
        }

        if(doAction)
        {
            try {
                realm.beginTransaction();

                crise.getLocaisDeDores().remove(index);

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
