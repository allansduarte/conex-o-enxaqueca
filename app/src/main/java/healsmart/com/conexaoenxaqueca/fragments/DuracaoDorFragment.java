package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.activities.SumarioCriseActivity;
import healsmart.com.conexaoenxaqueca.core.Service;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import io.realm.Realm;
import io.realm.RealmResults;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DuracaoDorFragment.OnQuestionOneListener} interface
 * to handle interaction events.
 * Use the {@link DuracaoDorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DuracaoDorFragment extends Fragment implements LoaderManager.LoaderCallbacks<Object>, SurveyConstants
{
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "QUANDO COMEÇOU A DOR?";
    private static final int LOADER_ID = 1;

    private View mViewFragment;

    private TextView mInicioDataCriseTextView;
    private TextView mInicioTempoCriseTextView;
    private ImageView mICDataCriseInicio;
    private ImageView mICTempoCriseInicio;
    private TextView mTextoICDataCriseInicio;
    private TextView mTextoICTempoCriseInicio;

    private TextView mFimDataCriseTextView;
    private TextView mFimTempoCriseTextView;
    private ImageView mICDataCriseFim;
    private ImageView mICTempoCriseFim;
    private TextView mTextoICDataCriseFim;
    private TextView mTextoICTempoCriseFim;

    private ImageView mICCriseAindaNaoTerminouImageView;
    private boolean mCriseAindaNaoTerminou=false;

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private CoordinatorLayout mCoordinatorLayout;
    private Snackbar mSnackbar;
    private String mCriseEmAndamentoId;

    private Calendar mCalendar;
    private Calendar mCalendarInicio;
    private Calendar mCalendarFim;

    private Animation animation;

    private OnQuestionOneListener mListener;

    private int mYear = 2000;
    private int mMonth;
    private int mDay = 1;
    private int mHour;
    private int mMinute;

    private SurveyManager mSurveyManager;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment DuracaoDorFragment.
     */
    public static DuracaoDorFragment newInstance(SurveyManager surveyManager) {
        DuracaoDorFragment fragment = new DuracaoDorFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public DuracaoDorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mViewFragment = inflater.inflate(R.layout.fragment_duracao_dor, container, false);
        initViews();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return mViewFragment;
    }

    @Override
    public void onStart() {
        super.onStart();

        initBehaviorAndEvents();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getLoaderManager().destroyLoader(LOADER_ID);
    }

    private void initViews()
    {
        mInicioDataCriseTextView = (TextView) mViewFragment.findViewById(R.id.inicioDataCriseTextView);
        mInicioTempoCriseTextView = (TextView) mViewFragment.findViewById(R.id.inicioTempoCriseTextView);
        mICDataCriseInicio = (ImageView) mViewFragment.findViewById(R.id.icDataCriseInicio);
        mICTempoCriseInicio = (ImageView) mViewFragment.findViewById(R.id.icTempoCriseInicio);
        mTextoICDataCriseInicio = (TextView) mViewFragment.findViewById(R.id.textoICDataCriseInicio);
        mTextoICTempoCriseInicio = (TextView) mViewFragment.findViewById(R.id.textoICTempoCriseInicio);

        mFimDataCriseTextView = (TextView) mViewFragment.findViewById(R.id.fimDataCriseTextView);
        mFimTempoCriseTextView = (TextView) mViewFragment.findViewById(R.id.fimTempoCriseTextView);
        mICDataCriseFim = (ImageView) mViewFragment.findViewById(R.id.icDataCriseFim);
        mICTempoCriseFim = (ImageView) mViewFragment.findViewById(R.id.icTempoCriseFim);
        mTextoICDataCriseFim = (TextView) mViewFragment.findViewById(R.id.textoICDataCriseFim);
        mTextoICTempoCriseFim = (TextView) mViewFragment.findViewById(R.id.textoICTempoCriseFim);

        mICCriseAindaNaoTerminouImageView = (ImageView) mViewFragment.findViewById(R.id.icCriseNaoTerminouImageView);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);
        mCoordinatorLayout = (CoordinatorLayout) getActivity().findViewById(R.id.coordinatorLayout);
    }

    private void initBehaviorAndEvents()
    {
        mSurveyTitleTextView.setText(TITLE_QUESTION);
        mSurveyIndicatorTextView.setVisibility(View.GONE);

        buildVisibilityNavMenu();

        mICDataCriseInicio.setOnClickListener(onClickSelecionarData);
        mTextoICDataCriseInicio.setOnClickListener(onClickSelecionarData);
        mInicioDataCriseTextView.setOnClickListener(onClickSelecionarData);
        mICDataCriseFim.setOnClickListener(onClickSelecionarData);
        mTextoICDataCriseFim.setOnClickListener(onClickSelecionarData);
        mFimDataCriseTextView.setOnClickListener(onClickSelecionarData);

        mICTempoCriseInicio.setOnClickListener(onClickSelecionarTempo);
        mTextoICTempoCriseInicio.setOnClickListener(onClickSelecionarTempo);
        mInicioTempoCriseTextView.setOnClickListener(onClickSelecionarTempo);
        mICTempoCriseFim.setOnClickListener(onClickSelecionarTempo);
        mTextoICTempoCriseFim.setOnClickListener(onClickSelecionarTempo);
        mFimTempoCriseTextView.setOnClickListener(onClickSelecionarTempo);

        mICCriseAindaNaoTerminouImageView.setOnClickListener(onClickSelecionarOpcao);
    }

    private void buildVisibilityNavMenu() {
        if(mSurveyManager.isUpdateMode())
        {
            mNextButton.setVisibility(View.GONE);
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
        } else {
            mNextButton.setVisibility(View.VISIBLE);
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
        }
    }

    public void onDataChange(Calendar calendarInicio, Calendar calendarFim, boolean criseAindaNaoTerminou) {
        if (mListener != null) {
            mListener.onQuestionOneListener(calendarInicio, calendarFim, criseAindaNaoTerminou);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnQuestionOneListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnQuestionOneListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    View.OnClickListener onClickSelecionarOpcao = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!mCriseAindaNaoTerminou)
                setBehaviorCrisisNotYetOver();
        }
    };

    private void setBehaviorCrisisNotYetOver()
    {
        mICCriseAindaNaoTerminouImageView.setSelected(true);
        mFimTempoCriseTextView.setText("---");
        mCalendarFim.setTimeInMillis(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());
        mCriseAindaNaoTerminou = true;

        onDataChange(mCalendarInicio, null, true);
    }

    View.OnClickListener onClickSelecionarTempo = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            animation = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce_up);
            switch (v.getId()){
                case R.id.inicioTempoCriseTextView:
                    callAndUpdateDialogTimePicker(mInicioTempoCriseTextView, mCalendarInicio);
                    break;
                case R.id.icTempoCriseInicio:
                    callAndUpdateDialogTimePicker(mInicioTempoCriseTextView, mCalendarInicio);
                    break;
                case R.id.textoICTempoCriseInicio:
                    callAndUpdateDialogTimePicker(mInicioTempoCriseTextView, mCalendarInicio);
                    break;
                case R.id.fimTempoCriseTextView:
                    callAndUpdateDialogTimePicker(mFimTempoCriseTextView, mCalendarFim);
                    break;
                case R.id.icTempoCriseFim:
                    callAndUpdateDialogTimePicker(mFimTempoCriseTextView, mCalendarFim);
                    break;
                case R.id.textoICTempoCriseFim:
                    callAndUpdateDialogTimePicker(mFimTempoCriseTextView, mCalendarFim);
                    break;
            }
        }
    };

    /**
     * Método utilizado para escolha de data de início de crise. É apresentado um {@link DatePickerDialog} com a data atual.
     *
     * @return void
     */
    View.OnClickListener onClickSelecionarData = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            animation = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce_up);
            switch (v.getId()){
                case R.id.inicioDataCriseTextView:
                    callAndUpdateDialogDatePicker(mInicioDataCriseTextView, mCalendarInicio);
                    break;
                case R.id.icDataCriseInicio:
                    callAndUpdateDialogDatePicker(mInicioDataCriseTextView, mCalendarInicio);
                    break;
                case R.id.textoICDataCriseInicio:
                    callAndUpdateDialogDatePicker(mInicioDataCriseTextView, mCalendarInicio);
                    break;
                case R.id.fimDataCriseTextView:
                    callAndUpdateDialogDatePicker(mFimDataCriseTextView, mCalendarFim);
                    break;
                case R.id.icDataCriseFim:
                    callAndUpdateDialogDatePicker(mFimDataCriseTextView, mCalendarFim);
                    break;
                case R.id.textoICDataCriseFim:
                    callAndUpdateDialogDatePicker(mFimDataCriseTextView, mCalendarFim);
                    break;
            }
        }
    };

    private void callAndUpdateDialogDatePicker(final TextView textView, Calendar calendar)
    {
        if(calendar == null)
            mCalendar =   Calendar.getInstance();
        else
            mCalendar = calendar;

        setPickerYear(mCalendar.get(Calendar.YEAR));
        setPickerMonth(mCalendar.get(Calendar.MONTH));
        setPickerDay(mCalendar.get(Calendar.DAY_OF_MONTH));
        setPickerHour(mCalendar.get(Calendar.HOUR_OF_DAY));
        setPickerMinute(mCalendar.get(Calendar.MINUTE));

        DatePickerDialog mDatePicker = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.HOUR_OF_DAY,getPickerHour());
                        calendar.set(Calendar.MINUTE,getPickerMinute());
                        calendar.set(Calendar.SECOND,0);
                        calendar.set(Calendar.MILLISECOND,0);

                        mCalendar = calendar;
                        atualizarData(textView);
                    }
                }, getPickerYear(), getPickerMonth(), getPickerDay());
        mDatePicker.show();
    }

    private void atualizarData(TextView textView) {

        SimpleDateFormat dateFormatDate = new SimpleDateFormat("dd/MM/yyyy");

        if( textView.getId() == R.id.inicioDataCriseTextView)
        {
            mCalendarInicio = (Calendar) mCalendar.clone();
            textView.setText(dateFormatDate.format(mCalendar.getTime()));
        } else if(textView.getId() == R.id.fimDataCriseTextView)
        {
            if(mCalendar != null) {
                mCalendarFim = (Calendar) mCalendar.clone();
                textView.setText(dateFormatDate.format(mCalendar.getTime()));
            } else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());

                textView.setText(dateFormatDate.format(calendar.getTime()));
            }
        }

        textView.startAnimation(animation);

        onDataChange(mCalendarInicio, mCalendarFim, mCriseAindaNaoTerminou);
    }

    private void callAndUpdateDialogTimePicker(final TextView textView, Calendar calendar)
    {
        if( calendar == null )
            mCalendar   =   Calendar.getInstance();
        else
            mCalendar = calendar;

        setPickerYear(mCalendar.get(Calendar.YEAR));
        setPickerMonth(mCalendar.get(Calendar.MONTH));
        setPickerDay(mCalendar.get(Calendar.DAY_OF_MONTH));
        setPickerHour(mCalendar.get(Calendar.HOUR_OF_DAY));
        setPickerMinute(mCalendar.get(Calendar.MINUTE));

        TimePickerDialog mTimePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR,getPickerYear());
                        calendar.set(Calendar.MONTH,getPickerMonth());
                        calendar.set(Calendar.DAY_OF_MONTH,getPickerDay());
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);
                        calendar.set(Calendar.SECOND,0);
                        calendar.set(Calendar.MILLISECOND,0);

                        mCalendar = calendar;
                        atualizarTempo(textView);
                    }
                },
                getPickerHour(), getPickerMinute(), true);
        mTimePickerDialog.show();
    }

    private void atualizarTempo(TextView textView) {
        SimpleDateFormat dateFormatDate = new SimpleDateFormat("HH:mm");
        String texto = "---";

        if(mCalendar != null)
            texto = dateFormatDate.format(mCalendar.getTime());

        textView.setText(texto);
        textView.startAnimation(animation);

        if( textView.getId() == R.id.inicioTempoCriseTextView)
        {
            mCalendarInicio = (Calendar) mCalendar.clone();
        }
        else if(textView.getId() == R.id.fimTempoCriseTextView)
        {
            if(mCalendar != null)
                mCalendarFim = (Calendar) mCalendar.clone();

            mCriseAindaNaoTerminou = false;
            mICCriseAindaNaoTerminouImageView.setSelected(false);
        }

        verifyTimeDurationPain();
        onDataChange(mCalendarInicio, mCalendarFim, mCriseAindaNaoTerminou);
    }

    public boolean verifyTimeDurationPain()
    {
        boolean acaoValidacao = true;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        if(mCalendarInicio != null && mCalendarFim != null) {
            if(!mCriseAindaNaoTerminou && mCalendarInicio.getTimeInMillis() > mCalendarFim.getTimeInMillis()) {
                acaoValidacao = false;

                String text = getActivity().getResources().getString(R.string.validacao_horario_final);
                mSnackbar = Snackbar.make(mCoordinatorLayout, text, Snackbar.LENGTH_INDEFINITE);
                View snackbarView = mSnackbar.getView();
                snackbarView.setBackgroundColor(Color.RED);
                mSnackbar.show();

                mNextButton.setVisibility(View.GONE);
                mSuspendSurveyButton.setVisibility(View.GONE);
            }
            else {
                snackBarDismiss();
            }
        }

        return acaoValidacao;
    }

    public boolean verifyCrisisInProgress() {
        boolean acaoValidacao = false;

        Crise crise = hasCrisisInProgress();
        if(crise != null) {
            acaoValidacao = true;

            mCriseEmAndamentoId = crise.getId();
            String text = "Existe uma crise em andamento!";
            mSnackbar = Snackbar.make(mCoordinatorLayout, text, Snackbar.LENGTH_INDEFINITE)
            .setAction("Visualizar", mOnClickVisualizarCriseSnackBar);
            mSnackbar.setActionTextColor(Color.YELLOW);
            View snackbarView = mSnackbar.getView();
            snackbarView.setBackgroundColor(Color.BLACK);
            mSnackbar.show();

            mNextButton.setVisibility(View.GONE);
            mSuspendSurveyButton.setVisibility(View.GONE);
        } else {
            snackBarDismiss();
        }

        return acaoValidacao;
    }

    private void snackBarDismiss() {
        if (mSnackbar != null && mSnackbar.isShown()) {
            mSnackbar.dismiss();

            mNextButton.setVisibility(View.VISIBLE);
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
        }
    }

    private View.OnClickListener mOnClickVisualizarCriseSnackBar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Activity activity = getActivity();
            String paramIntentMainActivity = "MainActivity";
            Intent intentSumarioCrise = new Intent(activity.getApplicationContext(), SumarioCriseActivity.class);
            intentSumarioCrise.putExtra(SurveyConstants.INTENT_CRISE_ID, mCriseEmAndamentoId);
            intentSumarioCrise.putExtra(paramIntentMainActivity, paramIntentMainActivity);

            activity.startActivity(intentSumarioCrise);
        }
    };

    private Crise hasCrisisInProgress() {

        Crise crise = null;
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Crise> crises = realm.where(Crise.class)
                .equalTo(CriseEntidade.DATAFIM, 0)
                .findAll();
        int sizeCrises = (crises == null ? 0 : crises.size());
        if(sizeCrises > 0)
            crise = Crise.copy(crises.last());
        realm.close();

        return crise;
    }

    public int getPickerYear() {
        return mYear;
    }

    public void setPickerYear(int mYear) {
        this.mYear = mYear;
    }

    public int getPickerMonth() {
        return mMonth;
    }

    public void setPickerMonth(int mMonth) {
        this.mMonth = mMonth;
    }

    public int getPickerDay() {
        return mDay;
    }

    public void setPickerDay(int mDay) {
        this.mDay = mDay;
    }

    public int getPickerHour() {
        return mHour;
    }

    public void setPickerHour(int mHour) {
        this.mHour = mHour;
    }

    public int getPickerMinute() {
        return mMinute;
    }

    public void setPickerMinute(int mMinute) {
        this.mMinute = mMinute;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_DURACAO_DOR_ID, mSurveyManager.getCriseId());
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        Crise crise = (Crise) data;

        boolean criseAindaNaoTerminou = false;
        animation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);

        if(crise != null)
        {
            mCalendarInicio = Calendar.getInstance();
            mCalendarInicio.setTimeInMillis(crise.getDataIni());
            long dataFim = crise.getDataFim();

            if(dataFim > 0)
            {
                mCalendarFim = Calendar.getInstance();
                mCalendarFim.setTimeInMillis(dataFim);
            } else criseAindaNaoTerminou = true;
        } else {
            mCalendar = Calendar.getInstance();
            mCalendar.setTimeInMillis(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());
        }

        if( mCalendarInicio != null )
            mCalendar = mCalendarInicio;

        atualizarTempo(mInicioTempoCriseTextView);
        atualizarData(mInicioDataCriseTextView);

        if( mCalendarFim != null )
            mCalendar = mCalendarFim;
        else mCalendar.setTimeInMillis(Service.getInstance().getServiceUtils().getCurrentDate());

        atualizarTempo(mFimTempoCriseTextView);
        atualizarData(mFimDataCriseTextView);

        if(crise == null)
            setBehaviorCrisisNotYetOver();
        else if(criseAindaNaoTerminou)
            setBehaviorCrisisNotYetOver();
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    public interface OnQuestionOneListener {
        void onQuestionOneListener(Calendar calendarInicio, Calendar calendarFim, boolean criseAindaNaoTerminou);
    }
}
