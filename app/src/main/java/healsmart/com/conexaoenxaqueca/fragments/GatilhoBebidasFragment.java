package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.AddSurveyOptionsTask;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.adapters.SpacesItemDecoration;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptions;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptionsAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Bebida;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.BebidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.GatilhoCriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.RecyclerViewOnClickListenerHack;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GatilhoBebidasFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GatilhoBebidasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GatilhoBebidasFragment extends Fragment
        implements  RecyclerViewOnClickListenerHack,
                    LoaderManager.LoaderCallbacks<Object>,
                    SurveyConstants
{
    private static final int NR_OPCOES_FIXAS = 3;
    private static final int LOADER_ID = 72;
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "INFORME A BEBIDA:";
    private static final String TITLE_QUESTION_INDICATOR = "gatilhos - bebida";

    private View mViewFragment;
    private RecyclerView mContentRecyclerView;
    private List<SurveyOptions> mOptions;
    private List<String> mOptionsSelected = new ArrayList<>();

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private SurveyManager mSurveyManager;
    private SurveyOptionsAdapter mAdapter;
    private Resources mResources;
    private Realm realm;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment GatilhoBebidasFragment.
     */
    public static GatilhoBebidasFragment newInstance(SurveyManager surveyManager) {
        GatilhoBebidasFragment fragment = new GatilhoBebidasFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public GatilhoBebidasFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mViewFragment = inflater.inflate(R.layout.fragment_gatilho_bebida, container, false);
        initViews();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return mViewFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
    }

    private void initViews()
    {
        mContentRecyclerView = (RecyclerView) mViewFragment.findViewById(R.id.content_recycler_view);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);
        mResources = getActivity().getResources();

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mContentRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        mContentRecyclerView.setLayoutManager(gridLayoutManager);

        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration();
        spacesItemDecoration.setBottom(20);
        mContentRecyclerView.addItemDecoration(spacesItemDecoration);

        mSurveyIndicatorTextView.setText(TITLE_QUESTION_INDICATOR);
        mSurveyIndicatorTextView.setVisibility(View.VISIBLE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);

        mNextButton.setVisibility(View.GONE);
        mSuspendSurveyButton.setVisibility(View.GONE);
        mConfirmarButton.setVisibility(View.VISIBLE);
    }

    public void onChangeSelectedOptions(List<String> selectedOptions) {
        if (mListener != null)
        {
            mListener.onGatilhosBebidaOptionsUpdated(selectedOptions);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mAdapter = new SurveyOptionsAdapter(getActivity().getApplicationContext(), new ArrayList<SurveyOptions>());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mAdapter = null;
        mOptionsSelected=null;
    }

    public List<SurveyOptions> buildOptions(List<Bebida> bebidaItens){
        int[] drawables = new int[]{R.drawable.bebida_cafe, R.drawable.bebida_vinho,
                R.drawable.bebida_cerveja, R.drawable.bebida_destilados};
        List<SurveyOptions> listAux = new ArrayList<>();

        List<String> itensFixo = new ArrayList<>();
        itensFixo.add("café");
        itensFixo.add("vinho");
        itensFixo.add("cerveja");
        itensFixo.add("destilados");

        for(int i = 0; i < bebidaItens.size(); i++)
        {
            String itemTitulo;
            int itemDrawable;
            String itemId;

            Bebida opcaoItem = bebidaItens.get(i);
            if(itensFixo.contains(opcaoItem.getNome()))
                itemDrawable = drawables[i];
            else
                itemDrawable = R.drawable.option_pattern;

            itemTitulo = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(opcaoItem.getNome());
            itemId = opcaoItem.getId();

            SurveyOptions surveyOptions = new SurveyOptions( itemTitulo, itemDrawable, itemId );
            listAux.add(surveyOptions);
        }

        SurveyOptions itemExcluir = new SurveyOptions( "Excluir", R.drawable.null_option);
        SurveyOptions itemAdicionar = new SurveyOptions( "Adicionar", R.drawable.ic_add_option);

        listAux.add(itemAdicionar);
        listAux.add(itemExcluir);
        return listAux;
    }

    @Override
    public void onClickListener(View view, int position) {
        int totalItens = mAdapter.getItemCount();
        int deleteButton = totalItens-1;
        int addBtnPosition = deleteButton-1;

        if( position == addBtnPosition )
            adicionarOpcao(position);
        else if(position == deleteButton)
            mSurveyManager.setMode(ServiceFactory.getInstance().getServiceSurveyManager(getActivity().getApplicationContext()).alterarEstado(mSurveyManager.getMode(), "CLIQUE NO ÍCONE PARA EXCLUIR.", TITLE_QUESTION, mSurveyTitleTextView, view));
        else
        {
            boolean selectedOption = false;
            if(!mOptionsSelected.contains(mOptions.get(position).getId()) )
            {
                if(mSurveyManager.getMode() == MODE_DELETE && position > NR_OPCOES_FIXAS)
                {
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                } else if(mSurveyManager.getMode() != MODE_DELETE) {
                    mOptionsSelected.add(mOptions.get(position).getId());
                    selectedOption = true;
                }
            } else{
                mOptionsSelected.remove(mOptions.get(position).getId());
                selectedOption = false;

                if(mSurveyManager.getMode() == MODE_DELETE && position > NR_OPCOES_FIXAS){
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                }
            }

            view.setSelected(selectedOption);
            onChangeSelectedOptions(mOptionsSelected);
            mAdapter.setOptionsSelected(mOptionsSelected);
        }
    }

    private boolean excluirOpcao(String id, int position)
    {
        long nrOpcaoRelacionadaCrise = realm.where(Crise.class)
                .equalTo(CriseEntidade.GATILHOS_CRISE + "." + GatilhoCriseEntidade.BEBIDA+"."+ BebidaEntidade.ID, id)
                .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                .count();

        if(nrOpcaoRelacionadaCrise == 0)
        {
            try {
                realm.beginTransaction();

                Bebida bebida = realm.where(Bebida.class)
                        .equalTo(BebidaEntidade.ID, id)
                        .equalTo(BebidaEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();
                bebida.removeFromRealm();

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        } else
        {
            Crise crise = realm.where(Crise.class)
                    .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                    .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                    .findFirst();

            int sizeGatilhos = (crise.getGatilhosCrise() == null ? 0 : crise.getGatilhosCrise().size());

            if(sizeGatilhos > 0)
            {
                for(int i=0; i < sizeGatilhos; i++)
                {
                    RealmList<Bebida> bebidas = crise.getGatilhosCrise().get(i).getBebidas();
                    int sizeBebida = (bebidas == null ? 0 : bebidas.size());
                    if(sizeBebida > 0)
                    {
                        for(int x=0; x < sizeBebida; x++)
                        {
                            if(bebidas.get(x).getId().equals(id))
                            {
                                try {
                                    realm.beginTransaction();

                                    if(sizeBebida == 1)
                                        crise.getGatilhosCrise().remove(i);
                                    else
                                        crise.getGatilhosCrise().get(i).getBebidas().remove(x);

                                    bebidas.get(x).setDisplay(QUESTION_INACTIVE);
                                    realm.copyToRealmOrUpdate(bebidas);

                                    realm.commitTransaction();
                                } catch (Exception e) {
                                    realm.cancelTransaction();
                                }
                                break;
                            }
                        }
                    } else {
                        Bebida bebida = realm.where(Bebida.class)
                                .equalTo(BebidaEntidade.ID, id)
                                .equalTo(BebidaEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                                .findFirst();

                        try {
                            realm.beginTransaction();

                            bebida.setDisplay(QUESTION_INACTIVE);
                            realm.copyToRealmOrUpdate(bebida);

                            realm.commitTransaction();
                        } catch (Exception e) {
                            realm.cancelTransaction();
                        }
                        break;
                    }
                }
            } else {
                Bebida bebida = realm.where(Bebida.class)
                        .equalTo(BebidaEntidade.ID, id)
                        .equalTo(BebidaEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();

                try {
                    realm.beginTransaction();

                    bebida.setDisplay(QUESTION_INACTIVE);
                    realm.copyToRealmOrUpdate(bebida);

                    realm.commitTransaction();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
            }
        }

        realm.refresh();
        mAdapter.removeListItem(position);
        mOptions = mAdapter.getItens();

        return true;
    }

    private void adicionarOpcao(final int addBtnPosition)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_option, null);

        final Dialog dialog = new Dialog(getActivity());
        Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
        Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ServiceFactory.getInstance().getServiceUtils().validarDialogAddOption(dialogView, mResources))
                {
                    EditText optionNameEditText = (EditText) dialogView.findViewById(R.id.optionName);
                    String nomeOpcao = optionNameEditText.getText().toString();

                    Bebida opcaoBebida = realm.where(Bebida.class)
                            .equalTo(BebidaEntidade.USUARIO+"."+UsuarioEntidade.ID, mSurveyManager.getUserId())
                            .equalTo(BebidaEntidade.NOME, nomeOpcao)
                            .findFirst();

                    if(opcaoBebida != null && opcaoBebida.getDisplay().equals(QUESTION_ACTIVE))
                    {
                        optionNameEditText.requestFocus();
                        optionNameEditText.setError("O nome desta opção já foi adicionado.");
                    } else {
                        String bebidaId;

                        if(opcaoBebida == null)
                            bebidaId = ServiceFactory.getInstance().getServiceUtils().generateHash(getActivity().getApplicationContext());
                        else
                            bebidaId = opcaoBebida.getId();

                        SurveyOptions surveyOptions = new SurveyOptions(nomeOpcao, R.drawable.option_pattern, bebidaId);
                        mAdapter.addListItem(surveyOptions, addBtnPosition);
                        mOptions = mAdapter.getItens();

                        Usuario usuario = realm.where(Usuario.class)
                                .equalTo(UsuarioEntidade.ID, mSurveyManager.getUserId())
                                .findFirst();

                        Bebida bebida = new Bebida();
                        bebida.setId(bebidaId);
                        bebida.setNome(nomeOpcao);
                        bebida.setUsuario(usuario);
                        bebida.setDisplay(QUESTION_ACTIVE);
                        bebida.setCreated(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());

                        new AddSurveyOptionsTask(getActivity().getApplicationContext(), QUESTION_GATILHOS_BEBIDA_ID).execute(bebida);

                        dialog.dismiss();
                    }
                }
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_GATILHOS_BEBIDA_ID);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        Crise crise =   realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                .findFirst();

        int indice=-1;
        int sizeGatilhos = (crise.getGatilhosCrise() == null ? 0 : crise.getGatilhosCrise().size());
        for(int i=0; i < sizeGatilhos; i++)
        {
            if(crise.getGatilhosCrise().get(i).getGatilho().getNome().equals("bebida"))
            {
                indice=i;
            }
        }

        if(indice > -1)
        {
            RealmList<Bebida> bebidaOptionsCriseList = crise.getGatilhosCrise().get(indice).getBebidas();

            if(bebidaOptionsCriseList != null){
                for(Bebida bebida : bebidaOptionsCriseList)
                {
                    String bebidaID = bebida.getId();
                    if(!mOptionsSelected.contains(bebidaID))
                        mOptionsSelected.add(bebidaID);
                }

                onChangeSelectedOptions(mOptionsSelected);
                mAdapter.setOptionsSelected(mOptionsSelected);
            }
        }

        List<Bebida> bebidaResults = Utils.castList(data, Bebida.class);

        List<String> bebidaResultsCompare = new ArrayList<>();
        for(int i=0; i < bebidaResults.size(); i++)
        {
            bebidaResultsCompare.add(bebidaResults.get(i).getId());
        }

        for(int i=0; i < mOptionsSelected.size(); i++)
        {
            String bebidaID = mOptionsSelected.get(i);
            if(!bebidaResultsCompare.contains(bebidaID))
            {
                Bebida opcao = realm.where(Bebida.class)
                        .equalTo(BebidaEntidade.ID, bebidaID)
                        .equalTo(BebidaEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();

                bebidaResults.add(opcao);
            }
        }

        mOptions = buildOptions(bebidaResults);
        mAdapter.setItens(mOptions);

        mAdapter.setRecyclerViewOnClickListenerHack(this);
        mContentRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {
        mAdapter.setItens(new ArrayList<SurveyOptions>());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onGatilhosBebidaOptionsUpdated(List<String> selectedOptions);
    }

}
