package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.AddSurveyOptionsTask;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.adapters.SpacesItemDecoration;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptions;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptionsAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.Gatilho;
import healsmart.com.conexaoenxaqueca.domain.GatilhoCrise;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.GatilhoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.RecyclerViewOnClickListenerHack;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GatilhosDorFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GatilhosDorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GatilhosDorFragment extends Fragment
        implements  RecyclerViewOnClickListenerHack,
                    LoaderManager.LoaderCallbacks<Object>,
                    SurveyConstants
{
    private static final int NR_OPCOES_FIXAS = 7;
    private static final int ESFORCO_FISICO_OPTION = 3;
    private static final int BEBIDA_OPTION = 6;
    private static final int COMIDA_OPTION = 7;
    private static final int LOADER_ID = 7;
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "O QUE VOCÊ ACREDITA QUE CONTRIBUIU PARA CRISE?";
    private static final String TITLE_QUESTION_INDICATOR = "gatilhos";

    private View mViewFragment;
    private RecyclerView mContentRecyclerView;
    private List<SurveyOptions> mOptions;
    private List<String> mOptionsSelected = new ArrayList<>();
    private List<String> mOptionsSaveSelected = new ArrayList<>();

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private SurveyManager mSurveyManager;
    private SurveyOptionsAdapter mAdapter;
    private Resources mResources;
    private Realm realm;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment GatilhosDorFragment.
     */
    public static GatilhosDorFragment newInstance(SurveyManager surveyManager) {
        GatilhosDorFragment fragment = new GatilhosDorFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public GatilhosDorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mViewFragment = inflater.inflate(R.layout.fragment_gatilhos_dor, container, false);
        initViews();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return mViewFragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mSurveyTitleTextView.setBackgroundColor(mResources.getColor(R.color.verdeClaro));
        mSurveyManager.setMode(MODE_INSERT);

        getLoaderManager().destroyLoader(LOADER_ID);
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
    }

    private void initViews()
    {
        mContentRecyclerView = (RecyclerView) mViewFragment.findViewById(R.id.content_recycler_view);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);
        mResources = getActivity().getResources();

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mContentRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        mContentRecyclerView.setLayoutManager(gridLayoutManager);

        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration();
        spacesItemDecoration.setBottom(20);
        mContentRecyclerView.addItemDecoration(spacesItemDecoration);

        mSurveyIndicatorTextView.setText(TITLE_QUESTION_INDICATOR);
        mSurveyIndicatorTextView.setVisibility(View.VISIBLE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);

        if(mSurveyManager.isUpdateMode())
        {
            mNextButton.setVisibility(View.GONE);
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
        } else {
            mNextButton.setVisibility(View.VISIBLE);
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
        }
    }

    public void onChangeSelectedOptions(List<String> selectedOptions) {
        if (mListener != null)
        {
            mListener.onGatilhosOptionsUpdated(selectedOptions);
        }
    }

    private void onNavigationClicked(int navigation)
    {
        if (mListener != null) {
            mListener.onQuestionNineNavigationListener(navigation);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mAdapter = new SurveyOptionsAdapter(getActivity().getApplicationContext(), new ArrayList<SurveyOptions>());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mAdapter = null;
        mOptionsSelected=null;
        mOptionsSaveSelected=null;
    }

    public List<SurveyOptions> buildOptions(List<Gatilho> gatilhosItens){
        int[] drawables = new int[]{R.drawable.gatilho_estresse, R.drawable.gatilho_falta_sono, R.drawable.gatilho_menstruacao,
                R.drawable.gatilho_esforco_fisico, R.drawable.gatilho_ansiedade, R.drawable.gatilho_com_fome,
                R.drawable.gatilho_bebida, R.drawable.gatilho_comida};
        List<SurveyOptions> listAux = new ArrayList<>();

        List<String> itensFixo = new ArrayList<>();
        itensFixo.add("estresse");
        itensFixo.add("falta de sono");
        itensFixo.add("menstruação");
        itensFixo.add("esforço físico");
        itensFixo.add("ansiedade");
        itensFixo.add("jejum");
        itensFixo.add("bebida");
        itensFixo.add("comida");

        for(int i = 0; i < gatilhosItens.size(); i++)
        {
            String itemTitulo;
            int itemDrawable;
            String itemId;

            Gatilho opcaoItem = gatilhosItens.get(i);
            if(itensFixo.contains(opcaoItem.getNome()))
                itemDrawable = drawables[i];
            else
                itemDrawable = R.drawable.option_pattern;

            itemTitulo = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(opcaoItem.getNome());
            itemId = opcaoItem.getId();

            SurveyOptions surveyOptions = new SurveyOptions( itemTitulo, itemDrawable, itemId );
            listAux.add(surveyOptions);
        }

        SurveyOptions itemExcluir = new SurveyOptions( "Excluir", R.drawable.null_option);
        SurveyOptions itemAdicionar = new SurveyOptions( "Adicionar", R.drawable.ic_add_option);

        listAux.add(itemAdicionar);
        listAux.add(itemExcluir);

        return listAux;
    }

    @Override
    public void onClickListener(View view, int position) {
        int totalItens = mAdapter.getItemCount();
        int deleteButton = totalItens-1;
        int addBtnPosition = deleteButton-1;

        if( position == addBtnPosition )
            adicionarOpcao(position);
        else if(position == deleteButton)
            mSurveyManager.setMode(ServiceFactory.getInstance().getServiceSurveyManager(getActivity().getApplicationContext()).alterarEstado(mSurveyManager.getMode(), "CLIQUE NO ÍCONE PARA EXCLUIR.", TITLE_QUESTION, mSurveyTitleTextView, view));
        else if(position == ESFORCO_FISICO_OPTION)
            this.onNavigationClicked(position);
        else if(position == BEBIDA_OPTION)
            this.onNavigationClicked(position);
        else if(position == COMIDA_OPTION)
            this.onNavigationClicked(position);
        else
        {
            boolean selectedOption = false;
            if(!mOptionsSelected.contains(mOptions.get(position).getId()))
            {
                if(mSurveyManager.getMode() == MODE_DELETE && position > NR_OPCOES_FIXAS)
                {
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                } else if(mSurveyManager.getMode() != MODE_DELETE) {
                    mOptionsSelected.add(mOptions.get(position).getId());
                    mOptionsSaveSelected.add(mOptions.get(position).getId());
                    selectedOption = true;
                }
            } else {
                mOptionsSelected.remove(mOptions.get(position).getId());
                mOptionsSaveSelected.remove(mOptions.get(position).getId());
                selectedOption = false;

                if(mSurveyManager.getMode() == MODE_DELETE && position > NR_OPCOES_FIXAS){
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                }
            }

            view.setSelected(selectedOption);
            onChangeSelectedOptions(mOptionsSaveSelected);
            mAdapter.setOptionsSelected(mOptionsSelected);
        }
    }

    private boolean excluirOpcao(String id, int position)
    {
        long nrOpcaoRelacionadaCrise = realm.where(Crise.class)
                .equalTo(CriseEntidade.GATILHOS_CRISE + "." + GatilhoEntidade.ID, id)
                .count();

        if(nrOpcaoRelacionadaCrise == 0)
        {
            try {
                realm.beginTransaction();

                Gatilho opcao = realm.where(Gatilho.class)
                        .equalTo(GatilhoEntidade.ID, id)
                        .equalTo(GatilhoEntidade.USUARIO+"."+UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();
                opcao.removeFromRealm();

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        } else
        {
            try {
                realm.beginTransaction();

                Crise crise = realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                        .findFirst();

                int sizeGatilhos = (crise.getGatilhosCrise() == null ? 0 : crise.getGatilhosCrise().size());

                if(sizeGatilhos > 0)
                {
                    for(int i=0; i < sizeGatilhos; i++)
                    {
                        GatilhoCrise gatilhoCrise = crise.getGatilhosCrise().get(i);
                        if(gatilhoCrise.getGatilho().getId().equals(id))
                        {
                            Gatilho gatilho = gatilhoCrise.getGatilho();
                            crise.getGatilhosCrise().remove(i);

                            gatilho.setDisplay(QUESTION_INACTIVE);
                            realm.copyToRealmOrUpdate(gatilho);
                            break;
                        }
                    }
                } else {
                    Gatilho gatilho = realm.where(Gatilho.class)
                            .equalTo(GatilhoEntidade.ID, id)
                            .equalTo(GatilhoEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                            .findFirst();

                    gatilho.setDisplay(QUESTION_INACTIVE);
                    realm.copyToRealmOrUpdate(gatilho);
                }

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        }

        realm.refresh();
        mAdapter.removeListItem(position);
        mOptions = mAdapter.getItens();

        return true;
    }

    private void adicionarOpcao(final int position)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_option, null);

        final Dialog dialog = new Dialog(getActivity());
        Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
        Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ServiceFactory.getInstance().getServiceUtils().validarDialogAddOption(dialogView, mResources))
                {
                    EditText optionNameEditText = (EditText) dialogView.findViewById(R.id.optionName);
                    String nomeOpcao = optionNameEditText.getText().toString();

                    Gatilho opcaoGatilho = realm.where(Gatilho.class)
                            .equalTo(GatilhoEntidade.USUARIO+"."+UsuarioEntidade.ID, mSurveyManager.getUserId())
                            .equalTo(GatilhoEntidade.NOME, nomeOpcao)
                            .findFirst();

                    if(opcaoGatilho != null && opcaoGatilho.getDisplay().equals(QUESTION_ACTIVE))
                    {
                        optionNameEditText.requestFocus();
                        optionNameEditText.setError("O nome desta opção já foi adicionado.");
                    } else {
                        String gatilhoId;
                        if(opcaoGatilho == null)
                            gatilhoId = ServiceFactory.getInstance().getServiceUtils().generateHash(getActivity().getApplicationContext());
                        else
                            gatilhoId = opcaoGatilho.getId();

                        SurveyOptions surveyOptions = new SurveyOptions(nomeOpcao, R.drawable.option_pattern, gatilhoId);
                        mAdapter.addListItem(surveyOptions, position-1);
                        mOptions = mAdapter.getItens();

                        Usuario usuario = realm.where(Usuario.class)
                                .equalTo(UsuarioEntidade.ID, mSurveyManager.getUserId())
                                .findFirst();

                        Gatilho gatilho = new Gatilho();
                        gatilho.setId(gatilhoId);
                        gatilho.setNome(nomeOpcao);
                        gatilho.setUsuario(usuario);
                        gatilho.setDisplay(QUESTION_ACTIVE);
                        gatilho.setCreated(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());

                        new AddSurveyOptionsTask(getActivity().getApplicationContext(), QUESTION_GATILHOS_ID).execute(gatilho);

                        dialog.dismiss();
                    }
                }
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_GATILHOS_ID);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data)
    {
        Crise crise =   realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                        .findFirst();

        RealmList<GatilhoCrise> gatilhosCrise = crise.getGatilhosCrise();
        int sizeGatilhosCrise = (crise.getGatilhosCrise() == null ? 0 : crise.getGatilhosCrise().size());
        for(int i=0; i < sizeGatilhosCrise; i++)
        {
            String gatilhoID = gatilhosCrise.get(i).getGatilho().getId();
            if(!mOptionsSelected.contains(gatilhoID)) {
                mOptionsSelected.add(gatilhoID);
            }

            String nomeGatilho = gatilhosCrise.get(i).getGatilho().getNome();
            if(!nomeGatilho.equals("esforço físico") &&
                    !nomeGatilho.equals("bebida") &&
                    !nomeGatilho.equals("comida"))
            {
                mOptionsSaveSelected.add(gatilhosCrise.get(i).getGatilho().getId());
            }
        }

        onChangeSelectedOptions(mOptionsSaveSelected);
        mAdapter.setOptionsSelected(mOptionsSelected);

        List<Gatilho> gatilhoResults = Utils.castList(data, Gatilho.class);

        List<String> gatilhoResultsCompare = new ArrayList<>();
        for(int i=0; i < gatilhoResults.size(); i++)
        {
            gatilhoResultsCompare.add(gatilhoResults.get(i).getId());
        }

        for(int i=0; i < mOptionsSelected.size(); i++)
        {
            String gatilhoId = mOptionsSelected.get(i);
            if(!gatilhoResultsCompare.contains(gatilhoId))
            {
                Gatilho opcao = realm.where(Gatilho.class)
                        .equalTo(GatilhoEntidade.ID, gatilhoId)
                        .equalTo(GatilhoEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();

                gatilhoResults.add(opcao);
            }
        }

        mOptions = buildOptions(gatilhoResults);
        mAdapter.setItens(mOptions);

        mAdapter.setRecyclerViewOnClickListenerHack(this);
        mContentRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {
        mAdapter.setItens(new ArrayList<SurveyOptions>());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onGatilhosOptionsUpdated(List<String> selectedOptions);
        void onQuestionNineNavigationListener(int navigation);
    }

}
