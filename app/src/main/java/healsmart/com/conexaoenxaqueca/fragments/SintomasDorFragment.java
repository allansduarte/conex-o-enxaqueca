package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.AddSurveyOptionsTask;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.adapters.SpacesItemDecoration;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptions;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptionsAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.Sintoma;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.RecyclerViewOnClickListenerHack;
import healsmart.com.conexaoenxaqueca.interfaces.SintomaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SintomasDorFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SintomasDorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SintomasDorFragment extends Fragment
        implements  RecyclerViewOnClickListenerHack,
                    LoaderManager.LoaderCallbacks<Object>,
                    SurveyConstants
{
    private static final int NR_OPCOES_FIXAS = 6;
    private static final int LOADER_ID = 0;
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "ESTAVA TENDO ALGUM DESSES SINTOMAS?";
    private static final String TITLE_QUESTION_INDICATOR = "sintomas";

    private View mViewFragment;
    private RecyclerView mContentRecyclerView;
    private List<SurveyOptions> mOptions;
    private List<String> mOptionsSelected =  new ArrayList<>();

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private SurveyManager mSurveyManager;
    private SurveyOptionsAdapter mAdapter;
    private Resources mResources;
    private Realm realm;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment SintomasDorFragment.
     */
    public static SintomasDorFragment newInstance(SurveyManager surveyManager) {
        SintomasDorFragment fragment = new SintomasDorFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public SintomasDorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mViewFragment = inflater.inflate(R.layout.fragment_sintomas_dor, container, false);
        initViews();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return mViewFragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mSurveyTitleTextView.setBackgroundColor(mResources.getColor(R.color.verdeClaro));
        mSurveyManager.setMode(MODE_INSERT);

        getLoaderManager().destroyLoader(LOADER_ID);
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
    }

    private void initViews()
    {
        mContentRecyclerView = (RecyclerView) mViewFragment.findViewById(R.id.content_recycler_view);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);
        mResources = getActivity().getResources();

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mContentRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        mContentRecyclerView.setLayoutManager(gridLayoutManager);

        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration();
        spacesItemDecoration.setBottom(20);
        mContentRecyclerView.addItemDecoration(spacesItemDecoration);

        if(mSurveyManager.isUpdateMode())
        {
            mNextButton.setVisibility(View.GONE);
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
        } else {
            mNextButton.setVisibility(View.VISIBLE);
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
        }

        mSurveyIndicatorTextView.setText(TITLE_QUESTION_INDICATOR);
        mSurveyIndicatorTextView.setVisibility(View.VISIBLE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);
    }

    public void onChangeSelectedOptions(List<String> selectedOptions) {
        if (mListener != null)
        {
            mListener.onSintomasOptionsUpdated(selectedOptions);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mAdapter = new SurveyOptionsAdapter(getActivity().getApplicationContext(), new ArrayList<SurveyOptions>());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mAdapter = null;
        mOptionsSelected=null;
    }

    public List<SurveyOptions> buildOptions(List<Sintoma> sintomasItens){
        int[] drawables = new int[]{R.drawable.sintomas_nausea, R.drawable.sintomas_vomito, R.drawable.sintomas_intolerancia_luz,
                R.drawable.sintomas_intolerancia_barulho, R.drawable.sintomas_alteracoes_visao, R.drawable.sintomas_congestao_nasal,
                R.drawable.sintomas_tontura};
        List<SurveyOptions> listAux = new ArrayList<>();

        List<String> itensFixo = new ArrayList<>();
        itensFixo.add("náusea");
        itensFixo.add("vômito");
        itensFixo.add("intolerância a luz");
        itensFixo.add("intolerância ao barulho");
        itensFixo.add("alterações na visão");
        itensFixo.add("congestão nasal");
        itensFixo.add("tontura");

        for(int i = 0; i < sintomasItens.size(); i++)
        {
            if(sintomasItens.get(i).getId().equals(NULL_OPTION_ID))
                continue;

            String itemTitulo;
            int itemDrawable;
            String itemId;

            Sintoma opcaoItem = sintomasItens.get(i);
            if(itensFixo.contains(opcaoItem.getNome()))
                itemDrawable = drawables[i];
            else
                itemDrawable = R.drawable.option_pattern;

            itemTitulo = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(opcaoItem.getNome());
            itemId = opcaoItem.getId();

            SurveyOptions surveyOptions = new SurveyOptions( itemTitulo, itemDrawable, itemId );
            listAux.add(surveyOptions);
        }

        SurveyOptions itemNenhum = new SurveyOptions( "Nenhum", R.drawable.consequencia_nao_afetou, NULL_OPTION_ID);
        SurveyOptions itemExcluir = new SurveyOptions( "Excluir", R.drawable.null_option);
        SurveyOptions itemAdicionar = new SurveyOptions( "Adicionar", R.drawable.ic_add_option);

        listAux.add(itemNenhum);
        listAux.add(itemAdicionar);
        listAux.add(itemExcluir);

        return(listAux);
    }

    @Override
    public void onClickListener(View view, int position)
    {
        int totalItens = mAdapter.getItemCount();
        int deleteButton = totalItens-1;
        int addBtnPosition = deleteButton-1;
        int opcaoNenhumButton = addBtnPosition-1;

        if( position == addBtnPosition ){
            adicionarOpcao(position);
        }
        else if(position == deleteButton)
        {
            mSurveyManager.setMode(ServiceFactory.getInstance().getServiceSurveyManager(getActivity().getApplicationContext()).alterarEstado(mSurveyManager.getMode(), "CLIQUE NO ÍCONE PARA EXCLUIR.", TITLE_QUESTION, mSurveyTitleTextView, view));
        }
        else
        {
            boolean selectedOption = false;
            if(!mOptionsSelected.contains(mOptions.get(position).getId()))
            {
                if(mSurveyManager.getMode() == MODE_DELETE && position > NR_OPCOES_FIXAS && position != opcaoNenhumButton)
                {
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                } else if(mSurveyManager.getMode() != MODE_DELETE) {

                    if(position == opcaoNenhumButton)
                    {
                        mOptionsSelected.clear();
                        for(int i=0; i < mContentRecyclerView.getChildCount(); i++)
                        {
                            mContentRecyclerView.getChildAt(i).setSelected(false);
                        }
                    } else {
                        for(int i=0; i < mContentRecyclerView.getChildCount(); i++)
                        {
                            View v = mContentRecyclerView.getChildAt(i);
                            if(opcaoNenhumButton == mContentRecyclerView.getChildAdapterPosition(v)) {
                                v.setSelected(false);
                                mOptionsSelected.remove(NULL_OPTION_ID);
                                break;
                            }
                        }
                    }

                    mOptionsSelected.add(mOptions.get(position).getId());
                    selectedOption = true;
                }
            } else {
                mOptionsSelected.remove(mOptions.get(position).getId());
                selectedOption = false;

                if(mSurveyManager.getMode() == MODE_DELETE && position > NR_OPCOES_FIXAS && position != opcaoNenhumButton) {
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                }
            }

            view.setSelected(selectedOption);
            onChangeSelectedOptions(mOptionsSelected);
            mAdapter.setOptionsSelected(mOptionsSelected);
        }
    }

    private boolean excluirOpcao(String id, int position)
    {
        long nrOpcaoRelacionadaCrise = realm.where(Crise.class)
                .equalTo(CriseEntidade.SINTOMAS + "." + SintomaEntidade.ID, id)
                .count();

        if(nrOpcaoRelacionadaCrise == 0)
        {
            try {
                realm.beginTransaction();

                Sintoma opcao = realm.where(Sintoma.class)
                        .equalTo(SintomaEntidade.ID, id)
                        .equalTo(SintomaEntidade.USUARIO+"."+UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();
                opcao.removeFromRealm();

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        } else
        {
            try {
                realm.beginTransaction();

                Crise crise = realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                        .findFirst();

                int sizeSintomas = (crise.getSintomas() == null ? 0 : crise.getSintomas().size());

                if(sizeSintomas > 0)
                {
                    for(int i=0; i < sizeSintomas; i++)
                    {
                        Sintoma sintoma = crise.getSintomas().get(i);
                        if(sintoma.getId().equals(id))
                        {
                            crise.getSintomas().remove(i);

                            sintoma.setDisplay(QUESTION_INACTIVE);
                            realm.copyToRealmOrUpdate(sintoma);
                            break;
                        }
                    }
                } else {
                    Sintoma sintoma = realm.where(Sintoma.class)
                            .equalTo(SintomaEntidade.ID, id)
                            .equalTo(SintomaEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                            .findFirst();

                    sintoma.setDisplay(QUESTION_INACTIVE);
                    realm.copyToRealmOrUpdate(sintoma);
                }

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        }

        realm.refresh();
        mAdapter.removeListItem(position);
        mOptions = mAdapter.getItens();

        return true;
    }

    private void adicionarOpcao(final int position)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_option, null);

        final Dialog dialog = new Dialog(getActivity());
        Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
        Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ServiceFactory.getInstance().getServiceUtils().validarDialogAddOption(dialogView, mResources))
                {
                    EditText optionNameEditText = (EditText) dialogView.findViewById(R.id.optionName);
                    String nomeOpcao = optionNameEditText.getText().toString();

                    Sintoma opcaoSintoma = realm.where(Sintoma.class)
                            .equalTo(SintomaEntidade.USUARIO+"."+UsuarioEntidade.ID, mSurveyManager.getUserId())
                            .equalTo(SintomaEntidade.NOME, nomeOpcao)
                            .findFirst();

                    if(opcaoSintoma != null && opcaoSintoma.getDisplay().equals(QUESTION_ACTIVE))
                    {
                        optionNameEditText.requestFocus();
                        optionNameEditText.setError("O nome desta opção já foi adicionado.");
                    } else {
                        String sintomaId;
                        if(opcaoSintoma == null)
                            sintomaId = ServiceFactory.getInstance().getServiceUtils().generateHash(getActivity().getApplicationContext());
                        else
                            sintomaId = opcaoSintoma.getId();

                        SurveyOptions surveyOptions = new SurveyOptions(nomeOpcao, R.drawable.option_pattern, sintomaId);
                        mAdapter.addListItem(surveyOptions, position-1);
                        mOptions = mAdapter.getItens();

                        Usuario usuario = realm.where(Usuario.class)
                                .equalTo(UsuarioEntidade.ID, mSurveyManager.getUserId())
                                .findFirst();

                        Sintoma sintoma = new Sintoma();
                        sintoma.setId(sintomaId);
                        sintoma.setNome(nomeOpcao);
                        sintoma.setUsuario(usuario);
                        sintoma.setDisplay(QUESTION_ACTIVE);
                        sintoma.setCreated(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());

                        new AddSurveyOptionsTask(getActivity().getApplicationContext(), QUESTION_SINTOMAS_ID).execute(sintoma);

                        dialog.dismiss();
                    }
                }
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_SINTOMAS_ID);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data)
    {
        Crise crise = realm.where(Crise.class).equalTo(CriseEntidade.ID, mSurveyManager.getCriseId()).findFirst();
        RealmList<Sintoma> sintomasOptionsCriseList = crise.getSintomas();

        if(sintomasOptionsCriseList != null){
            for(Sintoma sintoma : sintomasOptionsCriseList)
            {
                String sintomaID = sintoma.getId();
                if(!mOptionsSelected.contains(sintomaID))
                    mOptionsSelected.add(sintomaID);
            }

            onChangeSelectedOptions(mOptionsSelected);
            mAdapter.setOptionsSelected(mOptionsSelected);
        }

        List<Sintoma> sintomaResults = Utils.castList(data, Sintoma.class);

        List<String> sintomaResultsCompare = new ArrayList<>();
        for(int i=0; i < sintomaResults.size(); i++)
        {
            sintomaResultsCompare.add(sintomaResults.get(i).getId());
        }

        for(int i=0; i < mOptionsSelected.size(); i++)
        {
            String sintomaId = mOptionsSelected.get(i);
            if(!sintomaResultsCompare.contains(sintomaId))
            {
                Sintoma opcao = realm.where(Sintoma.class)
                        .equalTo(SintomaEntidade.ID, sintomaId)
                        .equalTo(SintomaEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();

                sintomaResults.add(opcao);
            }
        }

        mOptions = buildOptions(sintomaResults);
        mAdapter.setItens(mOptions);

        mAdapter.setRecyclerViewOnClickListenerHack(this);
        mContentRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onSintomasOptionsUpdated(List<String> selectedOptions);
    }

}
