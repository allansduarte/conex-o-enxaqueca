package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.AddSurveyOptionsTask;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.adapters.SpacesItemDecoration;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptions;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptionsAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Consequencia;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.ConsequenciaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.RecyclerViewOnClickListenerHack;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ConsequenciasFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConsequenciasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConsequenciasFragment extends Fragment
        implements  RecyclerViewOnClickListenerHack,
                    PerfilUsuario,
                    SurveyConstants,
                    LoaderManager.LoaderCallbacks<Object>
{
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "ESTA CRISE AFETOU ALGUMA DE SUAS ATIVIDADES?";
    private static final String TITLE_QUESTION_INDICATOR = "consequências";
    private static final int LOADER_ID = 0;
    private static final int NR_OPCOES_FIXAS = 2;

    private View mViewFragment;
    private RecyclerView mContentRecyclerView;
    private List<SurveyOptions> mOptions;
    private List<String> mOptionsSelected = new ArrayList<>();

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private OnFragmentInteractionListener mListener;

    private Resources mResources;
    private Realm realm;

    private SurveyOptionsAdapter mAdapter;
    private SurveyManager mSurveyManager;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment ConsequenciasFragment.
     */
    public static ConsequenciasFragment newInstance(SurveyManager surveyManager) {
        ConsequenciasFragment fragment = new ConsequenciasFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public ConsequenciasFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mViewFragment = inflater.inflate(R.layout.fragment_consequencias, container, false);
        initViews();

        return mViewFragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mSurveyTitleTextView.setBackgroundColor(mResources.getColor(R.color.verdeClaro));
        mSurveyManager.setMode(MODE_INSERT);

        getLoaderManager().destroyLoader(LOADER_ID);
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
    }

    private void initViews()
    {
        mContentRecyclerView = (RecyclerView) mViewFragment.findViewById(R.id.content_recycler_view);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);
        mResources = getActivity().getResources();

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mContentRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        mContentRecyclerView.setLayoutManager(gridLayoutManager);

        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration();
        spacesItemDecoration.setBottom(20);
        mContentRecyclerView.addItemDecoration(spacesItemDecoration);

        mSurveyIndicatorTextView.setText(TITLE_QUESTION_INDICATOR);
        mSurveyIndicatorTextView.setVisibility(View.VISIBLE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);

        if(mSurveyManager.isUpdateMode())
        {
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
            mNextButton.setVisibility(View.GONE);
        } else {
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
            mNextButton.setVisibility(View.VISIBLE);
        }
    }

    public void onChangeSelectedOptions(List<String> selectedOptions) {
        if (mListener != null)
        {
            mListener.onConsequenciaOptionsUpdated(selectedOptions);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mAdapter = new SurveyOptionsAdapter(getActivity().getApplicationContext(), new ArrayList<SurveyOptions>());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public List<SurveyOptions> buildOptions(List<Consequencia> consequenciaItens){
        int[] drawables = new int[]{R.drawable.consequencia_faltar_trabalho, R.drawable.consequencia_faltar_escola,
                R.drawable.consequencia_atividades_sociais};
        List<SurveyOptions> listAux = new ArrayList<>();

        List<String> itensFixo = new ArrayList<>();
        itensFixo.add("falta no trabalho");
        itensFixo.add("falta na escola");
        itensFixo.add("atividades sociais");

        for(int i = 0; i < consequenciaItens.size(); i++)
        {
            if(consequenciaItens.get(i).getId().equals(NULL_OPTION_ID))
                continue;

            String itemTitulo;
            int itemDrawable;
            String itemId;

            Consequencia opcaoItem = consequenciaItens.get(i);
            if(itensFixo.contains(opcaoItem.getNome()))
                itemDrawable = drawables[i];
            else
                itemDrawable = R.drawable.option_pattern;

            itemTitulo = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(opcaoItem.getNome());
            itemId = opcaoItem.getId();

            SurveyOptions surveyOptions = new SurveyOptions( itemTitulo, itemDrawable, itemId );
            listAux.add(surveyOptions);
        }

        SurveyOptions itemNenhum = new SurveyOptions( "Não afetou", R.drawable.consequencia_nao_afetou, NULL_OPTION_ID);
        SurveyOptions itemExcluir = new SurveyOptions( "Excluir", R.drawable.null_option);
        SurveyOptions itemAdicionar = new SurveyOptions( "Adicionar", R.drawable.ic_add_option);

        listAux.add(itemNenhum);
        listAux.add(itemAdicionar);
        listAux.add(itemExcluir);
        return(listAux);
    }

    @Override
    public void onClickListener(View view, int position) {
        int totalItens = mAdapter.getItemCount();
        int deleteButton = totalItens-1;
        int addBtnPosition = deleteButton-1;
        int opcaoNenhumButton = addBtnPosition-1;

        if( position == addBtnPosition )
            onClickAddOption(opcaoNenhumButton);
        else if(position == deleteButton)
            mSurveyManager.setMode(ServiceFactory.getInstance().getServiceSurveyManager(getActivity().getApplicationContext()).alterarEstado(mSurveyManager.getMode(), "CLIQUE NO ÍCONE PARA EXCLUIR.", TITLE_QUESTION, mSurveyTitleTextView, view));
        else
        {
            boolean selectedOption = false;
            if(!mOptionsSelected.contains(mOptions.get(position).getId()) )
            {
                if(mSurveyManager.getMode() == MODE_DELETE && position > NR_OPCOES_FIXAS && position != opcaoNenhumButton)
                {
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                } else if(mSurveyManager.getMode() != MODE_DELETE) {
                    if(position == opcaoNenhumButton) {
                        mOptionsSelected.clear();
                        for(int i=0; i < mContentRecyclerView.getChildCount(); i++)
                        {
                            mContentRecyclerView.getChildAt(i).setSelected(false);
                        }
                    } else {
                        for(int i=0; i < mContentRecyclerView.getChildCount(); i++)
                        {
                            View v = mContentRecyclerView.getChildAt(i);
                            if(opcaoNenhumButton == mContentRecyclerView.getChildAdapterPosition(v)) {
                                v.setSelected(false);
                                mOptionsSelected.remove(NULL_OPTION_ID);
                                break;
                            }
                        }
                    }
                    mOptionsSelected.add(mOptions.get(position).getId());
                    selectedOption = true;
                }
            } else{
                mOptionsSelected.remove(mOptions.get(position).getId());
                selectedOption = false;

                if(mSurveyManager.getMode() == MODE_DELETE && position > NR_OPCOES_FIXAS && position != opcaoNenhumButton){
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                }
            }

            view.setSelected(selectedOption);
            onChangeSelectedOptions(mOptionsSelected);
            mAdapter.setOptionsSelected(mOptionsSelected);
        }
    }

    private boolean excluirOpcao(String id, int position)
    {
        long nrOpcaoRelacionadaCrise = realm.where(Crise.class)
                .equalTo(CriseEntidade.CONSEQUENCIAS + "." + ConsequenciaEntidade.ID, id)
                .count();

        if(nrOpcaoRelacionadaCrise == 0)
        {
            try {
                realm.beginTransaction();

                Consequencia opcao = realm.where(Consequencia.class)
                        .equalTo(ConsequenciaEntidade.ID, id)
                        .equalTo(ConsequenciaEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();
                opcao.removeFromRealm();

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        } else
        {
            try {
                realm.beginTransaction();

                Crise crise = realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                        .findFirst();

                int sizeConsequencias = (crise.getConsequencias() == null ? 0 : crise.getConsequencias().size());

                if(sizeConsequencias > 0)
                {
                    for(int i=0; i < sizeConsequencias; i++)
                    {
                        Consequencia consequencia = crise.getConsequencias().get(i);
                        if(consequencia.getId().equals(id))
                        {
                            crise.getConsequencias().remove(i);

                            consequencia.setDisplay(QUESTION_INACTIVE);
                            realm.copyToRealmOrUpdate(consequencia);
                            break;
                        }
                    }
                } else {
                    Consequencia consequencia = realm.where(Consequencia.class)
                            .equalTo(ConsequenciaEntidade.ID, id)
                            .equalTo(ConsequenciaEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                            .findFirst();

                    consequencia.setDisplay(QUESTION_INACTIVE);
                    realm.copyToRealmOrUpdate(consequencia);
                }

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        }

        realm.refresh();
        mAdapter.removeListItem(position);
        mOptions = mAdapter.getItens();

        return true;
    }

    private void onClickAddOption(final int addBtnPosition)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_option, null);

        final Dialog dialog = new Dialog(getActivity());
        Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
        Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ServiceFactory.getInstance().getServiceUtils().validarDialogAddOption(dialogView, mResources))
                {
                    EditText optionNameEditText = (EditText) dialogView.findViewById(R.id.optionName);
                    String nomeOpcao = optionNameEditText.getText().toString();

                    Consequencia opcaoConsequencia = realm.where(Consequencia.class)
                            .equalTo(ConsequenciaEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                            .equalTo(ConsequenciaEntidade.NOME, nomeOpcao)
                            .findFirst();

                    if(opcaoConsequencia != null && opcaoConsequencia.getDisplay().equals(QUESTION_ACTIVE))
                    {
                        optionNameEditText.requestFocus();
                        optionNameEditText.setError("O nome desta opção já foi adicionado.");
                    } else {
                        String consequenciaId;

                        if(opcaoConsequencia == null)
                            consequenciaId = ServiceFactory.getInstance().getServiceUtils().generateHash(getActivity().getApplicationContext());
                        else
                            consequenciaId = opcaoConsequencia.getId();

                        SurveyOptions surveyOptions = new SurveyOptions(nomeOpcao, R.drawable.option_pattern, consequenciaId);
                        mAdapter.addListItem(surveyOptions, addBtnPosition);
                        mOptions = mAdapter.getItens();

                        Usuario usuario = realm.where(Usuario.class)
                                .equalTo(UsuarioEntidade.ID, mSurveyManager.getUserId())
                                .findFirst();

                        Consequencia consequencia = new Consequencia();
                        consequencia.setId(consequenciaId);
                        consequencia.setNome(nomeOpcao);
                        consequencia.setUsuario(usuario);
                        consequencia.setDisplay(QUESTION_ACTIVE);
                        consequencia.setCreated(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());

                        new AddSurveyOptionsTask(getActivity().getApplicationContext(), QUESTION_CONSEQUENCIAS_ID).execute(consequencia);

                        dialog.dismiss();
                    }
                }
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_CONSEQUENCIAS_ID);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        Crise crise =  realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                .findFirst();

        RealmList<Consequencia> consequenciaList = crise.getConsequencias();

        if(consequenciaList != null){
            for(Consequencia opcao : consequenciaList)
            {
                if(!mOptionsSelected.contains(opcao.getId()))
                    mOptionsSelected.add(opcao.getId());
            }
            onChangeSelectedOptions(mOptionsSelected);
            mAdapter.setOptionsSelected(mOptionsSelected);
        }

        List<Consequencia> consequenciaResults = Utils.castList(data, Consequencia.class);

        List<String> consequenciaResultsCompare = new ArrayList<>();
        for(int i=0; i < consequenciaResults.size(); i++)
        {
            consequenciaResultsCompare.add(consequenciaResults.get(i).getId());
        }

        for(int i=0; i < mOptionsSelected.size(); i++)
        {
            String consequenciaId = mOptionsSelected.get(i);
            if(!consequenciaResultsCompare.contains(consequenciaId))
            {
                Consequencia opcao = realm.where(Consequencia.class)
                                          .equalTo(ConsequenciaEntidade.ID, consequenciaId)
                                          .equalTo(ConsequenciaEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                                          .findFirst();

                consequenciaResults.add(opcao);
            }
        }

        mOptions = buildOptions(consequenciaResults);
        mAdapter.setItens(mOptions);

        mAdapter.setRecyclerViewOnClickListenerHack(this);
        mContentRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {
        mAdapter.setItens(new ArrayList<SurveyOptions>());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onConsequenciaOptionsUpdated(List<String> selectedOptions);
    }

}
