package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentSeveridadeDorListener} interface
 * to handle interaction events.
 * Use the {@link SeveridadeDorFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SeveridadeDorFragment extends Fragment implements SurveyConstants, LoaderManager.LoaderCallbacks<Object> {

    private static final int LOADER_ID = 2;
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "CLASSIFIQUE SUA DOR DE 1 A 10:";
    private static final String TITLE_QUESTION_INDICATOR = "severidade da dor";
    private static final int INTENSIDADE_MAX_VALUE = 10;
    private static final int INTENSIDADE_MIN_VALUE = 1;

    private OnFragmentSeveridadeDorListener mListener;

    private View mViewFragment;
    private TextView mTitleIndicatorTextView;
    private TextView mDescriptionIndicatorTextView;
    private ImageView mEmoticonImageView;
    private ImageView mIncrementarIntensidadeImageView;
    private ImageView mDecrementarIntensidadeImageView;
    private EditText mIntensidadeEditText;
    private int mNrPain=3;
    private int mNrOldPain=3;
    private Animation mAnimation;

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;
    private SurveyManager mSurveyManager;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment SeveridadeDorFragment.
     */
    public static SeveridadeDorFragment newInstance(SurveyManager surveyManager)
    {
        SeveridadeDorFragment fragment = new SeveridadeDorFragment();
        Bundle args = new Bundle();

        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public SeveridadeDorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mViewFragment  = inflater.inflate(R.layout.fragment_severidade_dor, container, false);
        initViews();

        getLoaderManager().initLoader(LOADER_ID, null, this);

        return mViewFragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getLoaderManager().destroyLoader(LOADER_ID);
        mNrPain = mNrOldPain = 3;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentSeveridadeDorListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentSeveridadeDorListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_INTENSIDADE_ID, mSurveyManager.getCriseId());
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {
        Crise crise = (Crise) data;

        if(crise != null)
        {
            mAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            if(crise.getIntensidade() > 0)
            {
                setBehaviorIntensidade(crise.getIntensidade());
            } else {
                setBehaviorIntensidade(mNrPain);
            }
        } else onDataChange(mNrPain);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    private void initViews()
    {
        mTitleIndicatorTextView = (TextView) mViewFragment.findViewById(R.id.titleIndicatorTextView);
        mDescriptionIndicatorTextView = (TextView) mViewFragment.findViewById(R.id.descriptionIndicatorTextView);
        mEmoticonImageView = (ImageView) mViewFragment.findViewById(R.id.emoticonImageView);
        mDecrementarIntensidadeImageView = (ImageView) mViewFragment.findViewById(R.id.decrementarIntensidadeImageView);
        mIncrementarIntensidadeImageView = (ImageView) mViewFragment.findViewById(R.id.incrementarIntensidadeImageView);
        mIntensidadeEditText = (EditText) mViewFragment.findViewById(R.id.intensidadeEditText);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mSurveyIndicatorTextView.setText(TITLE_QUESTION_INDICATOR);
        mSurveyIndicatorTextView.setVisibility(View.VISIBLE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);

        if(mSurveyManager.isUpdateMode())
        {
            mNextButton.setVisibility(View.GONE);
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
        } else {
            mNextButton.setVisibility(View.VISIBLE);
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
        }

        mIncrementarIntensidadeImageView.setOnClickListener(onClickChangeIntensidade);
        mDecrementarIntensidadeImageView.setOnClickListener(onClickChangeIntensidade);
    }

    public void onDataChange(int data) {
        if (mListener != null) {
            mListener.onChangeIntensidadeDor(data);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentSeveridadeDorListener {
        void onChangeIntensidadeDor(int data);
    }

    View.OnClickListener onClickChangeIntensidade = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int value = Integer.parseInt(mIntensidadeEditText.getText().toString());

            switch (v.getId())
            {
                case R.id.decrementarIntensidadeImageView:
                    if(value > INTENSIDADE_MIN_VALUE){
                        mAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right);
                        mNrOldPain = value;
                        value -= 1;
                    }
                    break;
                case R.id.incrementarIntensidadeImageView:
                    if(value < INTENSIDADE_MAX_VALUE){
                        mAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_left);
                        mNrOldPain = value;
                        value += 1;
                    }
                    break;
            }

            setBehaviorIntensidade(value);
        }
    };

    private void setBehaviorIntensidade(int value)
    {
        mNrPain = value;
        onDataChange(value);
        updatePainValues();
    }

    private void updatePainValues()
    {

        mIntensidadeEditText.setText(String.valueOf(mNrPain));
        if(mNrPain < INTENSIDADE_MAX_VALUE && mNrPain > INTENSIDADE_MIN_VALUE)
            mIntensidadeEditText.startAnimation(mAnimation);

        if(mudouIntensidadeDor(1, 2))
            setBehaviorDescricaoIntensidade("DÓI POUCO", "A dor está presente, mas não limita nenhuma de minhas atividades", R.drawable.ic_intensidade_dor_1);
        else if(mudouIntensidadeDor(3, 4))
            setBehaviorDescricaoIntensidade("DÓI UM POUCO MAIS", "Ainda posso fazer minhas atividades, mas com dificuldades", R.drawable.ic_intensidade_dor_2);
        else if(mudouIntensidadeDor(5, 6))
            setBehaviorDescricaoIntensidade("DÓI BASTANTE", "Não é possível fazer algumas atividades", R.drawable.ic_intensidade_dor_3);
        else if(mudouIntensidadeDor(7, 8))
            setBehaviorDescricaoIntensidade("DOR SEVERA", "Não é possível fazer muitas atividades", R.drawable.ic_intensidade_dor_4);
        else if(mudouIntensidadeDor(9, 10))
            setBehaviorDescricaoIntensidade("DOR INSUPORTÁVEL", "Não consigo fazer qualquer atividade por causa da dor", R.drawable.ic_intensidade_dor_5);
    }

    private void setBehaviorDescricaoIntensidade(String titulo, String descricao, int drawable)
    {
        mTitleIndicatorTextView.setText(titulo);
        mDescriptionIndicatorTextView.setText(descricao);
        setImageDrawable(drawable);
    }

    private boolean mudouIntensidadeDor(int num1, int num2)
    {
        boolean change = false;
        boolean mudouIntensidade = mNrOldPain != num1 && mNrOldPain != num2;

        if(mNrPain == num1 && mudouIntensidade) change = true;
        if(mNrPain == num2  && mudouIntensidade) change = true;

        return change;
    }

    private void setImageDrawable(int drawable)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mEmoticonImageView.setImageDrawable(getResources().getDrawable(drawable, getActivity().getTheme()));
        } else {
            mEmoticonImageView.setImageDrawable(getResources().getDrawable(drawable));
        }

        mAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
        mEmoticonImageView.startAnimation(mAnimation);
    }
}
