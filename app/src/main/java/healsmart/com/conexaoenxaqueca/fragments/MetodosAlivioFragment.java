package healsmart.com.conexaoenxaqueca.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.AddSurveyOptionsTask;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.SurveyOpcoesLoader;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.adapters.SpacesItemDecoration;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptions;
import healsmart.com.conexaoenxaqueca.adapters.SurveyOptionsAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivio;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivioCrise;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.MetodoAlivioEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.RecyclerViewOnClickListenerHack;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MetodosAlivioFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MetodosAlivioFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MetodosAlivioFragment extends Fragment
        implements  RecyclerViewOnClickListenerHack,
                    PerfilUsuario,
                    SurveyConstants,
                    LoaderManager.LoaderCallbacks<Object>
{
    private static final String ARG_GERENCIADOR_QUESTIONARIO = "param1";
    private static final String TITLE_QUESTION = "FOI UTILIZADO ALGUM MÉTODO DE ALÍVIO?";
    private static final String TITLE_QUESTION_INDICATOR = "métodos de alívio";
    private static final int LOADER_ID = 0;

    private View mViewFragment;
    private RecyclerView mContentRecyclerView;
    private List<SurveyOptions> mOptions;
    private List<String> mOptionsSelected = new ArrayList<>();

    private TextView mSurveyTitleTextView;
    private TextView mSurveyIndicatorTextView;
    private Button mNextButton;
    private Button mSuspendSurveyButton;
    private Button mConfirmarButton;

    private OnFragmentInteractionListener mListener;

    private Resources mResources;
    private Realm realm;

    private SurveyOptionsAdapter mAdapter;
    private SurveyManager mSurveyManager;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param surveyManager Parameter 1.
     * @return A new instance of fragment MetodosAlivioFragment.
     */
    public static MetodosAlivioFragment newInstance(SurveyManager surveyManager) {
        MetodosAlivioFragment fragment = new MetodosAlivioFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_GERENCIADOR_QUESTIONARIO, surveyManager);

        fragment.setArguments(args);
        return fragment;
    }

    public MetodosAlivioFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            mSurveyManager = getArguments().getParcelable(ARG_GERENCIADOR_QUESTIONARIO);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mViewFragment = inflater.inflate(R.layout.fragment_metodos_alivio, container, false);
        initViews();

        //getLoaderManager().initLoader(LOADER_ID, null, this);

        return mViewFragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mSurveyTitleTextView.setBackgroundColor(mResources.getColor(R.color.verdeClaro));
        mSurveyManager.setMode(MODE_INSERT);

        getLoaderManager().destroyLoader(LOADER_ID);
    }

    @Override
    public void onStart() {
        super.onStart();
        realm = Realm.getDefaultInstance();
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public void onStop() {
        super.onStop();
        realm.close();
        getLoaderManager().destroyLoader(LOADER_ID);
    }

    private void initViews()
    {
        mContentRecyclerView = (RecyclerView) mViewFragment.findViewById(R.id.content_recycler_view);

        mSurveyTitleTextView = (TextView) getActivity().findViewById(R.id.surveyTitleTextView);
        mSurveyIndicatorTextView = (TextView) getActivity().findViewById(R.id.surveyIndicatorTextView);

        mNextButton = (Button) getActivity().findViewById(R.id.nextButton);
        mSuspendSurveyButton = (Button) getActivity().findViewById(R.id.suspendSurveyButton);
        mConfirmarButton = (Button) getActivity().findViewById(R.id.confirmarButton);
        mResources = getActivity().getResources();

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mContentRecyclerView.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3, GridLayoutManager.VERTICAL, false);
        mContentRecyclerView.setLayoutManager(gridLayoutManager);

        SpacesItemDecoration spacesItemDecoration = new SpacesItemDecoration();
        spacesItemDecoration.setBottom(20);
        mContentRecyclerView.addItemDecoration(spacesItemDecoration);

        mSurveyIndicatorTextView.setText(TITLE_QUESTION_INDICATOR);
        mSurveyIndicatorTextView.setVisibility(View.VISIBLE);
        mSurveyTitleTextView.setText(TITLE_QUESTION);

        if(mSurveyManager.isUpdateMode())
        {
            mNextButton.setVisibility(View.GONE);
            mSuspendSurveyButton.setVisibility(View.GONE);
            mConfirmarButton.setVisibility(View.VISIBLE);
        } else {
            mNextButton.setVisibility(View.VISIBLE);
            mSuspendSurveyButton.setVisibility(View.VISIBLE);
            mConfirmarButton.setVisibility(View.GONE);
        }
    }

    public void onChangeSelectedOptions(List<String> selectedOptions) {
        if (mListener != null)
        {
            mListener.onMetodoAlivioOptionsUpdated(selectedOptions);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mAdapter = new SurveyOptionsAdapter(getActivity().getApplicationContext(), new ArrayList<SurveyOptions>());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        mAdapter = null;
        mOptionsSelected=null;
    }

    @Override
    public void onClickListener(View view, int position) {
        int totalItens = mAdapter.getItemCount();
        int deleteButton = totalItens-1;
        int addBtnPosition = deleteButton-1;

        if( position == addBtnPosition )
            onClickAddOption(position);
        else if(position == deleteButton)
            mSurveyManager.setMode(ServiceFactory.getInstance().getServiceSurveyManager(getActivity().getApplicationContext()).alterarEstado(mSurveyManager.getMode(), "CLIQUE NO ÍCONE PARA EXCLUIR.", TITLE_QUESTION, mSurveyTitleTextView, view));
        else
        {
            boolean selectedOption = false;
            if(!mOptionsSelected.contains(mOptions.get(position).getId()) )
            {
                if(mSurveyManager.getMode() == MODE_DELETE && position > 5)
                {
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                } else if(mSurveyManager.getMode() != MODE_DELETE) {
                    mOptionsSelected.add(mOptions.get(position).getId());
                    selectedOption = true;
                }
            } else{
                mOptionsSelected.remove(mOptions.get(position).getId());
                selectedOption = false;

                if(mSurveyManager.getMode() == MODE_DELETE && position > 5) {
                    SurveyOptions option = mOptions.get(position);
                    excluirOpcao(option.getId(), position);
                }
            }

            view.setSelected(selectedOption);
            onChangeSelectedOptions(mOptionsSelected);
            mAdapter.setOptionsSelected(mOptionsSelected);
        }
    }

    private boolean excluirOpcao(String id, int position)
    {
        long nrOpcaoRelacionadaCrise = realm.where(Crise.class)
                .equalTo(CriseEntidade.METODO_ALIVIO + "." + MetodoAlivioEntidade.ID, id)
                .count();

        if(nrOpcaoRelacionadaCrise == 0)
        {
            try {
                realm.beginTransaction();

                MetodoAlivio opcao = realm.where(MetodoAlivio.class)
                        .equalTo(MetodoAlivioEntidade.ID, id)
                        .equalTo(MetodoAlivioEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();
                opcao.removeFromRealm();

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        } else
        {
            try {
                realm.beginTransaction();

                Crise crise = realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                        .findFirst();

                int sizeMetodoAlivioCrise = (crise.getMetodos() == null ? 0 : crise.getMetodos().size());

                if(sizeMetodoAlivioCrise > 0)
                {
                    for(int i=0; i < sizeMetodoAlivioCrise; i++)
                    {
                        MetodoAlivio metodo = crise.getMetodos().get(i).getMetodoAlivio();
                        if(metodo.getId().equals(id))
                        {
                            crise.getMetodos().remove(i);

                            metodo.setDisplay(QUESTION_INACTIVE);
                            realm.copyToRealmOrUpdate(metodo);
                            break;
                        }
                    }
                } else {
                    MetodoAlivio metodo = realm.where(MetodoAlivio.class)
                            .equalTo(MetodoAlivioEntidade.ID, id)
                            .equalTo(MetodoAlivioEntidade.USUARIO+"."+ UsuarioEntidade.ID, mSurveyManager.getUserId())
                            .findFirst();

                    metodo.setDisplay(QUESTION_INACTIVE);
                    realm.copyToRealmOrUpdate(metodo);
                }

                realm.commitTransaction();
            } catch (Exception e) {
                realm.cancelTransaction();
            }
        }

        realm.refresh();
        mAdapter.removeListItem(position);
        mOptions = mAdapter.getItens();

        return true;
    }

    private void  onClickAddOption(final int addBtnPosition)
    {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_option, null);

        final Dialog dialog = new Dialog(getActivity());
        Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
        Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ServiceFactory.getInstance().getServiceUtils().validarDialogAddOption(dialogView, mResources))
                {
                    EditText optionNameEditText = (EditText) dialogView.findViewById(R.id.optionName);
                    String nomeOpcao = optionNameEditText.getText().toString();

                    MetodoAlivio opcaoMetodoAlivio = realm.where(MetodoAlivio.class)
                                                          .equalTo(MetodoAlivioEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                                                          .equalTo(MetodoAlivioEntidade.NOME, nomeOpcao)
                                                          .findFirst();

                    if(opcaoMetodoAlivio != null && opcaoMetodoAlivio.getDisplay().equals(QUESTION_ACTIVE))
                    {
                        optionNameEditText.requestFocus();
                        optionNameEditText.setError("O nome desta opção já foi adicionado.");
                    } else {
                        String metodoAlivioId;

                        if(opcaoMetodoAlivio == null)
                            metodoAlivioId = ServiceFactory.getInstance().getServiceUtils().generateHash(getActivity().getApplicationContext());
                        else
                            metodoAlivioId = opcaoMetodoAlivio.getId();

                        SurveyOptions surveyOptions = new SurveyOptions(nomeOpcao, R.drawable.option_pattern, metodoAlivioId);
                        mAdapter.addListItem(surveyOptions, addBtnPosition);
                        mOptions = mAdapter.getItens();

                        Usuario usuario = realm.where(Usuario.class)
                                .equalTo(UsuarioEntidade.ID, mSurveyManager.getUserId())
                                .findFirst();

                        MetodoAlivio metodoAlivio = new MetodoAlivio();
                        metodoAlivio.setId(metodoAlivioId);
                        metodoAlivio.setNome(nomeOpcao);
                        metodoAlivio.setUsuario(usuario);
                        metodoAlivio.setDisplay(QUESTION_ACTIVE);
                        metodoAlivio.setCreated(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());

                        new AddSurveyOptionsTask(getActivity().getApplicationContext(), QUESTION_METODO_ALIVIO_ID).execute(metodoAlivio);

                        dialog.dismiss();
                    }
                }
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();
    }

    public List<SurveyOptions> buildOptions(List<MetodoAlivio> metodoItens){
        int[] drawables = new int[]{R.drawable.metodo_alivio_repouso, R.drawable.metodo_alivio_relaxamento,
                R.drawable.metodo_alivio_quarto_escuro, R.drawable.metodo_alivio_massagem,
                R.drawable.metodo_alivio_banho, R.drawable.metodo_alivio_compressa};
        List<SurveyOptions> listAux = new ArrayList<>();

        List<String> itensFixo = new ArrayList<>();
        itensFixo.add("repouso");
        itensFixo.add("relaxamento");
        itensFixo.add("quarto escuro");
        itensFixo.add("massagem");
        itensFixo.add("banho");
        itensFixo.add("compressas");

        for(int i = 0; i < metodoItens.size(); i++)
        {
            String itemTitulo;
            int itemDrawable;
            String itemId;

            MetodoAlivio item = metodoItens.get(i);
            if(itensFixo.contains(item.getNome()))
            {
                itemDrawable = drawables[i];
            } else{
                itemDrawable = R.drawable.option_pattern;
            }

            itemTitulo = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(item.getNome());
            itemId = item.getId();

            SurveyOptions surveyOptions = new SurveyOptions( itemTitulo, itemDrawable, itemId );
            listAux.add(surveyOptions);
        }

        SurveyOptions itemAdicionar = new SurveyOptions( "Adicionar", R.drawable.ic_add_option);
        SurveyOptions itemExcluir = new SurveyOptions( "Excluir", R.drawable.null_option);

        listAux.add(itemAdicionar);
        listAux.add(itemExcluir);
        return(listAux);
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        return new SurveyOpcoesLoader(getActivity().getApplicationContext(), QUESTION_METODO_ALIVIO_ID);
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data)
    {
        Crise crise =  realm.where(Crise.class)
                            .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                            .findFirst();

        RealmList<MetodoAlivioCrise> metodoAlivioCriseList = crise.getMetodos();
        int sizeMetodoList = (metodoAlivioCriseList == null ? 0 : metodoAlivioCriseList.size());

        if(sizeMetodoList > 0)
        {
            for(MetodoAlivioCrise metodoAlivioCrise : metodoAlivioCriseList)
            {
                String metodoID = metodoAlivioCrise.getMetodoAlivio().getId();
                if(!mOptionsSelected.contains(metodoID))
                {
                    mOptionsSelected.add(metodoID);
                }
            }

            onChangeSelectedOptions(mOptionsSelected);
            mAdapter.setOptionsSelected(mOptionsSelected);
        }

        List<MetodoAlivio> metodoResults = Utils.castList(data, MetodoAlivio.class);

        List<String> metodoAlivioResultsCompare = new ArrayList<>();
        for(int i=0; i < metodoResults.size(); i++)
        {
            metodoAlivioResultsCompare.add(metodoResults.get(i).getId());
        }

        for(int i=0; i < sizeMetodoList; i++)
        {
            String metodoID = mOptionsSelected.get(i);
            if(!metodoAlivioResultsCompare.contains(metodoID))
            {
                MetodoAlivio metodoAlivio = realm.where(MetodoAlivio.class)
                        .equalTo(MetodoAlivioEntidade.ID, metodoID)
                        .equalTo(MetodoAlivioEntidade.USUARIO + "." + UsuarioEntidade.ID, mSurveyManager.getUserId())
                        .findFirst();

                metodoResults.add(metodoAlivio);
            }
        }

        mOptions = buildOptions(metodoResults);
        mAdapter.setItens(mOptions);

        mAdapter.setRecyclerViewOnClickListenerHack(this);
        mContentRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {
        mAdapter.setItens(new ArrayList<SurveyOptions>());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onMetodoAlivioOptionsUpdated(List<String> selectedOptions);
    }

}
