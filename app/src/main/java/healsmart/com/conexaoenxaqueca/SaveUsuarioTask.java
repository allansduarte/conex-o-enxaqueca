package healsmart.com.conexaoenxaqueca;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import healsmart.com.conexaoenxaqueca.activities.MainActivity;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import io.realm.Realm;


public class SaveUsuarioTask extends AsyncTask<Object, Object, Usuario> implements PerfilUsuario
{

    private Context mContext;

    public SaveUsuarioTask(Context context)
    {
        mContext = context;
    }

    @Override
    protected Usuario doInBackground(Object[] params) {
        Usuario usuarioParam = (Usuario) params[0];

        Realm realm = Realm.getDefaultInstance();
        try{
            realm.beginTransaction();

            Usuario usuario = realm.createObject(Usuario.class);
            usuario.setId(usuarioParam.getId());
            usuario.setEmail(usuarioParam.getEmail());
            usuario.setSenha(usuarioParam.getSenha());
            usuario.setNome(usuarioParam.getNome());
            usuario.setSobrenome(usuarioParam.getSobrenome());
            usuario.setDataNascimento(usuarioParam.getDataNascimento());
            usuario.setGenero(usuarioParam.getGenero());

            realm.copyToRealmOrUpdate(usuario);
            realm.commitTransaction();
        }
        catch(Exception e){
            realm.cancelTransaction();
            //tratar
        }

        realm.refresh();
        realm.close();

        return usuarioParam;
    }

    @Override
    protected void onPostExecute(Usuario result)
    {
        SharedPreferences prefs = mContext.getSharedPreferences(USUARIO_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(USUARIO_PREFS_LOGIN, true);
        editor.putString(USUARIO_PREFS_USUARIO_ID, result.getId());
        editor.apply();

        ServiceFactory.getInstance().getServiceUtils().redirectTo(mContext, MainActivity.class);
    }
}
