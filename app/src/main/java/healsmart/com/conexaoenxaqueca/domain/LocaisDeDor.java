package healsmart.com.conexaoenxaqueca.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by allan on 21/12/15.
 */
public class LocaisDeDor extends RealmObject
{
    @PrimaryKey
    private int id;

    private String nome;
    private Usuario usuario;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public static LocaisDeDor copy(LocaisDeDor locaisDeDor)
    {
        LocaisDeDor locaisDeDorCopy = new LocaisDeDor();
        locaisDeDorCopy.setId(locaisDeDor.getId());
        locaisDeDorCopy.setNome(locaisDeDor.getNome());
        locaisDeDorCopy.setUsuario(Usuario.copy(locaisDeDor.getUsuario()));

        return locaisDeDorCopy;
    }

    public static int quantidade = 19;
}
