package healsmart.com.conexaoenxaqueca.domain;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by allan on 26/11/15.
 */
public class GatilhoCrise extends RealmObject
{
    @PrimaryKey
    private String id;

    private Gatilho gatilho;
    private RealmList<Bebida> bebidas;
    private RealmList<Comida> comidas;
    private RealmList<EsforcoFisico> esforcoFisicos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RealmList<Bebida> getBebidas() {
        return bebidas;
    }

    public void setBebidas(RealmList<Bebida> bebidas) {
        this.bebidas = bebidas;
    }

    public RealmList<Comida> getComidas() {
        return comidas;
    }

    public void setComidas(RealmList<Comida> comidas) {
        this.comidas = comidas;
    }

    public RealmList<EsforcoFisico> getEsforcoFisicos() {
        return esforcoFisicos;
    }

    public void setEsforcoFisicos(RealmList<EsforcoFisico> esforcoFisicos) {
        this.esforcoFisicos = esforcoFisicos;
    }

    public Gatilho getGatilho() {
        return gatilho;
    }

    public void setGatilho(Gatilho gatilho) {
        this.gatilho = gatilho;
    }

    public static GatilhoCrise copy(GatilhoCrise gatilhoCrise)
    {
        GatilhoCrise gatilhoCriseCopy = new GatilhoCrise();
        gatilhoCriseCopy.setId(gatilhoCrise.getId());

        gatilhoCriseCopy.setGatilho(Gatilho.copy(gatilhoCrise.getGatilho()));

        RealmList<Bebida> bebidas = new RealmList<>();
        for(Bebida bebida : gatilhoCrise.getBebidas())
        {
            bebidas.add(Bebida.copy(bebida));
        }
        gatilhoCriseCopy.setBebidas(bebidas);

        RealmList<Comida> comidas = new RealmList<>();
        for(Comida comida : gatilhoCrise.getComidas())
        {
            comidas.add(Comida.copy(comida));
        }
        gatilhoCriseCopy.setComidas(comidas);

        RealmList<EsforcoFisico> esforcoFisicos = new RealmList<>();
        for(EsforcoFisico esforcoFisico : gatilhoCrise.getEsforcoFisicos())
        {
            esforcoFisicos.add(EsforcoFisico.copy(esforcoFisico));
        }
        gatilhoCriseCopy.setEsforcoFisicos(esforcoFisicos);

        return gatilhoCriseCopy;
    }
}
