package healsmart.com.conexaoenxaqueca.domain;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Medicamento extends RealmObject
{
    @PrimaryKey
    private String id;

    private String nome;
    private int quantidade;
    private boolean funcionou;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.trim().toLowerCase();
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public boolean isFuncionou() {
        return funcionou;
    }

    public void setFuncionou(boolean funcionou) {
        this.funcionou = funcionou;
    }

    public static Medicamento copy(Medicamento medicamento)
    {
        Medicamento medicamentoCopy = new Medicamento();
        medicamentoCopy.setId(medicamento.getId());
        medicamentoCopy.setNome(medicamento.getNome());
        medicamentoCopy.setQuantidade(medicamento.getQuantidade());
        medicamentoCopy.setFuncionou(medicamento.isFuncionou());

        return medicamentoCopy;
    }

    public static String toJSON(Medicamento medicamento){

        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("id", medicamento.getId());
            jsonObject.put("nome", medicamento.getNome());
            jsonObject.put("quantidade", medicamento.getQuantidade());
            jsonObject.put("funcionou", medicamento.isFuncionou());

            return jsonObject.toString();
        } catch (JSONException e) {
            return "";
        }

    }

    public static Medicamento fromJson(String json){

        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(json);
        } catch (JSONException e) {
            jsonObject = null;
        }

        Medicamento medicamento = null;

        if(jsonObject != null)
        {
            medicamento = new Medicamento();
            medicamento.setId(jsonObject.optString("id"));
            medicamento.setNome(jsonObject.optString("nome"));
            medicamento.setQuantidade(jsonObject.optInt("quantidade"));
            medicamento.setFuncionou(jsonObject.optBoolean("funcionou"));
        }

        return medicamento;
    }
}
