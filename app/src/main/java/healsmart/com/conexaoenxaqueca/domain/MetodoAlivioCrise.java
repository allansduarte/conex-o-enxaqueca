package healsmart.com.conexaoenxaqueca.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by allan on 24/11/15.
 */
public class MetodoAlivioCrise extends RealmObject
{
    @PrimaryKey
    private String id;

    private MetodoAlivio metodoAlivio;
    private boolean funcionou;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public MetodoAlivio getMetodoAlivio() {
        return metodoAlivio;
    }

    public void setMetodoAlivio(MetodoAlivio metodoAlivio) {
        this.metodoAlivio = metodoAlivio;
    }

    public boolean isFuncionou() {
        return funcionou;
    }

    public void setFuncionou(boolean funcionou) {
        this.funcionou = funcionou;
    }

    public static MetodoAlivioCrise copy(MetodoAlivioCrise metodoAlivioCrise)
    {
        MetodoAlivioCrise metodoAlivioCriseCopy = new MetodoAlivioCrise();
        metodoAlivioCriseCopy.setId(metodoAlivioCrise.getId());
        metodoAlivioCriseCopy.setMetodoAlivio(MetodoAlivio.copy(metodoAlivioCrise.getMetodoAlivio()));
        metodoAlivioCriseCopy.setFuncionou(metodoAlivioCrise.isFuncionou());

        return metodoAlivioCriseCopy;
    }
}
