package healsmart.com.conexaoenxaqueca.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by allan on 20/11/15.
 */
public class Consequencia extends RealmObject
{
    @PrimaryKey
    private String id;

    private String nome;
    private String display;
    private long created;
    private Usuario usuario;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.trim().toLowerCase();
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public static Consequencia copy(Consequencia consequencia)
    {
        Consequencia consequenciaCopy = new Consequencia();
        consequenciaCopy.setId(consequencia.getId());
        consequenciaCopy.setUsuario(Usuario.copy(consequencia.getUsuario()));
        consequenciaCopy.setNome(consequencia.getNome());
        consequenciaCopy.setDisplay(consequencia.getDisplay());
        consequenciaCopy.setCreated(consequencia.getCreated());

        return consequenciaCopy;
    }
}
