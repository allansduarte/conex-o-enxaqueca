package healsmart.com.conexaoenxaqueca.domain;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Crise extends RealmObject
{
    @PrimaryKey
    private String id;

    private Usuario usuario;
    private int nrCrise;
    private long dataIni;
    private long dataFim;
    private int intensidade;
    private RealmList<Medicamento> medicamentos;
    private RealmList<Sintoma> sintomas;
    private RealmList<GatilhoCrise> gatilhosCrise;
    private RealmList<QualidadeDor> qualidadeDors;
    private RealmList<MetodoAlivioCrise> metodos;
    private RealmList<Consequencia> consequencias;
    private RealmList<Local> locais;
    private RealmList<LocaisDeDor> locaisDeDores;
    private long created;

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public int getNrCrise() {
        return nrCrise;
    }

    public void setNrCrise(int nrCrise) {
        this.nrCrise = nrCrise;
    }

    public long getDataIni() {
        return dataIni;
    }

    public void setDataIni(long dataIni) {
        this.dataIni = dataIni;
    }

    public long getDataFim() {
        return dataFim;
    }

    public void setDataFim(long dataFim) {
        this.dataFim = dataFim;
    }

    public int getIntensidade() {
        return intensidade;
    }

    public void setIntensidade(int intensidade) {
        this.intensidade = intensidade;
    }

    public RealmList<Medicamento> getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(RealmList<Medicamento> medicamentos) {
        this.medicamentos = medicamentos;
    }

    public RealmList<Sintoma> getSintomas() {
        return sintomas;
    }

    public void setSintomas(RealmList<Sintoma> sintomas) {
        this.sintomas = sintomas;
    }

    public RealmList<GatilhoCrise> getGatilhosCrise() {
        return gatilhosCrise;
    }

    public void setGatilhosCrise(RealmList<GatilhoCrise> gatilhos) {
        this.gatilhosCrise = gatilhos;
    }

    public RealmList<QualidadeDor> getQualidadeDors() {
        return qualidadeDors;
    }

    public void setQualidadeDors(RealmList<QualidadeDor> qualidadeDors) {
        this.qualidadeDors = qualidadeDors;
    }

    public RealmList<MetodoAlivioCrise> getMetodos() {
        return metodos;
    }

    public void setMetodos(RealmList<MetodoAlivioCrise> metodos) {
        this.metodos = metodos;
    }

    public RealmList<Consequencia> getConsequencias() {
        return consequencias;
    }

    public void setConsequencias(RealmList<Consequencia> consequencias) {
        this.consequencias = consequencias;
    }

    public RealmList<Local> getLocais() {
        return locais;
    }

    public void setLocais(RealmList<Local> locais) {
        this.locais = locais;
    }

    public RealmList<LocaisDeDor> getLocaisDeDores() {
        return locaisDeDores;
    }

    public void setLocaisDeDores(RealmList<LocaisDeDor> locaisDeDores) {
        this.locaisDeDores = locaisDeDores;
    }

    public static Crise copy(Crise crise)
    {
        Crise criseCopy = new Crise();

        criseCopy.setId(crise.getId());
        criseCopy.setNrCrise(crise.getNrCrise());
        criseCopy.setDataIni(crise.getDataIni());
        criseCopy.setDataFim(crise.getDataFim());
        criseCopy.setIntensidade(crise.getIntensidade());

        RealmList<Medicamento> medicamentos = new RealmList<>();
        for(Medicamento medicamento : crise.getMedicamentos())
        {
            medicamentos.add(Medicamento.copy(medicamento));
        }
        criseCopy.setMedicamentos(medicamentos);

        RealmList<Sintoma> sintomas = new RealmList<>();
        for(Sintoma sintoma : crise.getSintomas())
        {
            sintomas.add(Sintoma.copy(sintoma));
        }
        criseCopy.setSintomas(sintomas);

        RealmList<GatilhoCrise> gatilhos = new RealmList<>();
        for(GatilhoCrise gatilho : crise.getGatilhosCrise())
        {
            gatilhos.add(GatilhoCrise.copy(gatilho));
        }
        criseCopy.setGatilhosCrise(gatilhos);

        RealmList<QualidadeDor> qualidadeDorList = new RealmList<>();
        for(QualidadeDor qualidadeDor : crise.getQualidadeDors())
        {
            qualidadeDorList.add(QualidadeDor.copy(qualidadeDor));
        }
        criseCopy.setQualidadeDors(qualidadeDorList);

        RealmList<MetodoAlivioCrise> metodos = new RealmList<>();
        for(MetodoAlivioCrise metodoAlivioCrise : crise.getMetodos())
        {
            metodos.add(MetodoAlivioCrise.copy(metodoAlivioCrise));
        }
        criseCopy.setMetodos(metodos);

        RealmList<Consequencia> consequencias = new RealmList<>();
        for(Consequencia consequencia : crise.getConsequencias())
        {
            consequencias.add(Consequencia.copy(consequencia));
        }
        criseCopy.setConsequencias(consequencias);

        RealmList<Local> locais = new RealmList<>();
        for(Local local : crise.getLocais())
        {
            locais.add(Local.copy(local));
        }
        criseCopy.setLocais(locais);

        RealmList<LocaisDeDor> locaisDeDores = new RealmList<>();
        for(LocaisDeDor locaisDeDor : crise.getLocaisDeDores())
        {
            locaisDeDores.add(LocaisDeDor.copy(locaisDeDor));
        }
        criseCopy.setLocaisDeDores(locaisDeDores);

        criseCopy.setCreated(crise.getCreated());

        return criseCopy;
    }
}
