package healsmart.com.conexaoenxaqueca.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by allan on 23/09/15.
 */
public class Usuario extends RealmObject
{
    public static final String ID = "conexaoenxaqueca.com.healsmart.conexoenxaqueca.domain.RealmObject.ID";

    @PrimaryKey
    private String id;

    private String email;
    private String senha;
    private String nome;
    private String sobrenome;
    private long dataNascimento;
    private String genero;

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public long getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(long dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        if(sobrenome == null)
            this.sobrenome = null;
        else
            this.sobrenome = sobrenome.trim();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if(nome == null)
            this.nome = null;
        else
            this.nome = nome.trim();
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        if(senha == null)
            this.senha = null;
        else
            this.senha = senha.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static Usuario copy(Usuario usuario)
    {
        Usuario usuarioCopy = new Usuario();
        usuarioCopy.setId(usuario.getId());
        usuarioCopy.setNome(usuario.getNome());
        usuarioCopy.setSobrenome(usuario.getSobrenome());
        usuarioCopy.setDataNascimento(usuario.getDataNascimento());
        usuarioCopy.setGenero(usuario.getGenero());
        usuarioCopy.setEmail(usuario.getEmail());
        usuarioCopy.setSenha(usuario.getSenha());

        return usuarioCopy;
    }
}
