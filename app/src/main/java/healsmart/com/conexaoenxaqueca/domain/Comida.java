package healsmart.com.conexaoenxaqueca.domain;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by allan on 25/11/15.
 */
public class Comida extends RealmObject
{
    @PrimaryKey
    private String id;

    private String nome;
    private String display;
    private Usuario usuario;
    private long created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.trim().toLowerCase();
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public static Comida copy(Comida comida)
    {
        Comida comidaCopy = new Comida();
        comidaCopy.setId(comida.getId());
        comidaCopy.setNome(comida.getNome());
        comidaCopy.setDisplay(comida.getDisplay());
        comidaCopy.setUsuario(Usuario.copy(comida.getUsuario()));
        comidaCopy.setCreated(comida.getCreated());

        return comidaCopy;
    }
}
