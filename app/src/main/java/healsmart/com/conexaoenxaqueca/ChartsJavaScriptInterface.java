package healsmart.com.conexaoenxaqueca;


import android.content.Context;
import android.content.res.Resources;
import android.webkit.JavascriptInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChartsJavaScriptInterface
{
    private String[] mIntensidadeData;
    private Context mContext;

    public ChartsJavaScriptInterface(Context context) {
        mContext = context;
    }

    @JavascriptInterface
    public String getData(){

        String retorno = "";

        String[] dataGraph = getIntensidadeData();

        if(dataGraph != null)
        {
            int nrCriseFraca = Integer.parseInt(dataGraph[0]);
            int nrCriseMedia = Integer.parseInt(dataGraph[1]);
            int nrCriseForte = Integer.parseInt(dataGraph[2]);

            if(nrCriseFraca > 0 || nrCriseMedia > 0 || nrCriseForte > 0)
            {
                Resources resources = mContext.getResources();

                JSONObject jsonObjectCriseForte = buildJSONObject(nrCriseForte, String.format("#%06X", (0xFFFFFF & resources.getColor(R.color.criseForte))), String.format("#%06X", (0xFFFFFF & resources.getColor(R.color.criseForteHighlight))), "Forte");
                JSONObject jsonObjectCriseMedia = buildJSONObject(nrCriseMedia, String.format("#%06X", (0xFFFFFF & resources.getColor(R.color.criseMedia))), String.format("#%06X", (0xFFFFFF & resources.getColor(R.color.criseMediaHighlight))), "Média");
                JSONObject jsonObjectCriseFraca = buildJSONObject(nrCriseFraca, String.format("#%06X", (0xFFFFFF & resources.getColor(R.color.criseFraca))), String.format("#%06X", (0xFFFFFF & resources.getColor(R.color.criseFracaHighlight))), "Fraca");

                JSONArray jsonArray = new JSONArray();
                jsonArray.put(jsonObjectCriseFraca);
                jsonArray.put(jsonObjectCriseMedia);
                jsonArray.put(jsonObjectCriseForte);

                retorno = jsonArray.toString();
            }
        }

        return retorno;
    }

    public String[] getIntensidadeData() {
        return mIntensidadeData;
    }

    public void setIntensidadeData(String[] mIntensidadeData) {
        this.mIntensidadeData = mIntensidadeData;
    }

    private JSONObject buildJSONObject(int value, String color, String highlight, String label)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("value", value);
            jsonObject.put("color", color);
            jsonObject.put("highlight", highlight);
            jsonObject.put("label", label);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
