package healsmart.com.conexaoenxaqueca;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import healsmart.com.conexaoenxaqueca.activities.CadastrarActivity;
import healsmart.com.conexaoenxaqueca.activities.LoginActivity;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;

public class SplashScreen extends FragmentActivity {

    private static final int NUM_PAGES = 3;
    private static final String TAG = "SplashScreen.java";

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    private Button loginButton;
    private Button cadastreSeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ServiceFactory.iniciar();
        ServiceFactory.getInstance().getServiceUtils().hideStatusBar(this);

        setContentView(R.layout.activity_splash_screen);

        inicializarView();

        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.addOnPageChangeListener(mPagerOnPageChangeListener);
        mPager.setPageTransformer(true, new FadePageTransformer());

        loginButton.setOnClickListener(loginButtonOnClick);
        cadastreSeButton.setOnClickListener(cadastreSeButtonOnClick);
    }

    View.OnClickListener loginButtonOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent loginIntent = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(loginIntent);
        }
    };

    View.OnClickListener cadastreSeButtonOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent cadastrarIntent = new Intent(SplashScreen.this, CadastrarActivity.class);
            startActivity(cadastrarIntent);
        }
    };

    private static class FadePageTransformer implements ViewPager.PageTransformer {
        public void transformPage(View view, float position) {

            view.setAlpha(1 - Math.abs(position));

            if (position < 0) {
                view.setScrollX((int)((float)(view.getWidth()) * position));
            } else if (position > 0) {
                view.setScrollX(-(int) ((float) (view.getWidth()) * -position));
            } else {
                view.setScrollX(0);
            }
        }
    }

    private void inicializarView()
    {
        mPager = (ViewPager) findViewById(R.id.pager);
        loginButton = (Button) findViewById(R.id.login);
        cadastreSeButton = (Button) findViewById(R.id.cadastre_se);
    }

    ViewPager.OnPageChangeListener mPagerOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {


        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ScreenSlidePageFragment.create(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}