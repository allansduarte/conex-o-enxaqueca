package healsmart.com.conexaoenxaqueca.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import healsmart.com.conexaoenxaqueca.NotificationService;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.adapters.SumarioCriseAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Bebida;
import healsmart.com.conexaoenxaqueca.domain.Comida;
import healsmart.com.conexaoenxaqueca.domain.Consequencia;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.EsforcoFisico;
import healsmart.com.conexaoenxaqueca.domain.Gatilho;
import healsmart.com.conexaoenxaqueca.domain.GatilhoCrise;
import healsmart.com.conexaoenxaqueca.domain.LocaisDeDor;
import healsmart.com.conexaoenxaqueca.domain.Local;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivio;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivioCrise;
import healsmart.com.conexaoenxaqueca.domain.QualidadeDor;
import healsmart.com.conexaoenxaqueca.domain.Sintoma;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import io.realm.Realm;

public class SumarioCriseActivity extends AppCompatActivity implements SurveyConstants
{
    private Toolbar mToolbar;
    private Drawer navigationDrawerLeft;
    private String mCriseId;
    private String mClazzListaCriseMedicamento;
    private String mClazzMain;
    private SumarioCriseAdapter mAdapter;
    private ListView mListaQuestoesListView;

    private TextView mDataInicialTextView;
    private TextView mTempoInicialTextView;
    private TextView mDataFinalTextView;
    private TextView mTempoFinalTextView;
    private TextView mDuracaoCriseTextView;

    private Realm realm;
    private Map<Double, Object> mListQuestoesSumario;
    private boolean isContentLoaded=false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            mCriseId = extras.getString(INTENT_CRISE_ID);
            mClazzListaCriseMedicamento = extras.getString("conexaoenxaqueca.com.healsmart.conexoenxaqueca.activities.ListaCriseMedicamentoActivity");
            mClazzMain = extras.getString("MainActivity");
        }

        setContentView(R.layout.activity_sumario_crise);

        ServiceFactory.iniciar();
        initViews();
        navigationDrawerLeft = ServiceFactory.getInstance().getServiceNavigationDrawer(this, mToolbar, R.id.drawer_container).run();

        realm = Realm.getDefaultInstance();
        mListQuestoesSumario = getSumarioContent();
        if(mListQuestoesSumario != null)
            notifyView();
        else redirectToActivity();

        isContentLoaded=true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_sumario_crise, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        if(navigationDrawerLeft.isDrawerOpen()){
            navigationDrawerLeft.closeDrawer();
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public void onStop() {
        isContentLoaded=false;

        super.onStop();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(!isContentLoaded) {
            mListQuestoesSumario = getSumarioContent();
            notifyView();
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();

        super.onDestroy();
    }

    private void initViews()
    {
        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mListaQuestoesListView = (ListView) findViewById(R.id.listaQuestoesListView);

        mDataInicialTextView = (TextView) findViewById(R.id.dataInicialTextView);
        mTempoInicialTextView = (TextView) findViewById(R.id.tempoInicialTextView);
        mDataFinalTextView = (TextView) findViewById(R.id.dataFinalTextView);
        mTempoFinalTextView = (TextView) findViewById(R.id.tempoFinalTextView);
        mDuracaoCriseTextView = (TextView) findViewById(R.id.duracaoCriseTextView);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mAdapter = new SumarioCriseAdapter(this, new TreeMap<Double, Object>());
        mListaQuestoesListView.setAdapter(mAdapter);

        mToolbar.setTitle(getResources().getString(R.string.title_activity_sumario_crise));
        setSupportActionBar(mToolbar);
        mListaQuestoesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Map.Entry entry = mAdapter.getEntry(position);
                double key = (double) (entry != null ? entry.getKey() : 0);

                double questaoNavigation = key;

                if (key == 0) questaoNavigation = QUESTION_INTENSIDADE_ID;

                Intent intentAlterarQuestao = new Intent(SumarioCriseActivity.this, SurveyActivity.class);
                intentAlterarQuestao.putExtra(INTENT_CRISE_ID, mCriseId);
                intentAlterarQuestao.putExtra(INTENT_QUESTAO_NAVIGATION, questaoNavigation);
                startActivity(intentAlterarQuestao);
            }
        });
    }

    private void notifyView()
    {
        if(mListQuestoesSumario == null)
            ServiceFactory.getInstance().getServiceUtils().redirectTo(this, MainActivity.class);
        else {
            mAdapter.setListSumario(mListQuestoesSumario);
            feedTimeDurationData(mListQuestoesSumario);
        }
    }

    public void onClickDuracaoCrise(View v)
    {
        double questaoNavigation = 1;

        Intent intentAlterarQuestao = new Intent(SumarioCriseActivity.this, SurveyActivity.class);
        intentAlterarQuestao.putExtra(INTENT_CRISE_ID, mCriseId);
        intentAlterarQuestao.putExtra(INTENT_QUESTAO_NAVIGATION, questaoNavigation);
        startActivity(intentAlterarQuestao);
    }

    public void excluirCrise(View v)
    {
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_msg_padrao, null);

        final Dialog dialog = new Dialog(SumarioCriseActivity.this);

        Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
        Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);
        TextView messageTextView = (TextView) dialogView.findViewById(R.id.messageTextView);
        TextView titleTextView = (TextView) dialogView.findViewById(R.id.titleTextView);

        messageTextView.setText("TEM CERTEZA QUE DESEJA EXCLUIR ESTA CRISE?");
        titleTextView.setText("CONFIRMAÇÃO");
        positiveButton.setText("SIM, EXCLUIR!");
        positiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();

                Crise crise = realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mCriseId)
                        .findFirst();
                int nrCrise = crise.getNrCrise();

                NotificationService notificationService = ServiceFactory.getInstance().getNotificationService(SumarioCriseActivity.this);
                notificationService.setNotificationId(nrCrise);
                notificationService.cancelNotificationCrisisInProgress();

                deleteCrise();
                redirectToActivity();
            }
        });

        negativeButton.setText(getResources().getString(R.string.dialogPatternAddOptionNegativeButton));
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void deleteCrise() {
        Crise crise = realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mCriseId)
                .findFirst();

        try {

            realm.beginTransaction();
            crise.removeFromRealm();
            realm.commitTransaction();
        } catch (Exception e) {

            realm.cancelTransaction();
        }
    }

    private void redirectToActivity()
    {
        if(mClazzListaCriseMedicamento != null && mClazzListaCriseMedicamento.equals("conexaoenxaqueca.com.healsmart.conexoenxaqueca.activities.ListaCriseMedicamentoActivity"))
            ServiceFactory.getInstance().getServiceUtils().redirectTo(SumarioCriseActivity.this, ListaCriseMedicamentoActivity.class);
        else if(mClazzMain != null && mClazzMain.equals("MainActivity"))
            ServiceFactory.getInstance().getServiceUtils().redirectTo(SumarioCriseActivity.this, MainActivity.class);
        else
            ServiceFactory.getInstance().getServiceUtils().redirectTo(SumarioCriseActivity.this, ListaCriseActivity.class);
    }

    public void onClickConfirmar(View v)
    {
        redirectToActivity();
        finish();
    }

    private void feedTimeDurationData(Map<Double, Object> data)
    {
        Crise crise = (data == null ? null : (Crise) data.get((double) 0));

        if(crise != null)
        {
            String[] duracaoCrise = ServiceFactory.getInstance().getServiceUtils().getTimeDateDuration(crise.getDataIni(), crise.getDataFim());
            String dataIni = duracaoCrise[0];
            String horaIni = duracaoCrise[1];
            String dataFim = duracaoCrise[2];
            String horaFim = duracaoCrise[3];

            long horas = Long.parseLong(duracaoCrise[5]);
            long minutos = Long.parseLong(duracaoCrise[4]);
            String textoDuracaoCrise = "";

            if(horas > 0)
            {
                textoDuracaoCrise = String.valueOf(horas);
                if(horas == 1)
                    textoDuracaoCrise += " hora";
                else
                    textoDuracaoCrise += " horas";
            }

            if(minutos > 0)
            {
                textoDuracaoCrise += " "+minutos;
                if(minutos == 1)
                    textoDuracaoCrise += " minuto";
                else
                    textoDuracaoCrise += " minutos";
            }

            if(horas == 0 && minutos == 0)
                textoDuracaoCrise = "Sem duração de tempo";

            mDataInicialTextView.setText(dataIni);
            mTempoInicialTextView.setText(horaIni);
            mDuracaoCriseTextView.setText(textoDuracaoCrise);

            if(horaFim != null)
            {
                mDataFinalTextView.setText(dataFim);
                mTempoFinalTextView.setText(horaFim);
                mTempoFinalTextView.setVisibility(View.VISIBLE);
            } else {
                mDataFinalTextView.setText("Clique aqui e informe o tempo final da crise.");
                mTempoFinalTextView.setVisibility(View.GONE);
            }
        } else {
            ServiceFactory.getInstance().getServiceUtils().redirectTo(this, MainActivity.class);
        }
    }

    private Map<Double, Object> getSumarioContent()
    {
        if(mCriseId == null)
            return null;

        realm.refresh();
        Crise crise = realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mCriseId)
                .findFirst();

        if(crise == null)
            return null;

        Map<Double, Object> listObjectResult = new TreeMap<>();
        listObjectResult.put((double) 0, Crise.copy(crise));

        List<Medicamento> medicamentos = new ArrayList<>();
        List<Medicamento> medicamentosFuncionou = new ArrayList<>();
        for(Medicamento medicamento : crise.getMedicamentos())
        {
            medicamentos.add(Medicamento.copy(medicamento));

            if(medicamento.isFuncionou())
                medicamentosFuncionou.add(Medicamento.copy(medicamento));
        }
        listObjectResult.put((double) QUESTION_MEDICAMENTO_ID, medicamentos);

        List<LocaisDeDor> locaisDeDores = new ArrayList<>();
        for(LocaisDeDor localDeDor : crise.getLocaisDeDores())
        {
            locaisDeDores.add(LocaisDeDor.copy(localDeDor));
        }
        listObjectResult.put((double) QUESTION_LOCAIS_DOR_ID, locaisDeDores);

        List<QualidadeDor> qualidadeDorList = new ArrayList<>();
        for(QualidadeDor qualidadeDor : crise.getQualidadeDors())
        {
            qualidadeDorList.add(QualidadeDor.copy(qualidadeDor));
        }
        listObjectResult.put((double) QUESTION_QUALIDADE_DOR_ID, qualidadeDorList);

        List<Sintoma> sintomasList = new ArrayList<>();
        for(Sintoma sintoma : crise.getSintomas())
        {
            sintomasList.add(Sintoma.copy(sintoma));
        }
        listObjectResult.put((double) QUESTION_SINTOMAS_ID, sintomasList);

        List<Gatilho> gatilhosList = new ArrayList<>();
        for(GatilhoCrise gatilhoCrise : crise.getGatilhosCrise())
        {
            gatilhosList.add(Gatilho.copy(gatilhoCrise.getGatilho()));
        }
        listObjectResult.put((double) QUESTION_GATILHOS_ID, gatilhosList);

        List<Comida> gatilhoComidaList = new ArrayList<>();
        for(GatilhoCrise gatilhoCrise : crise.getGatilhosCrise())
        {
            if(gatilhoCrise.getGatilho().getNome().equals("comida"))
            {
                int sizeComida = (gatilhoCrise.getComidas() == null ? 0 : gatilhoCrise.getComidas().size());
                for(int i=0; i < sizeComida; i++)
                {
                    gatilhoComidaList.add(Comida.copy(gatilhoCrise.getComidas().get(i)));
                }
            }
        }
        if(gatilhoComidaList.size() > 0)
            listObjectResult.put(7.1, gatilhoComidaList);//GATILHOS - COMIDA

        List<Bebida> gatilhoBebidaList = new ArrayList<>();
        for(GatilhoCrise gatilhoCrise : crise.getGatilhosCrise())
        {
            if(gatilhoCrise.getGatilho().getNome().equals("bebida"))
            {
                int sizeBebida = (gatilhoCrise.getBebidas() == null ? 0 : gatilhoCrise.getBebidas().size());
                for(int i=0; i < sizeBebida; i++)
                {
                    gatilhoBebidaList.add(Bebida.copy(gatilhoCrise.getBebidas().get(i)));
                }
            }
        }
        if(gatilhoBebidaList.size() > 0)
            listObjectResult.put(7.2, gatilhoBebidaList);//GATILHOS - BEBIDA

        List<EsforcoFisico> gatilhoEsforcoFisicoList = new ArrayList<>();
        for(GatilhoCrise gatilhoCrise : crise.getGatilhosCrise())
        {
            if(gatilhoCrise.getGatilho().getNome().equals("esforço físico"))
            {
                int sizeEsforcoFisico = (gatilhoCrise.getEsforcoFisicos() == null ? 0 : gatilhoCrise.getEsforcoFisicos().size());
                for(int i=0; i < sizeEsforcoFisico; i++)
                {
                    gatilhoEsforcoFisicoList.add(EsforcoFisico.copy(gatilhoCrise.getEsforcoFisicos().get(i)));
                }
            }
        }
        if(gatilhoEsforcoFisicoList.size() > 0)
            listObjectResult.put(7.3, gatilhoEsforcoFisicoList);//GATILHOS - ESFORÇO FÍSICO

        List<MetodoAlivio> metodoList = new ArrayList<>();
        List<MetodoAlivio> metodoFuncionouList = new ArrayList<>();
        for(MetodoAlivioCrise metodo : crise.getMetodos())
        {
            metodoList.add(MetodoAlivio.copy(metodo.getMetodoAlivio()));

            if(metodo.isFuncionou())
                metodoFuncionouList.add(MetodoAlivio.copy(metodo.getMetodoAlivio()));
        }
        listObjectResult.put((double) QUESTION_METODO_ALIVIO_ID, metodoList);

        List<Object> metodosQueFuncionaram = new ArrayList<>();
        metodosQueFuncionaram.add(metodoFuncionouList);
        metodosQueFuncionaram.add(medicamentosFuncionou);
        if(metodosQueFuncionaram.size() > 0)
            listObjectResult.put((double) QUESTION_METODO_ALIVIO_FUNCIONOU_ID, metodosQueFuncionaram);

        List<Consequencia> consequenciaList = new ArrayList<>();
        for(Consequencia consequencia : crise.getConsequencias())
        {
            consequenciaList.add(Consequencia.copy(consequencia));
        }
        listObjectResult.put((double) QUESTION_CONSEQUENCIAS_ID, consequenciaList);

        List<Local> locais = new ArrayList<>();
        for(Local local : crise.getLocais())
        {
            locais.add(Local.copy(local));
        }
        listObjectResult.put((double) QUESTION_LOCAL_ID, locais);

        return listObjectResult;
    }
}
