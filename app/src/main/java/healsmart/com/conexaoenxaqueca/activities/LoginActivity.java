package healsmart.com.conexaoenxaqueca.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;

public class LoginActivity extends AppCompatActivity implements PerfilUsuario
{

    private Toolbar mToolbar;
    private EditText mEmailEditText;
    private EditText mSenhaEditText;

    private CallbackManager callbackManager;

    private SharedPreferences preferences;

    private Resources resources;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ServiceFactory.iniciar();
        ServiceFactory.getInstance().getServiceUtils().hideStatusBar(this);

        setContentView(R.layout.activity_login);

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        ServiceFactory.iniciar();
        initViews();
        resources = getResources();

        mToolbar.setTitle(resources.getString(R.string.title_activity_login));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        preferences = getSharedPreferences(USUARIO_PREFS, MODE_PRIVATE);
        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initViews() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if(mEmailEditText.getText().hashCode() == s.hashCode())
                {
                    ServiceFactory.getInstance().getServiceValidate().verifyAndClearErrors(mEmailEditText);
                }
                if(mSenhaEditText.getText().hashCode() == s.hashCode())
                {
                    ServiceFactory.getInstance().getServiceValidate().verifyAndClearErrors(mSenhaEditText);
                }
            }
        };

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mEmailEditText = (EditText) findViewById(R.id.emailEditText);
        mEmailEditText.addTextChangedListener(textWatcher);

        mSenhaEditText = (EditText) findViewById(R.id.senhaEditText);
        mSenhaEditText.addTextChangedListener(textWatcher);
    }

    public void loginFacebook(View v)
    {
        if (v.getId() == R.id.facebookButton) {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_friends", "user_birthday"));

            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            ServiceFactory.getInstance().getServiceLoginManager().salvarUsuario(loginResult, LoginActivity.this);
                        }

                        @Override
                        public void onCancel() {
                            LoginManager.getInstance().logOut();
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.dialog_msg_padrao, null);

                            final Dialog dialog = new Dialog(LoginActivity.this);

                            Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
                            Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);
                            TextView messageTextView = (TextView) dialogView.findViewById(R.id.messageTextView);
                            TextView titleTextView = (TextView) dialogView.findViewById(R.id.titleTextView);

                            messageTextView.setText("NÃO FOI POSSÍVEL CONECTAR-SE COM O FACEBOOK, VERIFIQUE SUA CONEXÃO COM A INTERNET. PARA CONTIUAR FAÇA O LOGIN OFF-LINE!");
                            titleTextView.setText("OPS!");
                            positiveButton.setText("LOGIN OFF-LINE");
                            positiveButton.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    dialog.dismiss();
                                }
                            });

                            negativeButton.setText(resources.getString(R.string.dialogPatternAddOptionNegativeButton));
                            negativeButton.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    dialog.dismiss();
                                }
                            });

                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(dialogView);
                            dialog.setCancelable(false);
                            dialog.show();
                        }
                    }
            );
        }
    }

    public void loginUsuario(View v) {
        if (v.getId() == R.id.entrarButton)
        {
            if (validateFields())
            {
                String email = mEmailEditText.getText().toString();
                String senha = mSenhaEditText.getText().toString();

                Usuario usuario = realm.where(Usuario.class)
                                        .equalTo(UsuarioEntidade.EMAIL, email)
                                        .equalTo(UsuarioEntidade.SENHA, senha)
                                        .findFirst();

                if(usuario == null){
                    Toast.makeText(LoginActivity.this, resources.getString(R.string.validacao_email_senha_incorretos), Toast.LENGTH_LONG).show();
                } else{
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean(USUARIO_PREFS_LOGIN, true);
                    editor.putString(USUARIO_PREFS_USUARIO_ID, usuario.getId());
                    editor.apply();

                    Intent mainActivityIntent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(mainActivityIntent);
                }
            }
        }
    }

    /**
     * Método de validação. Responsável por verificar e validar o formulário de cadastro.
     *
     * @return boolean que indica se os campos foram validados com sucesso ou não
     */
    private boolean validateFields() {

        return ( !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(mEmailEditText, resources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().hasSizeValid(mEmailEditText, 5, resources.getString(R.string.validacao_email_senha_incorretos))
                && ServiceFactory.getInstance().getServiceValidate().isValidEmail(mEmailEditText)
                && !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(mSenhaEditText, resources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().hasSizeValid(mSenhaEditText, 4, resources.getString(R.string.validacao_email_senha_incorretos))
            );
    }
}
