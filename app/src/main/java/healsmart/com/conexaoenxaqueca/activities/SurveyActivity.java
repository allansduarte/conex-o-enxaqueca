package healsmart.com.conexaoenxaqueca.activities;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import healsmart.com.conexaoenxaqueca.NotificationService;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SurveyManager;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Bebida;
import healsmart.com.conexaoenxaqueca.domain.Comida;
import healsmart.com.conexaoenxaqueca.domain.Consequencia;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.EsforcoFisico;
import healsmart.com.conexaoenxaqueca.domain.Gatilho;
import healsmart.com.conexaoenxaqueca.domain.GatilhoCrise;
import healsmart.com.conexaoenxaqueca.domain.Local;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivio;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivioCrise;
import healsmart.com.conexaoenxaqueca.domain.QualidadeDor;
import healsmart.com.conexaoenxaqueca.domain.Sintoma;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.fragments.ConsequenciasFragment;
import healsmart.com.conexaoenxaqueca.fragments.DuracaoDorFragment;
import healsmart.com.conexaoenxaqueca.fragments.GatilhoBebidasFragment;
import healsmart.com.conexaoenxaqueca.fragments.GatilhoComidasFragment;
import healsmart.com.conexaoenxaqueca.fragments.GatilhoEsforcoFisicoFragment;
import healsmart.com.conexaoenxaqueca.fragments.GatilhosDorFragment;
import healsmart.com.conexaoenxaqueca.fragments.LocalCriseDorFragment;
import healsmart.com.conexaoenxaqueca.fragments.LocalizacaoDorFragment;
import healsmart.com.conexaoenxaqueca.fragments.MedicamentoInfoFragment;
import healsmart.com.conexaoenxaqueca.fragments.MetodosAlivioFragment;
import healsmart.com.conexaoenxaqueca.fragments.MetodosAlivioFuncionouFragment;
import healsmart.com.conexaoenxaqueca.fragments.QualidadeDorFragment;
import healsmart.com.conexaoenxaqueca.fragments.SeveridadeDorFragment;
import healsmart.com.conexaoenxaqueca.fragments.SintomasDorFragment;
import healsmart.com.conexaoenxaqueca.interfaces.BebidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.ComidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.ConsequenciaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.EsforcoFisicoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.GatilhoCriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.GatilhoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.LocalEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.MetodoAlivioEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.QualidadeDorEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SintomaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class SurveyActivity extends AppCompatActivity
        implements DuracaoDorFragment.OnQuestionOneListener,
        SeveridadeDorFragment.OnFragmentSeveridadeDorListener,
        QualidadeDorFragment.OnFragmentInteractionListener,
        MetodosAlivioFragment.OnFragmentInteractionListener,
        MetodosAlivioFuncionouFragment.OnFragmentInteractionListener,
        SintomasDorFragment.OnFragmentInteractionListener,
        GatilhosDorFragment.OnFragmentInteractionListener,
        GatilhoComidasFragment.OnFragmentInteractionListener,
        GatilhoBebidasFragment.OnFragmentInteractionListener,
        GatilhoEsforcoFisicoFragment.OnFragmentInteractionListener,
        ConsequenciasFragment.OnFragmentInteractionListener,
        LocalCriseDorFragment.OnFragmentInteractionListener,
        PerfilUsuario,
        SurveyConstants

{
    private TextView mSurveyIndicatorTextView;

    private static final int QTD_PERGUNTAS = 11;

    private Double mCurrentQuestion;
    private Double mOldQuestion;

    private int mValorIntensidadeDor;
    private Calendar mQuestionOneDataInicio;
    private Calendar mQuestionOneDataFim;
    private List<String> mQualidadeDorOptionsSelected;
    private List<String> mSintomasOptionsSelected;
    private List<String> mGatilhosOptionsSelected;
    private List<String> mGatilhosEsforcoFisicoOptionsSelected;
    private List<String> mGatilhosComidaOptionsSelected;
    private List<String> mGatilhosBebidaOptionsSelected;
    private List<String> mMetodoAlivioOptionsSelected;
    private List<String> mConsequenciaOptionsSelected;
    private List<String> mLocalOptionsSelected;
    private List<String> mMetodoAlivioFuncionouOptionsSelected;
    private List<String> mMedicamentoFuncionouOptionsSelected;

    private Resources resources;
    private SharedPreferences mPreferences;
    private boolean mCriseAindaNaoTerminou=false;
    private SurveyManager mSurveyManager;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ServiceFactory.iniciar();
        ServiceFactory.getInstance().getServiceUtils().hideStatusBar(this);
        mSurveyManager = ServiceFactory.getInstance().getServiceSurveyManager(this);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            mSurveyManager.setCriseId(extras.getString(INTENT_CRISE_ID));
            mSurveyManager.setUpdateMode(true);
            mCurrentQuestion = mOldQuestion = extras.getDouble(INTENT_QUESTAO_NAVIGATION);
        } else {
            mCurrentQuestion = mOldQuestion = 1.;
            mSurveyManager.setMode(MODE_INSERT);
            mSurveyManager.setUpdateMode(false);
        }

        setContentView(R.layout.activity_survey);
        initViews();

        mSurveyIndicatorTextView.setVisibility(View.GONE);
        realm = Realm.getDefaultInstance();

        attachFragment(getFragment(), 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_survey, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        FragmentManager fm = getSupportFragmentManager();

        if(mSurveyManager.isUpdateMode())
        {
            finish();
            super.onBackPressed();
        }
        else
        {
            int qtdFragments = fm.getBackStackEntryCount()-1;
            if(qtdFragments > 0)
            {
                if(qtdFragments >= 8 && mOldQuestion > 7 && mOldQuestion < 8
                        || qtdFragments >= 8 && mCurrentQuestion == 7)
                {
                    mOldQuestion=mCurrentQuestion;
                    mCurrentQuestion = (double) mCurrentQuestion.intValue();//7.1 > 7 > 7.0

                    for(int i=5; i < qtdFragments; i++)
                    {
                        fm.popBackStack();
                    }
                    mCurrentQuestion=(double) QUESTION_SINTOMAS_ID;
                }
                else{
                    if(qtdFragments >= 7 && mCurrentQuestion > 7 && mCurrentQuestion < 8) {
                        mOldQuestion=mCurrentQuestion;
                        mCurrentQuestion = (double) mCurrentQuestion.intValue();//7.1 > 7 > 7.0
                    } else {
                        mOldQuestion=mCurrentQuestion;
                        if(mCurrentQuestion == QUESTION_CONSEQUENCIAS_ID && !hasReliefMethods())
                            mCurrentQuestion = (double) QUESTION_METODO_ALIVIO_ID;
                        else
                            mCurrentQuestion--;
                    }

                    fm.popBackStack();
                }
            } else dialogContinuarMaisTarde();

            if(mOldQuestion != 1)
                salvarQuestao(mOldQuestion);
        }
    }

    private void initViews()
    {
        mSurveyIndicatorTextView = (TextView) findViewById(R.id.surveyIndicatorTextView);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        resources = getResources();
        mPreferences = getSharedPreferences(USUARIO_PREFS, MODE_PRIVATE);
        boolean isAddQualidadeDorOptions = mPreferences.getBoolean(QUALIDADE_DOR_OPTIONS_ADDED_TAG, false);
        boolean isAddSintomasOptions = mPreferences.getBoolean(SINTOMAS_OPTIONS_ADDED_TAG, false);
        boolean isAddMetodoAlivioOptions = mPreferences.getBoolean(METODO_ALIVIO_OPTIONS_ADDED_TAG, false);
        boolean isAddConsequenciaOptions = mPreferences.getBoolean(CONSEQUENCIA_OPTIONS_ADDED_TAG, false);
        boolean isAddLocalOptions = mPreferences.getBoolean(LOCAL_OPTIONS_ADDED_TAG, false);
        boolean isAddGatilhoOptions = mPreferences.getBoolean(GATILHO_OPTIONS_ADDED_TAG, false);
        boolean isAddGatilhoBebidaOptions = mPreferences.getBoolean(GATILHO_BEBIDA_OPTIONS_ADDED_TAG, false);
        boolean isAddGatilhoComidaOptions = mPreferences.getBoolean(GATILHO_COMIDA_OPTIONS_ADDED_TAG, false);
        boolean isAddGatilhoEsforcoFisicoOptions = mPreferences.getBoolean(GATILHO_ESFORCO_FISICO_OPTIONS_ADDED_TAG, false);
        boolean isAddLocaisDeDorOptions = mPreferences.getBoolean(LOCAIS_DOR_OPTIONS_ADDED_TAG, false);

        if(!isAddQualidadeDorOptions)
            mSurveyManager.insertInitDataQualidadeDor();
        if(!isAddSintomasOptions)
            mSurveyManager.insertInitDataSintomas();
        if(!isAddMetodoAlivioOptions)
            mSurveyManager.insertInitDataMetodoAlivio();
        if(!isAddConsequenciaOptions)
            mSurveyManager.insertInitDataConsequencia();
        if(!isAddLocalOptions)
            mSurveyManager.insertInitDataLocal();
        if(!isAddGatilhoBebidaOptions)
            mSurveyManager.insertInitDataBebidas();
        if(!isAddGatilhoComidaOptions)
            mSurveyManager.insertInitDataComida();
        if(!isAddGatilhoEsforcoFisicoOptions)
            mSurveyManager.insertInitDataEsforcoFisico();
        if(!isAddGatilhoOptions)
            mSurveyManager.insertInitDataGatilho();
        if(!isAddLocaisDeDorOptions)
            mSurveyManager.insertInitDataLocaisDeDor();
    }

    private void attachFragment(Fragment fragment, int type)
    {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if(type == 0)
        {
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
            transaction.add(R.id.questionsfragments, fragment, String.valueOf(mCurrentQuestion));
        } else if(type == 1) {
            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right);
            transaction.replace(R.id.questionsfragments, fragment, String.valueOf(mCurrentQuestion));
        }

        transaction.addToBackStack(String.valueOf(mCurrentQuestion));

        transaction.commit();
    }

    private Fragment getFragment()
    {
        Fragment fragment = null;

        if(mCurrentQuestion >= 1 && mCurrentQuestion < 2)
        {
            fragment = DuracaoDorFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 2 && mCurrentQuestion < 3)
        {
            fragment = SeveridadeDorFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 3 && mCurrentQuestion < 4)
        {
            fragment = MedicamentoInfoFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 4 && mCurrentQuestion < 5)
        {
            fragment = LocalizacaoDorFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 5 && mCurrentQuestion < 6)
        {
            fragment = QualidadeDorFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 6 && mCurrentQuestion < 7)
        {
            fragment = SintomasDorFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 7 && mCurrentQuestion < 8)
        {
            if(mCurrentQuestion == 7.1)
            {
                fragment = GatilhoComidasFragment.newInstance(mSurveyManager);
            }
            else if(mCurrentQuestion == 7.2)
            {
                fragment = GatilhoBebidasFragment.newInstance(mSurveyManager);
            }
            else if(mCurrentQuestion == 7.3)
            {
                fragment = GatilhoEsforcoFisicoFragment.newInstance(mSurveyManager);
            }
            else{
                fragment = GatilhosDorFragment.newInstance(mSurveyManager);
            }
        }
        else if(mCurrentQuestion >= 8 && mCurrentQuestion < 9)
        {
            fragment = MetodosAlivioFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 9 && mCurrentQuestion < 10)
        {
            fragment = MetodosAlivioFuncionouFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 10 && mCurrentQuestion < 11)
        {
            fragment = ConsequenciasFragment.newInstance(mSurveyManager);
        }
        else if(mCurrentQuestion >= 11 && mCurrentQuestion < 12)
        {
            fragment = LocalCriseDorFragment.newInstance(mSurveyManager);
        }

        return fragment;
    }

    public void suspendSurvey(View view)
    {
        dialogContinuarMaisTarde();
    }

    public void confirmQuestion(View view)
    {
        if(mSurveyManager.isUpdateMode())
        {
            salvarQuestao(mCurrentQuestion);
        } else {
            if(mCurrentQuestion == QTD_PERGUNTAS)
            {
                salvarQuestao(mCurrentQuestion);
                ServiceFactory.getInstance().getServiceUtils().redirectTo(this, MainActivity.class);
                finish();
            } else
            {
                mOldQuestion = mCurrentQuestion;
                mCurrentQuestion = (double) mCurrentQuestion.intValue();//7.1 > 7 > 7.0

                salvarQuestao(mOldQuestion);
                attachFragment(getFragment(), 1);
            }
        }
    }

    public boolean checkTempoDuracaoDor()
    {
        FragmentManager fm = getSupportFragmentManager();
        DuracaoDorFragment duracaoDorFragment = (DuracaoDorFragment) fm.findFragmentByTag("1.0");
        return duracaoDorFragment.verifyTimeDurationPain();
    }

    private boolean hasCrisisInProgressAndAlert() {
        FragmentManager fm = getSupportFragmentManager();
        DuracaoDorFragment duracaoDorFragment = (DuracaoDorFragment) fm.findFragmentByTag("1.0");

        return duracaoDorFragment.verifyCrisisInProgress();
    }

    private boolean hasReliefMethods()
    {
        boolean action = true;
        Crise crise =  realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                .findFirst();

        int nrMedicamentos = (crise.getMedicamentos() == null ? 0 : crise.getMedicamentos().size());
        int nrMetodosAlivio = (crise.getMetodos() == null ? 0 : crise.getMetodos().size());

        if(nrMedicamentos == 0 && nrMetodosAlivio == 0)
            action = false;

        return action;
    }

    public void nextQuestion(View view)
    {
        boolean next = true;

        if(mCurrentQuestion == QUESTION_DURACAO_DOR_ID && hasCrisisInProgressAndAlert() ||
                mCurrentQuestion == QUESTION_DURACAO_DOR_ID && !checkTempoDuracaoDor())
            next = false;

        if(next)
        {
            mOldQuestion = mCurrentQuestion;
            mCurrentQuestion++;
            salvarQuestao(mOldQuestion);

            if(mCurrentQuestion == QUESTION_METODO_ALIVIO_FUNCIONOU_ID && !hasReliefMethods())
                mCurrentQuestion = 10.0;

            if(mCurrentQuestion >= 4 && mCurrentQuestion < 5)
            {
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_msg_padrao, null);

                final Dialog dialog = new Dialog(this);

                Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
                Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);
                TextView messageTextView = (TextView) dialogView.findViewById(R.id.messageTextView);
                TextView titleTextView = (TextView) dialogView.findViewById(R.id.titleTextView);

                messageTextView.setText("PRIMEIRA ETAPA CONCLUÍDA! VOCÊ JÁ POSSUI INFORMAÇÃO MÍNIMA PARA FORNECER AO SEU MÉDICO. DESEJA CONTINUAR RESPONDENDO?");
                titleTextView.setText("AVISO");
                positiveButton.setText(resources.getString(R.string.dialogPatternMSGPositiveButton));
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        attachFragment(LocalizacaoDorFragment.newInstance(mSurveyManager), 1);
                        dialog.dismiss();
                    }
                });

                negativeButton.setText(resources.getString(R.string.dialogPatternMSGNegativeButton));
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ServiceFactory.getInstance().getServiceUtils().redirectTo(SurveyActivity.this, MainActivity.class);
                        mCurrentQuestion = 3.;
                        dialog.dismiss();
                    }
                });

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(dialogView);
                dialog.setCancelable(false);
                dialog.show();
            } else {
                if(next)
                    attachFragment(getFragment(), 1);
            }
        }
    }

    private boolean hasCrisisInProgress() {
        boolean retorno = false;

        RealmResults<Crise> crises = realm.where(Crise.class)
                .equalTo(CriseEntidade.DATAFIM, 0)
                .findAll();
        
        int sizeCrises = (crises == null ? 0 : crises.size());
        if(sizeCrises > 0) 
            retorno = true;
        
        return retorno;
    }

    private void salvarQuestao(double questao)
    {
        if(questao == QUESTION_DURACAO_DOR_ID)
        {
            boolean insertCreatedField = false;

            if(mSurveyManager.getCriseId() == null)
            {
                insertCreatedField = true;
                mSurveyManager.setCriseId(ServiceFactory.getInstance().getServiceUtils().generateHash(this));
            }

            long dataFim;
            long nrMaxCriseUsuario = realm.where(Crise.class).equalTo(CriseEntidade.USUARIO+"."+UsuarioEntidade.ID, mSurveyManager.getUserId()).count();
            int nrCrise = (int) nrMaxCriseUsuario+1;

            NotificationService notificationService = ServiceFactory.getInstance().getNotificationService(this);

            if(mCriseAindaNaoTerminou) {
                if(!mSurveyManager.isUpdateMode() && !hasCrisisInProgress()) {
                    String paramIntentMainActivity = "MainActivity";
                    Intent intent = new Intent(this, SumarioCriseActivity.class);
                    intent.putExtra(SurveyConstants.INTENT_CRISE_ID, mSurveyManager.getCriseId());
                    intent.putExtra(paramIntentMainActivity, paramIntentMainActivity);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                    notificationService.setPendingIntent(pendingIntent);
                    notificationService.setNotificationId(nrCrise);
                    notificationService.showNotificationCrisisInProgress();
                }

                dataFim = 0;
            }
            else {
                notificationService.setNotificationId((int) nrMaxCriseUsuario);
                notificationService.cancelNotificationCrisisInProgress();

                dataFim = mQuestionOneDataFim.getTimeInMillis();
            }

            try{
                realm.beginTransaction();

                Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, mSurveyManager.getUserId()).findFirst();
                Crise crise;

                if(insertCreatedField) {
                    crise = new Crise();
                    crise.setId(mSurveyManager.getCriseId());
                }
                else {
                    crise = realm.where(Crise.class).equalTo(CriseEntidade.ID, mSurveyManager.getCriseId()).findFirst();
                }

                crise.setUsuario(usuario);
                crise.setDataIni(mQuestionOneDataInicio.getTimeInMillis());
                crise.setDataFim(dataFim);

                if(insertCreatedField){
                    crise.setNrCrise(nrCrise);
                    crise.setCreated(ServiceFactory.getInstance().getServiceUtils().getCurrentDate());
                }

                realm.copyToRealmOrUpdate(crise);
                realm.commitTransaction();
                realm.refresh();
            }
            catch(Exception e){
                realm.cancelTransaction();
            }
        }
        else if(questao == QUESTION_INTENSIDADE_ID)
        {
            Crise crise = realm.where(Crise.class).equalTo(CriseEntidade.ID, mSurveyManager.getCriseId()).findFirst();
            try{
                realm.beginTransaction();

                crise.setIntensidade(mValorIntensidadeDor);

                realm.copyToRealmOrUpdate(crise);
                realm.commitTransaction();
                realm.refresh();
            }
            catch(Exception e){
                realm.cancelTransaction();
            }
        }
        else if(questao == QUESTION_QUALIDADE_DOR_ID)
        {
            Crise crise = realm.where(Crise.class).equalTo(CriseEntidade.ID, mSurveyManager.getCriseId()).findFirst();
            RealmList<QualidadeDor> qualidadeDorCriseList = crise.getQualidadeDors();
            int sizeQualidadeDorCriseList = (qualidadeDorCriseList == null ? 0 : qualidadeDorCriseList.size());

            if(mQualidadeDorOptionsSelected != null)
            {
                int sizeSelectedOptions = mQualidadeDorOptionsSelected.size();
                if(sizeQualidadeDorCriseList == 0 && sizeSelectedOptions > 0)
                {
                    for(int i = 0; i < sizeSelectedOptions; i++)
                    {
                        QualidadeDor opcao =   realm.where(QualidadeDor.class)
                                                    .equalTo(QualidadeDorEntidade.ID, mQualidadeDorOptionsSelected.get(i))
                                                    .findFirst();

                        try {
                            realm.beginTransaction();

                            crise.getQualidadeDors().add(opcao);

                            realm.copyToRealmOrUpdate(crise);
                            realm.commitTransaction();
                            realm.refresh();
                        } catch (Exception e) {
                            realm.cancelTransaction();
                        }
                    }
                } else if(sizeQualidadeDorCriseList > 0 && sizeSelectedOptions == 0)
                {
                    try {
                        realm.beginTransaction();

                        crise.getQualidadeDors().clear();

                        realm.commitTransaction();
                        realm.refresh();
                    } catch (Exception e) {
                        realm.cancelTransaction();
                    }
                } else if(sizeQualidadeDorCriseList > 0 && sizeSelectedOptions > 0) {

                    List<String> qualidadeDorCriseIdCompare = new ArrayList<>();
                    for(int i = 0; i < sizeQualidadeDorCriseList; i++)
                    {
                        qualidadeDorCriseIdCompare.add(qualidadeDorCriseList.get(i).getId());
                    }

                    for(int i = 0; i < sizeSelectedOptions; i++)
                    {
                        String selectedId = mQualidadeDorOptionsSelected.get(i);

                        if(!qualidadeDorCriseIdCompare.contains(selectedId))
                        {
                            QualidadeDor opcao = realm.where(QualidadeDor.class)
                                                       .equalTo(QualidadeDorEntidade.ID, selectedId)
                                                       .findFirst();
                            try {
                                realm.beginTransaction();

                                crise.getQualidadeDors().add(opcao);

                                realm.copyToRealmOrUpdate(crise);
                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }

                    for(int i = 0; i < qualidadeDorCriseIdCompare.size(); i++)
                    {
                        String qualidadeDorID = qualidadeDorCriseIdCompare.get(i);
                        if(!mQualidadeDorOptionsSelected.contains(qualidadeDorID))
                        {
                            try {
                                realm.beginTransaction();

                                for(int x=0; x < sizeQualidadeDorCriseList; x++)
                                {
                                    QualidadeDor dbOpcao = crise.getQualidadeDors().get(x);

                                    if(dbOpcao.getId().equals(qualidadeDorID)) {
                                        crise.getQualidadeDors().remove(x);
                                        break;
                                    }
                                }

                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }
                }
            }
        }
        else if(questao == QUESTION_SINTOMAS_ID)
        {
            Crise crise =  realm.where(Crise.class)
                                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                                .findFirst();

            RealmList<Sintoma> sintomas = crise.getSintomas();
            int sizeSintoma = (sintomas == null ? 0 : sintomas.size());

            if(mSintomasOptionsSelected != null)
            {
                int sizeSelectedOptions = mSintomasOptionsSelected.size();
                if(sizeSintoma == 0 && sizeSelectedOptions > 0)
                {
                    for(String sintomaID : mSintomasOptionsSelected)
                    {
                        Sintoma sintoma = realm.where(Sintoma.class)
                                .equalTo(SintomaEntidade.ID, sintomaID)
                                .findFirst();

                        try {
                            realm.beginTransaction();

                            crise.getSintomas().add(sintoma);
                            realm.copyToRealmOrUpdate(crise);

                            realm.commitTransaction();
                            realm.refresh();
                        } catch (Exception e) {
                            realm.cancelTransaction();
                        }
                    }
                } else if(sizeSintoma > 0 && sizeSelectedOptions == 0)
                {
                    try {
                        realm.beginTransaction();

                        crise.getSintomas().clear();

                        realm.commitTransaction();
                        realm.refresh();
                    } catch (Exception e) {
                        realm.cancelTransaction();
                    }
                } else if(sizeSintoma > 0 && sizeSelectedOptions > 0) {

                    List<String> sintomasIdCompare = new ArrayList<>();
                    for(int i = 0; i < sizeSintoma; i++)
                    {
                        sintomasIdCompare.add(sintomas.get(i).getId());
                    }

                    for(int i = 0; i < sizeSelectedOptions; i++)
                    {
                        String selectedId = mSintomasOptionsSelected.get(i);

                        if(!sintomasIdCompare.contains(selectedId))
                        {
                            Sintoma sintoma = realm.where(Sintoma.class).equalTo(SintomaEntidade.ID, selectedId).findFirst();
                            try {
                                realm.beginTransaction();

                                crise.getSintomas().add(sintoma);

                                realm.copyToRealmOrUpdate(crise);
                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }

                    for(int i = 0; i < sintomasIdCompare.size(); i++)
                    {
                        String sintomaID = sintomasIdCompare.get(i);
                        if(!mSintomasOptionsSelected.contains(sintomaID))
                        {
                            try {
                                realm.beginTransaction();

                                for(int x=0; x < sizeSintoma; x++)
                                {
                                    Sintoma dbOpcao = crise.getSintomas().get(x);

                                    if(dbOpcao.getId().equals(sintomaID)) {
                                        crise.getSintomas().remove(x);
                                        break;
                                    }
                                }

                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }
                }
            }
        }
        else if(questao == QUESTION_GATILHOS_ID)
        {
            Crise crise =  realm.where(Crise.class)
                    .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                    .findFirst();

            RealmList<GatilhoCrise> gatilhosCrise = crise.getGatilhosCrise();
            int sizeGatilhoCrise = 0;
            if(gatilhosCrise != null)
            {
                for(int i=0; i < gatilhosCrise.size(); i++)
                {
                    if(!gatilhosCrise.get(i).getGatilho().getNome().equals("esforço físico")
                            && !gatilhosCrise.get(i).getGatilho().getNome().equals("comida")
                            && !gatilhosCrise.get(i).getGatilho().getNome().equals("bebida")){
                        sizeGatilhoCrise++;
                    }
                }
            }

            if(mGatilhosOptionsSelected != null)
            {
                int sizeSelectedOptions = mGatilhosOptionsSelected.size();
                if(sizeGatilhoCrise == 0 && sizeSelectedOptions > 0)
                {
                    for(String gatilhoSelectedID : mGatilhosOptionsSelected)
                    {
                        Gatilho gatilho = realm.where(Gatilho.class)
                                .equalTo(GatilhoEntidade.ID, gatilhoSelectedID)
                                .findFirst();

                        try {
                            realm.beginTransaction();

                            GatilhoCrise gatilhoCrise = realm.createObject(GatilhoCrise.class);
                            gatilhoCrise.setId(ServiceFactory.getInstance().getServiceUtils().generateHash(this));
                            gatilhoCrise.setGatilho(gatilho);

                            crise.getGatilhosCrise().add(gatilhoCrise);
                            realm.copyToRealmOrUpdate(crise);

                            realm.commitTransaction();
                            realm.refresh();
                        } catch (Exception e) {
                            realm.cancelTransaction();
                        }
                    }
                } else if(sizeGatilhoCrise > 0 && sizeSelectedOptions == 0)
                {
                    try {
                        realm.beginTransaction();

                        for(int i=0; i < sizeGatilhoCrise; i++)
                        {
                            if(!gatilhosCrise.get(i).getGatilho().getNome().equals("esforço físico")
                                    && !gatilhosCrise.get(i).getGatilho().getNome().equals("comida")
                                    && !gatilhosCrise.get(i).getGatilho().getNome().equals("bebida")){
                                crise.getGatilhosCrise().remove(i);
                            }
                        }

                        realm.commitTransaction();
                        realm.refresh();
                    } catch (Exception e) {
                        realm.cancelTransaction();
                    }
                } else if(sizeGatilhoCrise > 0 && sizeSelectedOptions > 0) {

                    List<String> gatilhoCriseIdCompare = new ArrayList<>();
                    for(int i = 0; i < sizeGatilhoCrise; i++)
                    {
                        gatilhoCriseIdCompare.add(gatilhosCrise.get(i).getGatilho().getId());
                    }

                    for(int i = 0; i < sizeSelectedOptions; i++)
                    {
                        String selectedId = mGatilhosOptionsSelected.get(i);

                        if(!gatilhoCriseIdCompare.contains(selectedId))
                        {
                            Gatilho gatilho =  realm.where(Gatilho.class)
                                                    .equalTo(GatilhoEntidade.ID, selectedId)
                                                    .findFirst();
                            try {
                                realm.beginTransaction();

                                GatilhoCrise gatilhoCrise = realm.createObject(GatilhoCrise.class);
                                gatilhoCrise.setId(ServiceFactory.getInstance().getServiceUtils().generateHash(this));
                                gatilhoCrise.setGatilho(gatilho);

                                crise.getGatilhosCrise().add(gatilhoCrise);

                                realm.copyToRealmOrUpdate(crise);
                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }

                    for(int i = 0; i < gatilhoCriseIdCompare.size(); i++)
                    {
                        String gatilhoID = gatilhoCriseIdCompare.get(i);
                        if(!mGatilhosOptionsSelected.contains(gatilhoID))
                        {
                            try {
                                realm.beginTransaction();

                                for(int x=0; x < sizeGatilhoCrise; x++)
                                {
                                    GatilhoCrise dbOpcao = crise.getGatilhosCrise().get(x);

                                    if(dbOpcao.getGatilho().getId().equals(gatilhoID)) {
                                        crise.getGatilhosCrise().remove(x);
                                        break;
                                    }
                                }

                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }
                }
            }
        }
        else if(questao == 7.1)//gatilho - comida
        {
            addGatilho(mGatilhosComidaOptionsSelected, "comida", questao);
        }
        else if(questao == 7.2)//gatilho - bebida
        {
            addGatilho(mGatilhosBebidaOptionsSelected, "bebida", questao);
        }
        else if(questao == 7.3)//gatilho - esforço físico
        {
            addGatilho(mGatilhosEsforcoFisicoOptionsSelected, "esforço físico", questao);
        }
        else if(questao == QUESTION_METODO_ALIVIO_ID)
        {
            Crise crise =  realm.where(Crise.class)
                                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                                .findFirst();

            RealmList<MetodoAlivioCrise> metodos = crise.getMetodos();
            int sizeMetodos = (metodos == null ? 0 : metodos.size());

            if(mMetodoAlivioOptionsSelected != null)
            {
                int sizeSelectedOptions = mMetodoAlivioOptionsSelected.size();
                if(sizeMetodos == 0 && sizeSelectedOptions > 0)
                {
                    for(String metodoID : mMetodoAlivioOptionsSelected)
                    {
                        MetodoAlivio metodoAlivio = realm.where(MetodoAlivio.class)
                                .equalTo(MetodoAlivioEntidade.ID, metodoID)
                                .findFirst();

                        try {
                            realm.beginTransaction();

                            MetodoAlivioCrise metodoAlivioCrise = realm.createObject(MetodoAlivioCrise.class);
                            metodoAlivioCrise.setId(ServiceFactory.getInstance().getServiceUtils().generateHash(SurveyActivity.this));
                            metodoAlivioCrise.setMetodoAlivio(metodoAlivio);
                            metodoAlivioCrise.setFuncionou(false);

                            crise.getMetodos().add(metodoAlivioCrise);
                            realm.copyToRealmOrUpdate(crise);

                            realm.commitTransaction();
                            realm.refresh();
                        } catch (Exception e) {
                            realm.cancelTransaction();
                        }
                    }
                } else if(sizeMetodos > 0 && sizeSelectedOptions == 0)
                {
                    try {
                        realm.beginTransaction();

                        crise.getMetodos().clear();

                        realm.commitTransaction();
                        realm.refresh();
                    } catch (Exception e) {
                        realm.cancelTransaction();
                    }
                } else if(sizeMetodos > 0 && sizeSelectedOptions > 0) {

                    List<String> metodosIdCompare = new ArrayList<>();
                    for(int i = 0; i < sizeMetodos; i++)
                    {
                        metodosIdCompare.add(metodos.get(i).getMetodoAlivio().getId());
                    }

                    for(int i = 0; i < sizeSelectedOptions; i++)
                    {
                        String selectedID = mMetodoAlivioOptionsSelected.get(i);
                        MetodoAlivio metodoAlivio =  realm.where(MetodoAlivio.class)
                                .equalTo(MetodoAlivioEntidade.ID, selectedID)
                                .findFirst();

                        if(!metodosIdCompare.contains(metodoAlivio.getId()))
                        {
                            try {
                                realm.beginTransaction();

                                MetodoAlivioCrise metodoAlivioCrise = realm.createObject(MetodoAlivioCrise.class);
                                metodoAlivioCrise.setId(ServiceFactory.getInstance().getServiceUtils().generateHash(SurveyActivity.this));
                                metodoAlivioCrise.setMetodoAlivio(metodoAlivio);
                                metodoAlivioCrise.setFuncionou(false);

                                crise.getMetodos().add(metodoAlivioCrise);

                                realm.copyToRealmOrUpdate(crise);
                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }

                    for(int i = 0; i < metodosIdCompare.size(); i++)
                    {
                        if(!mMetodoAlivioOptionsSelected.contains(metodosIdCompare.get(i)))
                        {
                            try {
                                realm.beginTransaction();

                                for(int x=0; x < sizeMetodos; x++)
                                {
                                    MetodoAlivio dbOpcao = crise.getMetodos().get(x).getMetodoAlivio();

                                    if(dbOpcao.getId().equals(metodosIdCompare.get(i)))
                                    {
                                        crise.getMetodos().remove(x);
                                        break;
                                    }
                                }

                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }
                }
            }
        }
        else if(questao == QUESTION_METODO_ALIVIO_FUNCIONOU_ID)
        {
            Crise crise =  realm.where(Crise.class)
                    .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                    .findFirst();

            int sizeMedicamentos = (crise.getMedicamentos() == null ? 0 : crise.getMedicamentos().size());
            for(int i = 0; i < sizeMedicamentos; i++)
            {
                boolean funcionou = false;
                Medicamento medicamento = crise.getMedicamentos().get(i);
                if(medicamento.isFuncionou() && !mMedicamentoFuncionouOptionsSelected.contains(medicamento.getId()))
                    funcionou = false;
                else if(mMedicamentoFuncionouOptionsSelected.contains(medicamento.getId()))
                    funcionou = true;

                try {
                    realm.beginTransaction();

                    crise.getMedicamentos().get(i).setFuncionou(funcionou);
                    realm.copyToRealmOrUpdate(crise);

                    realm.commitTransaction();
                    realm.refresh();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
            }

            int sizeMetodos = (crise.getMetodos() == null ? 0 : crise.getMetodos().size());
            for(int i = 0; i < sizeMetodos; i++)
            {
                boolean funcionou = false;
                MetodoAlivioCrise metodo = crise.getMetodos().get(i);
                if(metodo.isFuncionou() && !mMetodoAlivioFuncionouOptionsSelected.contains(metodo.getMetodoAlivio().getId()))
                    funcionou = false;
                else if(mMetodoAlivioFuncionouOptionsSelected.contains(metodo.getMetodoAlivio().getId()))
                    funcionou = true;

                try {
                    realm.beginTransaction();

                    crise.getMetodos().get(i).setFuncionou(funcionou);
                    realm.copyToRealmOrUpdate(crise);

                    realm.commitTransaction();
                    realm.refresh();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
            }
        }
        else if(questao == QUESTION_CONSEQUENCIAS_ID)
        {
            Crise crise =  realm.where(Crise.class)
                    .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                    .findFirst();

            RealmList<Consequencia> consequencias = crise.getConsequencias();
            int sizeConsequencias = (consequencias == null ? 0 : consequencias.size());

            if(mConsequenciaOptionsSelected != null)
            {
                int sizeSelectedOptions = mConsequenciaOptionsSelected.size();
                if(sizeConsequencias == 0 && sizeSelectedOptions > 0)
                {
                    for(String consequenciaID : mConsequenciaOptionsSelected)
                    {
                        Consequencia consequencia = realm.where(Consequencia.class)
                                .equalTo(ConsequenciaEntidade.ID, consequenciaID)
                                .findFirst();

                        try {
                            realm.beginTransaction();

                            crise.getConsequencias().add(consequencia);
                            realm.copyToRealmOrUpdate(crise);

                            realm.commitTransaction();
                            realm.refresh();
                        } catch (Exception e) {
                            realm.cancelTransaction();
                        }
                    }
                } else if(sizeConsequencias > 0 && sizeSelectedOptions == 0)
                {
                    try {
                        realm.beginTransaction();

                        crise.getConsequencias().clear();

                        realm.commitTransaction();
                        realm.refresh();
                    } catch (Exception e) {
                        realm.cancelTransaction();
                    }
                } else if(sizeConsequencias > 0 && sizeSelectedOptions > 0) {

                    List<String> consequenciasIdCompare = new ArrayList<>();
                    for(int i = 0; i < sizeConsequencias; i++)
                    {
                        consequenciasIdCompare.add(consequencias.get(i).getId());
                    }

                    for(int i = 0; i < sizeSelectedOptions; i++)
                    {
                        String selectedId = mConsequenciaOptionsSelected.get(i);

                        if(!consequenciasIdCompare.contains(selectedId))
                        {
                            Consequencia consequencia =  realm.where(Consequencia.class)
                                    .equalTo(ConsequenciaEntidade.ID, selectedId)
                                    .findFirst();

                            try {
                                realm.beginTransaction();

                                crise.getConsequencias().add(consequencia);

                                realm.copyToRealmOrUpdate(crise);
                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }

                    for(int i = 0; i < consequenciasIdCompare.size(); i++)
                    {
                        String consequenciaID = consequenciasIdCompare.get(i);
                        if(!mConsequenciaOptionsSelected.contains(consequenciaID))
                        {
                            try {
                                realm.beginTransaction();

                                for(int x=0; x < sizeConsequencias; x++) {
                                    Consequencia dbOpcao = crise.getConsequencias().get(x);

                                    if(dbOpcao.getId().equals(consequenciaID)) {
                                        crise.getConsequencias().remove(x);
                                        break;
                                    }
                                }

                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }
                }
            }
        }
        else if(questao == QUESTION_LOCAL_ID)
        {
            Crise crise =  realm.where(Crise.class)
                    .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                    .findFirst();

            RealmList<Local> locais = crise.getLocais();
            int sizeLocais = (locais == null ? 0 : locais.size());

            if(mLocalOptionsSelected != null)
            {
                int sizeSelectedOptions = mLocalOptionsSelected.size();
                if(sizeLocais == 0 && sizeSelectedOptions > 0)
                {
                    for(String localID : mLocalOptionsSelected)
                    {
                        Local local = realm.where(Local.class)
                                .equalTo(LocalEntidade.ID, localID)
                                .findFirst();

                        try {
                            realm.beginTransaction();

                            crise.getLocais().add(local);
                            realm.copyToRealmOrUpdate(crise);

                            realm.commitTransaction();
                            realm.refresh();
                        } catch (Exception e) {
                            realm.cancelTransaction();
                        }
                    }
                } else if(sizeLocais > 0 && sizeSelectedOptions == 0)
                {
                    try {
                        realm.beginTransaction();

                        crise.getLocais().clear();

                        realm.commitTransaction();
                        realm.refresh();
                    } catch (Exception e) {
                        realm.cancelTransaction();
                    }
                } else if(sizeLocais > 0 && sizeSelectedOptions > 0) {

                    List<String> locaisIdCompare = new ArrayList<>();
                    for(int i = 0; i < sizeLocais; i++)
                    {
                        locaisIdCompare.add(locais.get(i).getId());
                    }

                    for(int i = 0; i < sizeSelectedOptions; i++)
                    {
                        String selectedId = mLocalOptionsSelected.get(i);

                        if(!locaisIdCompare.contains(selectedId))
                        {
                            Local local =  realm.where(Local.class)
                                    .equalTo(LocalEntidade.ID, selectedId)
                                    .findFirst();

                            try {
                                realm.beginTransaction();

                                crise.getLocais().add(local);

                                realm.copyToRealmOrUpdate(crise);
                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }

                    for(int i = 0; i < locaisIdCompare.size(); i++)
                    {
                        String localID = locaisIdCompare.get(i);
                        if(!mLocalOptionsSelected.contains(localID))
                        {
                            try {
                                realm.beginTransaction();

                                for(int x=0; x < sizeLocais; x++) {
                                    Local dbOpcao = crise.getLocais().get(x);

                                    if(dbOpcao.getId().equals(localID)) {
                                        crise.getLocais().remove(x);
                                        break;
                                    }
                                }

                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                        }
                    }
                }
            }
        }

        boolean isSubQuestion = (mCurrentQuestion > 7 && mCurrentQuestion < 8);
        if(mSurveyManager.isUpdateMode() && !isSubQuestion){
            onBackPressed();
        } else if(isSubQuestion && mCurrentQuestion == questao)
        {
            onBackPressed();
        }
    }

    private void dialogContinuarMaisTarde()
    {
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_msg_padrao, null);

        final Dialog dialog = new Dialog(this);
        Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
        Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);
        TextView messageTextView = (TextView) dialogView.findViewById(R.id.messageTextView);
        TextView titleTextView = (TextView) dialogView.findViewById(R.id.titleTextView);

        messageTextView.setText("FALTAM APENAS "+(QTD_PERGUNTAS-mCurrentQuestion.intValue())+" QUESTÕES.");
        titleTextView.setText("TEM CERTEZA QUE DESEJA CONTINUAR MAIS TARDE?");

        positiveButton.setText(resources.getString(R.string.dialogPatternMSGPositiveButton));
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        negativeButton.setText(resources.getString(R.string.dialogPatternMSGNegativeButton));
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ServiceFactory.getInstance().getServiceUtils().redirectTo(SurveyActivity.this, MainActivity.class);
                finish();
            }
        });

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void addGatilho(List<String> opcoesSelecionadas, String opcaoGatilho, double questao)
    {
        Crise crise =  realm.where(Crise.class)
                .equalTo(CriseEntidade.ID, mSurveyManager.getCriseId())
                .findFirst();

        RealmList<GatilhoCrise> gatilhos = crise.getGatilhosCrise();
        int sizeGatilhos = (gatilhos == null ? 0 : gatilhos.size());

        if(opcoesSelecionadas != null)
        {
            int sizeSubGatilho = 0;
            int sizeSelectedOptions = opcoesSelecionadas.size();
            if(sizeGatilhos > 0)
            {
                GatilhoCrise gatilhoExists = gatilhos.where()
                        .equalTo(GatilhoCriseEntidade.GATILHO+ "." + GatilhoEntidade.NOME, opcaoGatilho)
                        .findFirst();
                if(gatilhoExists == null)
                {
                    Gatilho gatilho = realm.where(Gatilho.class)
                            .equalTo(GatilhoEntidade.NOME, opcaoGatilho)
                            .findFirst();

                    try {
                        realm.beginTransaction();

                        GatilhoCrise gatilhoCrise = realm.createObject(GatilhoCrise.class);
                        gatilhoCrise.setId(ServiceFactory.getInstance().getServiceUtils().generateHash(this));
                        gatilhoCrise.setGatilho(gatilho);

                        crise.getGatilhosCrise().add(gatilhoCrise);
                        realm.copyToRealmOrUpdate(crise);

                        realm.commitTransaction();
                        realm.refresh();
                    } catch (Exception e) {
                        realm.cancelTransaction();
                    }
                } else {
                    if(questao == 7.1)
                        sizeSubGatilho = (gatilhoExists.getComidas() == null ? 0 : gatilhoExists.getComidas().size());
                    else if(questao == 7.2)
                        sizeSubGatilho = (gatilhoExists.getBebidas() == null ? 0 : gatilhoExists.getBebidas().size());
                    else if(questao == 7.3)
                        sizeSubGatilho = (gatilhoExists.getEsforcoFisicos() == null ? 0 : gatilhoExists.getEsforcoFisicos().size());
                }
            } else if(sizeSelectedOptions > 0){
                Gatilho gatilho = realm.where(Gatilho.class)
                        .equalTo(GatilhoEntidade.NOME, opcaoGatilho)
                        .findFirst();

                try {
                    realm.beginTransaction();

                    GatilhoCrise gatilhoCrise = realm.createObject(GatilhoCrise.class);
                    gatilhoCrise.setId(ServiceFactory.getInstance().getServiceUtils().generateHash(this));
                    gatilhoCrise.setGatilho(gatilho);

                    crise.getGatilhosCrise().add(gatilhoCrise);
                    realm.copyToRealmOrUpdate(crise);

                    realm.commitTransaction();
                    realm.refresh();
                } catch (Exception e) {
                    realm.cancelTransaction();
                }
            }

            if(sizeSubGatilho == 0 && sizeSelectedOptions > 0)
            {
                for(String subGatilhoID : opcoesSelecionadas)
                {
                    Comida comida = null;
                    Bebida bebida = null;
                    EsforcoFisico esforcoFisico = null;
                    if(questao == 7.1)
                    {
                        comida = realm.where(Comida.class)
                                .equalTo(ComidaEntidade.ID, subGatilhoID)
                                .findFirst();
                    }
                    else if(questao == 7.2)
                    {
                        bebida = realm.where(Bebida.class)
                                .equalTo(BebidaEntidade.ID, subGatilhoID)
                                .findFirst();
                    }
                    else if(questao == 7.3)
                    {
                        esforcoFisico = realm.where(EsforcoFisico.class)
                                .equalTo(EsforcoFisicoEntidade.ID, subGatilhoID)
                                .findFirst();
                    }

                    for(int i=0; i < crise.getGatilhosCrise().size(); i++)
                    {
                        if(crise.getGatilhosCrise().get(i).getGatilho().getNome().equals(opcaoGatilho))
                        {
                            try {
                                realm.beginTransaction();

                                if(questao == 7.1)
                                    crise.getGatilhosCrise().get(i).getComidas().add(comida);
                                else if (questao == 7.2)
                                    crise.getGatilhosCrise().get(i).getBebidas().add(bebida);
                                else if(questao == 7.3)
                                    crise.getGatilhosCrise().get(i).getEsforcoFisicos().add(esforcoFisico);

                                realm.commitTransaction();
                                realm.refresh();
                            } catch (Exception e) {
                                realm.cancelTransaction();
                            }
                            break;
                        }
                    }
                }
            } else if(sizeSubGatilho > 0 && sizeSelectedOptions == 0)
            {
                for(int i=0; i < crise.getGatilhosCrise().size(); i++)
                {
                    if(crise.getGatilhosCrise().get(i).getGatilho().getNome().equals(opcaoGatilho))
                    {
                        try {
                            realm.beginTransaction();

                            crise.getGatilhosCrise().remove(i);

                            realm.commitTransaction();
                            realm.refresh();
                        } catch (Exception e) {
                            realm.cancelTransaction();
                        }
                        break;
                    }
                }
            } else if(sizeSubGatilho > 0 && sizeSelectedOptions > 0) {

                List<String> subGatilhoIdCompare = new ArrayList<>();
                for(int i = 0; i < sizeSubGatilho; i++)
                {
                    if(crise.getGatilhosCrise().get(i).getGatilho().getNome().equals(opcaoGatilho))
                    {
                        int size = 0;
                        if(questao == 7.1)
                            size = crise.getGatilhosCrise().get(i).getComidas().size();
                        else if(questao == 7.2)
                            crise.getGatilhosCrise().get(i).getBebidas().size();
                        else
                            size = crise.getGatilhosCrise().get(i).getEsforcoFisicos().size();

                        for(int x=0; x < size; x++)
                        {
                            if(questao == 7.1)
                                subGatilhoIdCompare.add(crise.getGatilhosCrise().get(i).getComidas().get(x).getId());
                            else if(questao == 7.2)
                                subGatilhoIdCompare.add(crise.getGatilhosCrise().get(i).getBebidas().get(x).getId());
                            else
                                subGatilhoIdCompare.add(crise.getGatilhosCrise().get(i).getEsforcoFisicos().get(x).getId());
                        }
                        break;
                    }
                }

                for(int i = 0; i < sizeSelectedOptions; i++)
                {
                    String selectedId = opcoesSelecionadas.get(i);

                    if(!subGatilhoIdCompare.contains(selectedId))
                    {
                        Comida comida = null;
                        Bebida bebida = null;
                        EsforcoFisico esforcoFisico = null;
                        if(questao == 7.1)
                        {
                            comida = realm.where(Comida.class)
                                    .equalTo(ComidaEntidade.ID, selectedId)
                                    .findFirst();
                        }
                        else if(questao == 7.2)
                        {
                            bebida = realm.where(Bebida.class)
                                    .equalTo(BebidaEntidade.ID, selectedId)
                                    .findFirst();
                        }
                        else {
                            esforcoFisico = realm.where(EsforcoFisico.class)
                                    .equalTo(EsforcoFisicoEntidade.ID, selectedId)
                                    .findFirst();
                        }

                        for(int x=0; x < crise.getGatilhosCrise().size(); x++) {
                            if (crise.getGatilhosCrise().get(x).getGatilho().getNome().equals(opcaoGatilho)) {
                                try {
                                    realm.beginTransaction();

                                    if(questao == 7.1)
                                        crise.getGatilhosCrise().get(x).getComidas().add(comida);
                                    else if(questao == 7.2)
                                        crise.getGatilhosCrise().get(x).getBebidas().add(bebida);
                                    else
                                        crise.getGatilhosCrise().get(x).getEsforcoFisicos().add(esforcoFisico);

                                    realm.copyToRealmOrUpdate(crise);
                                    realm.commitTransaction();
                                    realm.refresh();
                                } catch (Exception e) {
                                    realm.cancelTransaction();
                                }
                            }
                        }
                    }
                }

                for(int s = 0; s < subGatilhoIdCompare.size(); s++)
                {
                    String subGatilhoBDId = subGatilhoIdCompare.get(s);
                    if(!opcoesSelecionadas.contains(subGatilhoBDId))
                    {
                        for(int i=0; i < sizeGatilhos; i++)
                        {
                            if(crise.getGatilhosCrise().get(i).getGatilho().getNome().equals(opcaoGatilho))
                            {
                                int size = 0;
                                if(questao == 7.1)
                                    size = crise.getGatilhosCrise().get(i).getComidas().size();
                                else if(questao == 7.2)
                                    crise.getGatilhosCrise().get(i).getBebidas().size();
                                else
                                    size = crise.getGatilhosCrise().get(i).getEsforcoFisicos().size();

                                for(int x=0; x < size; x++)
                                {
                                    String subGatilhoLinhaBDId;
                                    if(questao == 7.1)
                                        subGatilhoLinhaBDId = crise.getGatilhosCrise().get(i).getComidas().get(x).getId();
                                    else if(questao == 7.2)
                                        subGatilhoLinhaBDId = crise.getGatilhosCrise().get(i).getBebidas().get(x).getId();
                                    else
                                        subGatilhoLinhaBDId = crise.getGatilhosCrise().get(i).getEsforcoFisicos().get(x).getId();

                                    if(subGatilhoLinhaBDId.equals(subGatilhoBDId))
                                    {
                                        try {
                                            realm.beginTransaction();

                                            if(questao == 7.1)
                                                crise.getGatilhosCrise().get(i).getComidas().remove(x);
                                            else if(questao == 7.2)
                                                crise.getGatilhosCrise().get(i).getBebidas().remove(x);
                                            else
                                                crise.getGatilhosCrise().get(i).getEsforcoFisicos().remove(x);

                                            realm.commitTransaction();
                                            realm.refresh();
                                        } catch (Exception e) {
                                            realm.cancelTransaction();
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onQuestionOneListener(Calendar calendarInicio, Calendar calendarFim, boolean criseAindaNaoTerminou) {
        mQuestionOneDataInicio = calendarInicio;
        mQuestionOneDataFim = calendarFim;
        mCriseAindaNaoTerminou = criseAindaNaoTerminou;
    }

    @Override
    public void onChangeIntensidadeDor(int data) {
        mValorIntensidadeDor = data;
    }

    @Override
    public void onGatilhosOptionsUpdated(List<String> selectedOptions) {
        mGatilhosOptionsSelected = selectedOptions;
    }

    @Override
    public void onGatilhosComidaOptionsUpdated(List<String> selectedOptions) {
        mGatilhosComidaOptionsSelected = selectedOptions;
    }

    @Override
    public void onGatilhosBebidaOptionsUpdated(List<String> selectedOptions) {
        mGatilhosBebidaOptionsSelected = selectedOptions;
    }

    @Override
    public void onGatilhosEsforcoFisicoOptionsUpdated(List<String> selectedOptions) {
        mGatilhosEsforcoFisicoOptionsSelected = selectedOptions;
    }

    @Override
    public void onQuestionNineNavigationListener(int navigation) {
        if( navigation == 3 ) mCurrentQuestion = 7.3;
        if( navigation == 6 ) mCurrentQuestion = 7.2;
        if( navigation == 7 ) mCurrentQuestion = 7.1;

        mOldQuestion = (double) mCurrentQuestion.intValue();
        salvarQuestao(mOldQuestion);
        attachFragment(getFragment(), 1);
    }

    @Override
    public void onMetodoAlivioFuncionouOptionsUpdated(List<String> selectedMedicamentoOptions,
                                                      List<String> selectedMetodoAlivioOptions)
    {
        mMetodoAlivioFuncionouOptionsSelected = selectedMetodoAlivioOptions;
        mMedicamentoFuncionouOptionsSelected = selectedMedicamentoOptions;
    }

    @Override
    public void onQualidadeDorOptionsUpdated(List<String> selectedOptions)
    {
        mQualidadeDorOptionsSelected = selectedOptions;
    }

    @Override
    public void onSintomasOptionsUpdated(List<String> selectedOptions) {
        mSintomasOptionsSelected = selectedOptions;
    }

    @Override
    public void onMetodoAlivioOptionsUpdated(List<String> selectedOptions) {
        mMetodoAlivioOptionsSelected = selectedOptions;
    }

    @Override
    public void onConsequenciaOptionsUpdated(List<String> selectedOptions) {
        mConsequenciaOptionsSelected = selectedOptions;
    }

    @Override
    public void onLocalOptionsUpdated(List<String> selectedOptions) {
        mLocalOptionsSelected = selectedOptions;
    }
}
