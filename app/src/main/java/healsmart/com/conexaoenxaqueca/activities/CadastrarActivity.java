package healsmart.com.conexaoenxaqueca.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.ArrayList;
import java.util.Arrays;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SaveUsuarioTask;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;

public class CadastrarActivity extends AppCompatActivity implements PerfilUsuario
{

    private Toolbar mToolbar;

    private TextView mSexoTextView;
    private EditText mEmailEditText;
    private EditText mSenhaEditText;

    private CallbackManager callbackManager;
    private Resources mResources;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ServiceFactory.iniciar();
        ServiceFactory.getInstance().getServiceUtils().hideStatusBar(this);

        setContentView(R.layout.activity_cadastrar);

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        ServiceFactory.iniciar();
        initViews();
        mResources = getResources();

        mToolbar.setTitle(mResources.getString(R.string.login_cadastre_se));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        realm = Realm.getDefaultInstance();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    /**
     * Método que inicia os componentes do layout XML inflado da atividade {@link CadastrarActivity}.
     * Também inicia eventos, atributos, variáveis e o que for necessário para inicialização da Atividade.
     * Este método sempre deve ser utilizado no onCreate()
     *
     * @return void
     */
    private void initViews() {

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if(mEmailEditText.getText().hashCode() == s.hashCode())
                {
                    ServiceFactory.getInstance().getServiceValidate().verifyAndClearErrors(mEmailEditText);
                }
                if(mSenhaEditText.getText().hashCode() == s.hashCode())
                {
                    ServiceFactory.getInstance().getServiceValidate().verifyAndClearErrors(mSenhaEditText);
                }
            }
        };

        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mSexoTextView = (TextView) findViewById(R.id.sexoTextView);

        mEmailEditText = (EditText) findViewById(R.id.emailEditText);
        mEmailEditText.addTextChangedListener(textWatcher);

        mSenhaEditText = (EditText) findViewById(R.id.senhaEditText);
        mSenhaEditText.addTextChangedListener(textWatcher);
    }

    public void cadastroFacebook(View v){

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile", "user_friends", "user_birthday"));

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        ServiceFactory.getInstance().getServiceLoginManager().salvarUsuario(loginResult, CadastrarActivity.this);
                    }

                    @Override
                    public void onCancel() {
                        LoginManager.getInstance().logOut();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.dialog_msg_padrao, null);

                        final Dialog dialog = new Dialog(CadastrarActivity.this);

                        Button positiveButton = (Button) dialogView.findViewById(R.id.positiveButton);
                        Button negativeButton = (Button) dialogView.findViewById(R.id.negativeButton);
                        TextView messageTextView = (TextView) dialogView.findViewById(R.id.messageTextView);
                        TextView titleTextView = (TextView) dialogView.findViewById(R.id.titleTextView);

                        messageTextView.setText("NÃO FOI POSSÍVEL CONECTAR-SE COM O FACEBOOK, VERIFIQUE SUA CONEXÃO COM A INTERNET. PARA CONTIUAR FAÇA O LOGIN OFF-LINE!");
                        titleTextView.setText("OPS!");
                        positiveButton.setText("LOGIN OFF-LINE");
                        positiveButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        negativeButton.setText(mResources.getString(R.string.dialogPatternAddOptionNegativeButton));
                        negativeButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(dialogView);
                        dialog.setCancelable(false);
                        dialog.show();
                    }
                }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void cadastrarUsuario(View v) {

        if (v.getId() == R.id.cadastrarButton) {
            if (validateFields()) {

                String email = mEmailEditText.getText().toString();

                Usuario usuario =  realm.where(Usuario.class)
                                        .equalTo(UsuarioEntidade.EMAIL, email)
                                        .findFirst();
                String idUsuario;
                if( usuario == null )
                {
                    idUsuario = ServiceFactory.getInstance().getServiceUtils().generateHash(CadastrarActivity.this);
                } else {
                    idUsuario = usuario.getId();
                }

                String senha = mSenhaEditText.getText().toString();
                String genero = mSexoTextView.getText().toString();

                usuario = ServiceFactory.getInstance().getServiceUsuario();
                usuario.setId(idUsuario);
                usuario.setEmail(email);
                usuario.setSenha(senha);
                usuario.setGenero(genero);

                new SaveUsuarioTask(CadastrarActivity.this).execute(usuario);
            }
        }
    }

    /**
     * Método de validação da activity. Responsável por verificar e validar o formulário de cadastro.
     *
     * @return boolean
     */
    private boolean validateFields() {
        String sexo = mSexoTextView.getText().toString().trim();

        return ( !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(mEmailEditText, mResources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().hasSizeValid(mEmailEditText, 5, "Seu e-mail deve conter no mínimo 5 caracteres.")
                && ServiceFactory.getInstance().getServiceValidate().isValidEmail(mEmailEditText)
                && !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(mSenhaEditText, mResources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().hasSizeValid(mSenhaEditText, 4, "Sua senha deve conter no mínimo 4 caracteres.")
                && !isEmptyFields(sexo));
    }

    private boolean isEmptyFields(String sexo) {
        if(sexo.equalsIgnoreCase("Sexo")){
            mSexoTextView.requestFocus();
            mSexoTextView.setError(mResources.getString(R.string.validacao_campo_obrigatorio));
            return true;
        }
        return false;
    }

    public void escolherSexo(View v) {

        ArrayList<String> itens = new ArrayList<String>();
        itens.add("Feminino");
        itens.add("Masculino");

        Context context = v.getContext();

        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.item_dialog, itens);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Sexo").setSingleChoiceItems(adapter, 0, dialogSexoTextViewOnClick);

        AlertDialog alerta = builder.create();
        alerta.show();
    }

    DialogInterface.OnClickListener dialogSexoTextViewOnClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String valorOpcao = (which == 0) ? "Feminino" : "Masculino";
            mSexoTextView.setText(valorOpcao);

            if(!mSexoTextView.getText().toString().equalsIgnoreCase("Sexo"))
            {
                mSexoTextView.setError(null);
            }

            dialog.dismiss();
        }
    };
}
