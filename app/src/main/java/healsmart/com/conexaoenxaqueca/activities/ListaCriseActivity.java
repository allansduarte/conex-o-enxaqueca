package healsmart.com.conexaoenxaqueca.activities;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.adapters.ListaCriseAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmResults;

public class ListaCriseActivity extends AppCompatActivity implements  PerfilUsuario
{
    private ListView mListaCriseListView;
    private ListaCriseAdapter mAdapter;

    private Drawer navigationDrawerLeft;
    private Toolbar mToolbar;
    private TextView mFimDataCriseTextView;
    private TextView mInicioDataCriseTextView;
    private TextView mMsgInfoTextView;

    private Calendar mCalendar;//revisar
    private Calendar mCalendarInicio;
    private Calendar mCalendarFim;
    private int mYear = 2000;
    private int mMonth;
    private int mDay = 1;
    private int mHour;
    private int mMinute;
    private Animation mAnimation;

    private Realm realm;

    private List<Crise> mListCrise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_crise);

        ServiceFactory.iniciar();

        realm = Realm.getDefaultInstance();
        initViews();

        navigationDrawerLeft = ServiceFactory.getInstance().getServiceNavigationDrawer(ListaCriseActivity.this, mToolbar, R.id.drawer_container).run();
    }

    private void notifyView()
    {
        int sizeLisCrise = (mListCrise == null ? 0 : mListCrise.size());
        if(sizeLisCrise > 0)
        {
            mListaCriseListView.setVisibility(View.VISIBLE);
            mMsgInfoTextView.setVisibility(View.GONE);
            mAdapter.setCrises(mListCrise);
        }
        else
        {
            mListaCriseListView.setVisibility(View.GONE);
            mMsgInfoTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mListCrise = getListContent();
        notifyView();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_lista_crise, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(navigationDrawerLeft.isDrawerOpen()){
            navigationDrawerLeft.closeDrawer();
        }
        else{
            super.onBackPressed();
        }
    }

    private void initViews()
    {
        mInicioDataCriseTextView = (TextView) findViewById(R.id.inicioDataCriseTextView);
        mFimDataCriseTextView = (TextView) findViewById(R.id.fimDataCriseTextView);
        mListaCriseListView = (ListView) findViewById(R.id.listaCriseListView);
        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mMsgInfoTextView = (TextView) findViewById(R.id.msgInfoTextView);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents() {
        mToolbar.setTitle("Minhas crises");
        mAdapter = new ListaCriseAdapter(ListaCriseActivity.this, new ArrayList<Crise>());
        mListaCriseListView.setAdapter(mAdapter);

        mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);

        mCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.YEAR, year);

        mAnimation = AnimationUtils.loadAnimation(ListaCriseActivity.this, R.anim.bounce_up);
        mCalendarInicio = (Calendar) mCalendar.clone();
        atualizarData(mInicioDataCriseTextView);

        mCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        mCalendarFim = (Calendar) mCalendar.clone();
        atualizarData(mFimDataCriseTextView);
    }

    public void onClickSelecionarData(View v) {
        mAnimation = AnimationUtils.loadAnimation(ListaCriseActivity.this, R.anim.bounce_up);
        switch (v.getId()){
            case R.id.inicioDataCriseTextView:
                callAndUpdateDialogDatePicker(mInicioDataCriseTextView, mCalendarInicio);
                break;
            case R.id.fimDataCriseTextView:
                callAndUpdateDialogDatePicker(mFimDataCriseTextView, mCalendarFim);
                break;
        }
    }

    private void callAndUpdateDialogDatePicker(final TextView textView, Calendar calendar)
    {

        int mHour=0,mMinute=0;

        if(calendar == null) {
            mCalendar   =   Calendar.getInstance();
        } else {
            mCalendar = calendar;
            mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
            mMinute = mCalendar.get(Calendar.MINUTE);
        }

        setPickerYear(mCalendar.get(Calendar.YEAR));
        setPickerMonth(mCalendar.get(Calendar.MONTH));
        setPickerDay(mCalendar.get(Calendar.DAY_OF_MONTH));
        setPickerHour(mHour);
        setPickerMinute(mMinute);

        DatePickerDialog mDatePicker = new DatePickerDialog(ListaCriseActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.HOUR_OF_DAY,getPickerHour());
                        calendar.set(Calendar.MINUTE,getPickerMinute());
                        calendar.set(Calendar.SECOND,0);
                        calendar.set(Calendar.MILLISECOND,0);

                        mCalendar = calendar;
                        atualizarData(textView);
                    }
                }, getPickerYear(), getPickerMonth(), getPickerDay());
        mDatePicker.show();
    }

    private void atualizarData(TextView textView) {
        if( textView.getId() == R.id.inicioDataCriseTextView){
            mCalendarInicio = (Calendar) mCalendar.clone();
            mCalendarInicio.set(Calendar.HOUR_OF_DAY, 0);
            mCalendarInicio.set(Calendar.MINUTE, 0);
        }

        else if(textView.getId() == R.id.fimDataCriseTextView){
            mCalendarFim = (Calendar) mCalendar.clone();
            mCalendarFim.set(Calendar.HOUR_OF_DAY, 23);
            mCalendarFim.set(Calendar.MINUTE, 59);
        }

        if(mCalendarInicio != null && mCalendarFim != null)
        {
            onStart();
        }

        SimpleDateFormat dateFormatDate = new SimpleDateFormat("dd/MM/yyyy");
        textView.setText(dateFormatDate.format(mCalendar.getTime()));
        textView.startAnimation(mAnimation);
    }

    public int getPickerYear() {
        return mYear;
    }

    public void setPickerYear(int mYear) {
        this.mYear = mYear;
    }

    public int getPickerMonth() {
        return mMonth;
    }

    public void setPickerMonth(int mMonth) {
        this.mMonth = mMonth;
    }

    public int getPickerDay() {
        return mDay;
    }

    public void setPickerDay(int mDay) {
        this.mDay = mDay;
    }

    public int getPickerHour() {
        return mHour;
    }

    public void setPickerHour(int mHour) {
        this.mHour = mHour;
    }

    public int getPickerMinute() {
        return mMinute;
    }

    public void setPickerMinute(int mMinute) {
        this.mMinute = mMinute;
    }

    private List<Crise> getListContent()
    {
        SharedPreferences mPreferences = getSharedPreferences(USUARIO_PREFS, MODE_PRIVATE);
        String userId = mPreferences.getString(USUARIO_PREFS_USUARIO_ID, null);

        realm.refresh();
        RealmResults<Crise> crises =   realm.where(Crise.class)
                .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, userId)
                .greaterThanOrEqualTo(CriseEntidade.DATAINI, mCalendarInicio.getTimeInMillis())
                .lessThanOrEqualTo(CriseEntidade.DATAFIM, mCalendarFim.getTimeInMillis())
                .findAll();

        List<Crise> criseList = new ArrayList<>();
        for(Crise crise : crises)
        {
            criseList.add(Crise.copy(crise));
        }

        return criseList;
    }

    public void onClickRegistrarCrise(View v)
    {
        ServiceFactory.getInstance().getServiceUtils().redirectTo(this, MainActivity.class);
    }
}
