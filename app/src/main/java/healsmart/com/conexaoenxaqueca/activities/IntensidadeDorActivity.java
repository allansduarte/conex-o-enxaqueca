package healsmart.com.conexaoenxaqueca.activities;

import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.DatePicker;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import healsmart.com.conexaoenxaqueca.ChartsJavaScriptInterface;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.Utils.NavigationDrawer;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.DrawerMenu;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;

public class IntensidadeDorActivity extends AppCompatActivity implements PerfilUsuario, DrawerMenu
{
    private Toolbar mToolbar;
    private WebView mChartWebView;
    private Resources mResource;

    private TextView mFimDataCriseTextView;
    private TextView mInicioDataCriseTextView;
    private TextView nrTotalCriseTextView;
    private TextView nrCriseFracaTextView;
    private TextView nrCriseMediaTextView;
    private TextView nrCriseForteTextView;

    private Calendar mCalendar;//revisar
    private Calendar mCalendarInicio;
    private Calendar mCalendarFim;
    private int mYear = 2000;
    private int mMonth;
    private int mDay = 1;
    private int mHour;
    private int mMinute;
    private Animation animation;

    private String mUserId;
    private  ChartsJavaScriptInterface chartsJavaScriptInterface;
    private Drawer navigationDrawerLeft;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ServiceFactory.iniciar();
        ServiceFactory.getInstance().getServiceUtils().hideStatusBar(IntensidadeDorActivity.this);

        setContentView(R.layout.activity_relatorio_intensidade_dor);

        realm = Realm.getDefaultInstance();
        initViews();

        navigationDrawerLeft = new NavigationDrawer(this, mToolbar, R.id.drawer_container).run();
        mChartWebView.loadUrl("file:///android_asset/html/android-pie.html");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_intensidade_dor, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //    return true;
        //}

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        try{
            mChartWebView.stopLoading();
        }catch(Exception e){
            e.printStackTrace();
        }

        super.onStop();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if(navigationDrawerLeft.isDrawerOpen())
            navigationDrawerLeft.closeDrawer();
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(navigationDrawerLeft.isDrawerOpen()){
            navigationDrawerLeft.closeDrawer();
        }
        else{
            super.onBackPressed();
        }
    }

    private void initViews()
    {
        mChartWebView = (WebView) findViewById(R.id.chartWebView);
        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mInicioDataCriseTextView = (TextView) findViewById(R.id.inicioDataCriseTextView);
        mFimDataCriseTextView = (TextView) findViewById(R.id.fimDataCriseTextView);
        mResource = getResources();
        nrTotalCriseTextView = (TextView) findViewById(R.id.nrTotalCriseTextView);
        nrCriseFracaTextView = (TextView) findViewById(R.id.nrCriseFracaTextView);
        nrCriseMediaTextView = (TextView) findViewById(R.id.nrCriseMediaTextView);
        nrCriseForteTextView = (TextView) findViewById(R.id.nrCriseForteTextView);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        /*if (Build.VERSION.SDK_INT >= 19) {
            mChartWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            mChartWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }*/

        animation = AnimationUtils.loadAnimation(IntensidadeDorActivity.this, R.anim.slide_up);

        mToolbar.setTitle(mResource.getString(R.string.title_activity_intensidade_dor));
        setSupportActionBar(mToolbar);

        chartsJavaScriptInterface = new ChartsJavaScriptInterface(IntensidadeDorActivity.this);

        mChartWebView.addJavascriptInterface(chartsJavaScriptInterface, "Android");
        mChartWebView.setWebChromeClient(new WebChromeClient());

        WebSettings webSettings = mChartWebView.getSettings();
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setBuiltInZoomControls(false);

        SharedPreferences mPreferences = getSharedPreferences(USUARIO_PREFS, MODE_PRIVATE);
        mUserId= mPreferences.getString(USUARIO_PREFS_USUARIO_ID, null);

        mCalendar = Calendar.getInstance();
        int year = mCalendar.get(Calendar.YEAR);
        int month = mCalendar.get(Calendar.MONTH);

        mCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        mCalendar.set(Calendar.MONTH, month);
        mCalendar.set(Calendar.YEAR, year);

        mCalendarInicio = (Calendar) mCalendar.clone();
        atualizarData(mInicioDataCriseTextView);

        mCalendar.set(Calendar.DAY_OF_MONTH, mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        mCalendarFim = (Calendar) mCalendar.clone();
        atualizarData(mFimDataCriseTextView);
    }

    public void onClickSelecionarData(View v) {
        animation = AnimationUtils.loadAnimation(IntensidadeDorActivity.this, R.anim.bounce_up);
        switch (v.getId()){
            case R.id.inicioDataCriseTextView:
                callAndUpdateDialogDatePicker(mInicioDataCriseTextView, mCalendarInicio);
                break;
            case R.id.fimDataCriseTextView:
                callAndUpdateDialogDatePicker(mFimDataCriseTextView, mCalendarFim);
                break;
        }
    }

    private void callAndUpdateDialogDatePicker(final TextView textView, Calendar calendar)
    {

        int mHour=0,mMinute=0;

        if(calendar == null) {
            mCalendar   =   Calendar.getInstance();
        } else {
            mCalendar = calendar;
            mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
            mMinute = mCalendar.get(Calendar.MINUTE);
        }

        setPickerYear(mCalendar.get(Calendar.YEAR));
        setPickerMonth(mCalendar.get(Calendar.MONTH));
        setPickerDay(mCalendar.get(Calendar.DAY_OF_MONTH));
        setPickerHour(mHour);
        setPickerMinute(mMinute);

        DatePickerDialog mDatePicker = new DatePickerDialog(IntensidadeDorActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, monthOfYear);
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        calendar.set(Calendar.HOUR_OF_DAY,getPickerHour());
                        calendar.set(Calendar.MINUTE,getPickerMinute());
                        calendar.set(Calendar.SECOND,0);
                        calendar.set(Calendar.MILLISECOND,0);

                        mCalendar = calendar;
                        atualizarData(textView);
                    }
                }, getPickerYear(), getPickerMonth(), getPickerDay());
        mDatePicker.show();
    }

    private void atualizarData(TextView textView) {
        if( textView.getId() == R.id.inicioDataCriseTextView){
            mCalendarInicio = (Calendar) mCalendar.clone();
            mCalendarInicio.set(Calendar.HOUR_OF_DAY, 0);
            mCalendarInicio.set(Calendar.MINUTE, 0);
        }

        else if(textView.getId() == R.id.fimDataCriseTextView){
            mCalendarFim = (Calendar) mCalendar.clone();
            mCalendarFim.set(Calendar.HOUR_OF_DAY, 23);
            mCalendarFim.set(Calendar.MINUTE, 59);
        }

        if(mCalendarInicio != null && mCalendarFim != null)
        {
            long dataIni = mCalendarInicio.getTimeInMillis();
            long dataFim = mCalendarFim.getTimeInMillis();

            long nrCriseFraca = realm.where(Crise.class)
                                        .between(CriseEntidade.INTENSIDADE, 1, 3)
                                        .greaterThanOrEqualTo(CriseEntidade.DATAINI, dataIni)
                                        .lessThanOrEqualTo(CriseEntidade.DATAFIM, dataFim)
                                        .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, mUserId)
                                        .count();

            long nrCriseMedia = realm.where(Crise.class)
                    .between(CriseEntidade.INTENSIDADE, 4, 7)
                    .greaterThanOrEqualTo(CriseEntidade.DATAINI, dataIni)
                    .lessThanOrEqualTo(CriseEntidade.DATAFIM, dataFim)
                    .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, mUserId)
                    .count();

            long nrCriseForte = realm.where(Crise.class)
                    .between(CriseEntidade.INTENSIDADE, 8, 10)
                    .greaterThanOrEqualTo(CriseEntidade.DATAINI, dataIni)
                    .lessThanOrEqualTo(CriseEntidade.DATAFIM, dataFim)
                    .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, mUserId)
                    .count();

            long totalCrise = nrCriseFraca+nrCriseMedia+nrCriseForte;

            String stringNrCriseFraca = String.valueOf(nrCriseFraca);
            String stringNrCriseMedia = String.valueOf(nrCriseMedia);
            String stringNrCriseForte = String.valueOf(nrCriseForte);

            String[] dataIntensidade = new String[]{stringNrCriseFraca, stringNrCriseMedia, stringNrCriseForte};

            nrCriseFracaTextView.setText(stringNrCriseFraca);
            nrCriseMediaTextView.setText(stringNrCriseMedia);
            nrCriseForteTextView.setText(stringNrCriseForte);
            nrTotalCriseTextView.setText(String.valueOf(totalCrise));
            chartsJavaScriptInterface.setIntensidadeData(dataIntensidade);
            mChartWebView.reload();
        }

        SimpleDateFormat dateFormatDate = new SimpleDateFormat("dd/MM/yyyy");
        textView.setText(dateFormatDate.format(mCalendar.getTime()));
        textView.startAnimation(animation);
    }

    public int getPickerYear() {
        return mYear;
    }

    public void setPickerYear(int mYear) {
        this.mYear = mYear;
    }

    public int getPickerMonth() {
        return mMonth;
    }

    public void setPickerMonth(int mMonth) {
        this.mMonth = mMonth;
    }

    public int getPickerDay() {
        return mDay;
    }

    public void setPickerDay(int mDay) {
        this.mDay = mDay;
    }

    public int getPickerHour() {
        return mHour;
    }

    public void setPickerHour(int mHour) {
        this.mHour = mHour;
    }

    public int getPickerMinute() {
        return mMinute;
    }

    public void setPickerMinute(int mMinute) {
        this.mMinute = mMinute;
    }
}
