package healsmart.com.conexaoenxaqueca.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.adapters.ListaCriseAdapter;
import healsmart.com.conexaoenxaqueca.adapters.RelatorioCriseMedicamentoExpandableAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.MedicamentoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmResults;

public class ListaCriseMedicamentoActivity extends AppCompatActivity implements PerfilUsuario {

    private ListView mListaCriseListView;
    private ListaCriseAdapter mAdapter;
    private Toolbar mToolbar;

    private Medicamento mMedicamento;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_crise_medicamento);

        ServiceFactory.iniciar();

        Bundle extras = getIntent().getExtras();
        if(extras != null){

            mMedicamento = Medicamento.fromJson( extras.getString(RelatorioCriseMedicamentoExpandableAdapter.class.toString()) );
        }

        if(mMedicamento != null) {
            realm = Realm.getDefaultInstance();
            initViews();

        } else {
            ServiceFactory.getInstance().getServiceUtils().redirectTo(this, SinteseCriseMedicamentoActivity.class);
        }
    }

    @Override
    public void onStop() {
        finish();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if(mMedicamento != null)
            realm.close();

        super.onDestroy();
    }

    private void initViews()
    {
        mListaCriseListView = (ListView) findViewById(R.id.listaCriseListView);
        mToolbar = (Toolbar) findViewById(R.id.tb_main);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents() {
        String tituloToolbar = "Crises com "+ServiceFactory.getInstance().getServiceUtils().textToCapitalize(mMedicamento.getNome());
        mToolbar.setTitle(tituloToolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_activity);

        mAdapter = new ListaCriseAdapter(this, getListContent(), ListaCriseMedicamentoActivity.class);
        mListaCriseListView.setAdapter(mAdapter);
    }

    private List<Crise> getListContent()
    {
        realm.refresh();

        SharedPreferences mPreferences = getSharedPreferences(USUARIO_PREFS, MODE_PRIVATE);
        String userId = mPreferences.getString(USUARIO_PREFS_USUARIO_ID, null);

        Crise c = realm.where(Crise.class)
                .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, userId)
                .equalTo(CriseEntidade.MEDICAMENTOS+"."+ MedicamentoEntidade.ID, mMedicamento.getId())
                .findFirst();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(c.getDataIni());
        int ano = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);

        Calendar calendarIni = Calendar.getInstance();
        calendarIni.set(Calendar.YEAR, ano);
        calendarIni.set(Calendar.MONTH, mes);
        calendarIni.set(Calendar.DAY_OF_MONTH, calendarIni.getActualMinimum(Calendar.DAY_OF_MONTH));
        calendarIni.set(Calendar.HOUR_OF_DAY, 0);
        calendarIni.set(Calendar.MINUTE, 0);
        long dataIni = calendarIni.getTimeInMillis();

        Calendar calendarEnd = Calendar.getInstance();
        calendarEnd.set(Calendar.YEAR, ano);
        calendarEnd.set(Calendar.MONTH,mes);
        calendarEnd.set(Calendar.DAY_OF_MONTH, calendarEnd.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
        calendarEnd.set(Calendar.MINUTE, 59);
        long dataFim = calendarEnd.getTimeInMillis();

        RealmResults<Crise> crises =   realm.where(Crise.class)
                .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, userId)
                .equalTo(CriseEntidade.MEDICAMENTOS+"."+ MedicamentoEntidade.NOME, mMedicamento.getNome())
                .greaterThanOrEqualTo(CriseEntidade.DATAINI, dataIni)
                .lessThanOrEqualTo(CriseEntidade.DATAFIM, dataFim)
                .findAll();

        List<Crise> criseList = new ArrayList<>();
        for(Crise crise : crises)
        {
            criseList.add(Crise.copy(crise));
        }

        return criseList;
    }
}
