package healsmart.com.conexaoenxaqueca.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.mikepenz.materialdrawer.Drawer;

import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SplashScreen;
import healsmart.com.conexaoenxaqueca.Utils.NavigationDrawer;
import healsmart.com.conexaoenxaqueca.adapters.DashboardCardsAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.DrawerMenu;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmResults;

public class MainActivity extends AppCompatActivity
        implements  PerfilUsuario,
                    DrawerMenu
{

    private SharedPreferences mUsuarioPreference;
    private SharedPreferences mUsuarioPreferenceTour;
    boolean mIsLogin;

    private Drawer navigationDrawerLeft;
    private Realm realm;

    private Toolbar mToolbar;
    private RecyclerView mCardsRecyclerView;
    private DashboardCardsAdapter mAdapter;
    private Map<Integer, Object> mContentCards;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ServiceFactory.iniciar();
        ServiceFactory.getInstance().getServiceUtils().hideStatusBar(this);

        setContentView(R.layout.activity_main);

        initView();

        navigationDrawerLeft = new NavigationDrawer(MainActivity.this, mToolbar, PAINEL_CONTROLE, savedInstanceState, R.id.drawer_container).run();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        realm = Realm.getDefaultInstance();
        mUsuarioPreference = getSharedPreferences(USUARIO_PREFS, MODE_PRIVATE);
        mIsLogin = mUsuarioPreference.getBoolean(USUARIO_PREFS_LOGIN, false);

        if(!mIsLogin)
            ServiceFactory.getInstance().getServiceUtils().redirectTo(MainActivity.this, SplashScreen.class);

        showDashboardCards();

        showTour();
    }

    @Override
    public void onStop() {
        super.onStop();

        mContentCards = new TreeMap<>();
        realm.close();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        navigationDrawerLeft.setSelectionAtPosition(PAINEL_CONTROLE, false);
        if(navigationDrawerLeft.isDrawerOpen())
            navigationDrawerLeft.closeDrawer();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //    return true;
        //}

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(navigationDrawerLeft.isDrawerOpen()){
            navigationDrawerLeft.closeDrawer();
        }
        else{
            super.onBackPressed();
        }
    }

    private void initView()
    {
        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mCardsRecyclerView = (RecyclerView) findViewById(R.id.cards_recycler_view);
        mContentCards = new TreeMap<>();

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mCardsRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        mCardsRecyclerView.setLayoutManager(llm);
    }

    public void onClickRegistrarCrise(View v) {
        ServiceFactory.getInstance().getServiceUtils().redirectTo(this, SurveyActivity.class);
    }

    public void onClickListaDeCrises(View v) {
        ServiceFactory.getInstance().getServiceUtils().redirectTo(this, ListaCriseActivity.class);
    }

    public void onClickRelatorioIntensidadeDor(View v) {
        ServiceFactory.getInstance().getServiceUtils().redirectTo(this, IntensidadeDorActivity.class);
    }

    public void onClickRelatorioCriseMedicamento(View v) {
        ServiceFactory.getInstance().getServiceUtils().redirectTo(this, SinteseCriseMedicamentoActivity.class);
    }

    private void showTour() {

        if(mIsLogin) {

            mUsuarioPreferenceTour = getSharedPreferences(USUARIO_PREFS_TOUR, MODE_PRIVATE);
            if(!mUsuarioPreferenceTour.getBoolean(USUARIO_PREFS_TOUR_REGISTRAR_CRISE, false)) {
                //showcaseview

                SharedPreferences.Editor editor = mUsuarioPreferenceTour.edit();
                editor.putBoolean(USUARIO_PREFS_TOUR_REGISTRAR_CRISE, true);
                editor.apply();
            } else if(!mUsuarioPreferenceTour.getBoolean(USUARIO_PREFS_TOUR_LISTA_CRISE, false)) {

                Realm realm = Realm.getDefaultInstance();
                long qtdCrise = realm.where(Crise.class).count();
                if(qtdCrise > 0 ) {

                    //showcaseview

                    SharedPreferences.Editor editor = mUsuarioPreferenceTour.edit();
                    editor.putBoolean(USUARIO_PREFS_TOUR_LISTA_CRISE, true);
                    editor.apply();
                }
                realm.close();
            }
        }
    }

    private void showDashboardCards() {

        if(mIsLogin) {

            RealmResults<Crise> crises = crisisInProgress();
            int sizeCrises = (crises == null ? 0 : crises.size());
            if(sizeCrises > 0) {
                Crise c = crises.last();
                mContentCards.put(0, c);
            }

            if(sizeCrises == 0)
                mContentCards.put(1, getDataCardTempoSemDor());

            mContentCards.put(2, getDataCardIntensidade());

            mAdapter = new DashboardCardsAdapter(this, mContentCards);
            mCardsRecyclerView.setAdapter(mAdapter);
        }
    }

    private RealmResults<Crise> crisisInProgress() {
        realm.refresh();

        return realm.where(Crise.class)
                .equalTo(CriseEntidade.DATAFIM, 0)
                .findAll();
    }

    private long getDataCardTempoSemDor() {
        realm.refresh();

        long retorno = 0;

        RealmResults<Crise> crises = realm.where(Crise.class).findAll();
        int sizeCrises = (crises == null ? 0 : crises.size());

        if(sizeCrises > 0) {
            Crise c = crises.last();
            retorno = c.getDataIni();
        }

        return retorno;
    }

    private long[] getDataCardIntensidade() {

        long[] retorno = new long[0];

        Calendar calendarIni = Calendar.getInstance();
        int year = calendarIni.get(Calendar.YEAR);
        int month = calendarIni.get(Calendar.MONTH);

        calendarIni.set(Calendar.DAY_OF_MONTH, calendarIni.getActualMinimum(Calendar.DAY_OF_MONTH));
        calendarIni.set(Calendar.MONTH, month);
        calendarIni.set(Calendar.YEAR, year);
        calendarIni.set(Calendar.HOUR_OF_DAY, 0);
        calendarIni.set(Calendar.MINUTE, 0);

        Calendar calendarFim = Calendar.getInstance();
        calendarFim.set(Calendar.DAY_OF_MONTH, calendarFim.getActualMaximum(Calendar.DAY_OF_MONTH));
        calendarFim.set(Calendar.MONTH, month);
        calendarFim.set(Calendar.YEAR, year);
        calendarFim.set(Calendar.HOUR_OF_DAY, 23);
        calendarFim.set(Calendar.MINUTE, 59);

        long dataIni = calendarIni.getTimeInMillis();
        long dataFim = calendarFim.getTimeInMillis();

        String userId = mUsuarioPreference.getString(USUARIO_PREFS_USUARIO_ID, null);
        if(userId != null) {

            long nrCriseFraca = realm.where(Crise.class)
                    .between(CriseEntidade.INTENSIDADE, 1, 3)
                    .greaterThanOrEqualTo(CriseEntidade.DATAINI, dataIni)
                    .lessThanOrEqualTo(CriseEntidade.DATAFIM, dataFim)
                    .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, userId)
                    .count();

            long nrCriseMedia = realm.where(Crise.class)
                    .between(CriseEntidade.INTENSIDADE, 4, 7)
                    .greaterThanOrEqualTo(CriseEntidade.DATAINI, dataIni)
                    .lessThanOrEqualTo(CriseEntidade.DATAFIM, dataFim)
                    .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, userId)
                    .count();

            long nrCriseForte = realm.where(Crise.class)
                    .between(CriseEntidade.INTENSIDADE, 8, 10)
                    .greaterThanOrEqualTo(CriseEntidade.DATAINI, dataIni)
                    .lessThanOrEqualTo(CriseEntidade.DATAFIM, dataFim)
                    .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, userId)
                    .count();

            long totalCrise = nrCriseFraca+nrCriseMedia+nrCriseForte;

            retorno = new long[]{nrCriseFraca, nrCriseMedia, nrCriseForte, totalCrise};
        }

        return retorno;
    }
}