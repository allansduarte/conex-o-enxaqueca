package healsmart.com.conexaoenxaqueca.activities;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.adapters.RelatorioCriseMedicamentoExpandableAdapter;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmResults;

public class SinteseCriseMedicamentoActivity extends AppCompatActivity
    implements PerfilUsuario
{
    private final int MAX_YEAR = Calendar.getInstance().get(Calendar.YEAR);
    private final int MIN_YEAR = 1899;

    private int mAno;
    private TextView mAnoIndicadorTextView;
    private ExpandableListView mCriseMedicamentoExpandableListView;

    private Drawer navigationDrawerLeft;
    private Toolbar mToolbar;
    private Resources mResources;
    private Animation mAnimation;

    private Realm realm;
    private boolean isContentLoaded=false;
    private RelatorioCriseMedicamentoExpandableAdapter mAdapter;

    private Map<Integer, List<Medicamento>> mDataChildList;
    private Map<Integer, List<Integer>> mDataParentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ServiceFactory.iniciar();
        ServiceFactory.getInstance().getServiceUtils().hideStatusBar(this);

        setContentView(R.layout.activity_sintese_crise_medicamento);
        realm = Realm.getDefaultInstance();

        initView();
        navigationDrawerLeft = ServiceFactory.getInstance().getServiceNavigationDrawer(this, mToolbar, R.id.drawer_container).run();
        isContentLoaded=true;
    }

    @Override
    public void onStop() {
        isContentLoaded=false;

        super.onStop();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(navigationDrawerLeft.isDrawerOpen())
            navigationDrawerLeft.closeDrawer();

        if(!isContentLoaded) {
            buildData();
            mAdapter.setContentData(mDataParentList, mDataChildList);
            isContentLoaded=true;
        }
    }

    @Override
    public void onBackPressed() {
        if(navigationDrawerLeft.isDrawerOpen()){
            navigationDrawerLeft.closeDrawer();
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();

        super.onDestroy();
    }

    private void initView()
    {
        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mAnoIndicadorTextView = (TextView) findViewById(R.id.anoIndicadorTextView);
        mCriseMedicamentoExpandableListView = (ExpandableListView) findViewById(R.id.criseMedicamentoExpandableListView);
        mResources = getResources();

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mToolbar.setTitle(mResources.getString(R.string.title_activity_sintese_crise_medicamento));
        setSupportActionBar(mToolbar);

        mAno = MAX_YEAR;
        mAnoIndicadorTextView.setText(String.valueOf(MAX_YEAR));

        buildData();
        mAdapter = new RelatorioCriseMedicamentoExpandableAdapter(this, mDataParentList, mDataChildList);
        mCriseMedicamentoExpandableListView.setAdapter(mAdapter);
        mCriseMedicamentoExpandableListView.setOnGroupExpandListener(onGroupExpandedListener);
        mCriseMedicamentoExpandableListView.setOnGroupCollapseListener(onGroupCollapseListener);
    }

    ExpandableListView.OnGroupExpandListener onGroupExpandedListener = new ExpandableListView.OnGroupExpandListener() {
        @Override
        public void onGroupExpand(int groupPosition) {

            List<Medicamento> childContent = mDataChildList.get(groupPosition);
            int sizeChildContent = (childContent == null ? 0 : childContent.size());
            if(sizeChildContent > 0) {

                for(int i=0; i <mCriseMedicamentoExpandableListView.getChildCount(); i++) {

                    LinearLayout ll = (LinearLayout) mCriseMedicamentoExpandableListView.getChildAt(i);
                    if(ll.getChildAt(0) instanceof LinearLayout) {

                        TextView expandableTextView = (TextView) ((LinearLayout) ll.getChildAt(0)).getChildAt(0);
                        String mesExpandableView = expandableTextView.getText().toString();
                        String mesGroupPosition = ServiceFactory.getInstance().getServiceUtils().getNameMonth(groupPosition, mResources);

                        if(mesExpandableView.equals(mesGroupPosition)) {
                            expandableTextView.setSelected(true);
                            break;
                        }
                    }
                }
            }
        }
    };

    ExpandableListView.OnGroupCollapseListener onGroupCollapseListener = new ExpandableListView.OnGroupCollapseListener() {
        @Override
        public void onGroupCollapse(int groupPosition) {

            List<Medicamento> childContent = mDataChildList.get(groupPosition);
            int sizeChildContent = (childContent == null ? 0 : childContent.size());
            if(sizeChildContent > 0) {

                for(int i=0; i <mCriseMedicamentoExpandableListView.getChildCount(); i++) {

                    LinearLayout ll = (LinearLayout) mCriseMedicamentoExpandableListView.getChildAt(i);
                    if(ll.getChildAt(0) instanceof LinearLayout) {

                        TextView expandableTextView = (TextView) ((LinearLayout) ll.getChildAt(0)).getChildAt(0);
                        String mesExpandableView = expandableTextView.getText().toString();
                        String mesGroupPosition = ServiceFactory.getInstance().getServiceUtils().getNameMonth(groupPosition, mResources);

                        if(mesExpandableView.equals(mesGroupPosition)) {
                            expandableTextView.setSelected(false);
                            break;
                        }
                    }
                }
            }
        }
    };

    public void onClickChangeYear(View v)
    {
        boolean changeBehavior = false;
        mAno = Integer.parseInt(mAnoIndicadorTextView.getText().toString());

        switch (v.getId())
        {
            case R.id.beforeYearImageView:
                if(mAno > MIN_YEAR){
                    mAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
                    mAno -= 1;
                    changeBehavior = true;

                    ((LinearLayout) v.getParent()).getChildAt(2).setSelected(false);
                } else v.setSelected(true);
                break;
            case R.id.nextYearImageView:
                if(mAno < MAX_YEAR){
                    mAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
                    mAno += 1;
                    changeBehavior = true;
                    ((LinearLayout) v.getParent()).getChildAt(0).setSelected(false);
                } else v.setSelected(true);
                break;
        }

        if(changeBehavior)
        {
            changeYear();
        }
    }

    private void changeYear()
    {
        mAnoIndicadorTextView.setText(String.valueOf(mAno));
        mAnoIndicadorTextView.startAnimation(mAnimation);

        buildData();
        mAdapter.setContentData(mDataParentList, mDataChildList);
    }

    private void buildData()
    {
        realm.refresh();

        SharedPreferences mPreferences = getSharedPreferences(USUARIO_PREFS, MODE_PRIVATE);
        String userId = mPreferences.getString(USUARIO_PREFS_USUARIO_ID, null);

        Map<Integer, List<Medicamento>> dataChildList = new TreeMap<>();
        Map<Integer, List<Integer>> dataParentList = new TreeMap<>();
        for(int i=0; i < 12; i++)
        {
            dataParentList.put(i, new ArrayList<Integer>());
            dataChildList.put(i, new ArrayList<Medicamento>());
        }

        if(userId != null)
        {
            Calendar calendarIni = Calendar.getInstance();
            calendarIni.set(Calendar.YEAR, mAno);
            calendarIni.set(Calendar.MONTH, 0);
            calendarIni.set(Calendar.DAY_OF_MONTH, calendarIni.getActualMinimum(Calendar.DAY_OF_MONTH));
            calendarIni.set(Calendar.HOUR_OF_DAY, 0);
            calendarIni.set(Calendar.MINUTE, 0);
            long dataIni = calendarIni.getTimeInMillis();

            Calendar calendarEnd = Calendar.getInstance();
            calendarEnd.set(Calendar.YEAR, mAno);
            calendarEnd.set(Calendar.MONTH,11);
            calendarEnd.set(Calendar.DAY_OF_MONTH, calendarEnd.getActualMaximum(Calendar.DAY_OF_MONTH));
            calendarEnd.set(Calendar.HOUR_OF_DAY, 23);
            calendarEnd.set(Calendar.MINUTE, 59);
            long dataFim = calendarEnd.getTimeInMillis();

            RealmResults<Crise> crises = realm.where(Crise.class)
                    .greaterThanOrEqualTo(CriseEntidade.DATAINI, dataIni)
                    .lessThanOrEqualTo(CriseEntidade.DATAFIM, dataFim)
                    .greaterThan(CriseEntidade.DATAFIM, 0)
                    .equalTo(CriseEntidade.USUARIO + "." + UsuarioEntidade.ID, userId)
                    .findAll();

            for(Crise crise : crises)
            {
                long criseDataIni = crise.getDataIni();
                Calendar criseCalendar = Calendar.getInstance();
                criseCalendar.setTimeInMillis(criseDataIni);

                int key = criseCalendar.get(Calendar.MONTH);
                if(dataParentList.containsKey(key))
                {
                    Map.Entry entryChildData = getEntry(key, dataChildList);
                    Object listEntryChildValue = entryChildData.getValue();
                    List<Medicamento> listaMedicamentosDoMes = null;

                    Map.Entry entryParentData = ServiceFactory.getInstance().getServiceUtils().getEntry(key, dataParentList);
                    Object listEntryParentValue = entryParentData.getValue();
                    List<Integer> contentParentList = null;
                    int nrCrise = 1, nrCriseComMedicamento = 0;

                    int sizeMedicamentos = (crise.getMedicamentos() == null ? 0 : crise.getMedicamentos().size());
                    if(sizeMedicamentos > 0) {
                        nrCriseComMedicamento = 1;

                        if(listEntryChildValue != null)
                            listaMedicamentosDoMes = Utils.castList(listEntryChildValue, Medicamento.class);

                        int sizeMedicamentosDoMes = (listaMedicamentosDoMes == null ? 0 : listaMedicamentosDoMes.size());
                        if(sizeMedicamentosDoMes > 0)
                        {
                            for(int i=0; i < crise.getMedicamentos().size(); i++)
                            {
                                Medicamento medicamento = crise.getMedicamentos().get(i);

                                int indexMedicamentoDoMes = compararListaMedicamento(medicamento.getNome(), listaMedicamentosDoMes);
                                if(indexMedicamentoDoMes >= 0) {
                                    Medicamento medicamentoAtual = listaMedicamentosDoMes.get(indexMedicamentoDoMes);
                                    int quantidade = medicamentoAtual.getQuantidade() + medicamento.getQuantidade();
                                    medicamentoAtual.setQuantidade(quantidade);
                                    listaMedicamentosDoMes.set(indexMedicamentoDoMes, medicamentoAtual);
                                } else {
                                    Medicamento novoMedicamento = Medicamento.copy(medicamento);
                                    listaMedicamentosDoMes.add(novoMedicamento);
                                }
                            }
                        } else {
                            listaMedicamentosDoMes = new ArrayList<>();

                            for(Medicamento medicamento : crise.getMedicamentos())
                            {
                                listaMedicamentosDoMes.add(Medicamento.copy(medicamento));
                            }
                        }
                        dataChildList.put(key, listaMedicamentosDoMes);
                    }

                    if(listEntryParentValue != null)
                        contentParentList = Utils.castList(listEntryParentValue, Integer.class);

                    int sizeParentContentList = (contentParentList == null ? 0 : contentParentList.size());
                    if(sizeParentContentList > 0)
                    {
                        nrCrise += contentParentList.get(0);
                        nrCriseComMedicamento += contentParentList.get(1);
                    } else {
                        contentParentList = new ArrayList<>();
                    }

                    contentParentList.add(0, nrCrise);
                    contentParentList.add(1, nrCriseComMedicamento);

                    dataParentList.put(key, contentParentList);
                }
            }
        }

        mDataParentList = dataParentList;
        mDataChildList = dataChildList;
    }

    private int compararListaMedicamento(String nome, List<Medicamento> listaMedicamentos)
    {
        int index = -1;

        for(int x=0; x < listaMedicamentos.size(); x++) {
            if(listaMedicamentos.get(x).getNome().equals(nome)) {
                index = x;
                break;
            }
        }

        return index;
    }

    private Map.Entry getEntry(int id, Map<Integer, List<Medicamento>> listMap)
    {
        Iterator iterator = listMap.entrySet().iterator();
        int n = 0;
        while(iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            if(n == id){
                return entry;
            }
            n ++;
        }

        return null;
    }
}
