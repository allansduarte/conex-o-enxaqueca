package healsmart.com.conexaoenxaqueca.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mikepenz.materialdrawer.Drawer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.Utils.NavigationDrawer;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.DrawerMenu;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;

public class PerfilActivity extends AppCompatActivity implements PerfilUsuario, DrawerMenu
{
    private Toolbar mToolbar;

    private EditText mEmailEditText;
    private EditText mSenhaEditText;
    private EditText mNomeCompletoEdiText;
    private TextView mDataNascimentoTextView;
    private TextView mSexoTextView;
    private TextView mMsgInfoTextView;

    private Calendar mCalendar;
    private Resources mResources;

    private Drawer navigationDrawerLeft;

    private int mDataSexo;
    private String mUserId;
    private Usuario mUsuarioPerfil;
    protected Realm realm;

    private boolean isContentLoaded=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        ServiceFactory.iniciar();
        initViews();

        navigationDrawerLeft = new NavigationDrawer(this, mToolbar, PERFIL_USUARIO, savedInstanceState, R.id.drawer_container).run();
        SharedPreferences mPreferences = getSharedPreferences(USUARIO_PREFS, MODE_PRIVATE);
        mUserId= mPreferences.getString(USUARIO_PREFS_USUARIO_ID, null);

        realm = Realm.getDefaultInstance();

        mUsuarioPerfil = getPerfilUsuario();
        notifyView();
        isContentLoaded=true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_perfil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(navigationDrawerLeft.isDrawerOpen()){
            navigationDrawerLeft.closeDrawer();
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public void onStop() {
        isContentLoaded=false;

        super.onStop();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(!isContentLoaded) {
            mUsuarioPerfil = getPerfilUsuario();
            notifyView();
        }

        navigationDrawerLeft.setSelectionAtPosition(PERFIL_USUARIO, false);
        if(navigationDrawerLeft.isDrawerOpen())
            navigationDrawerLeft.closeDrawer();
    }

    @Override
    protected void onDestroy() {
        realm.close();

        super.onDestroy();
    }

    private void initViews()
    {
        mResources = getResources();
        mToolbar = (Toolbar) findViewById(R.id.tb_main);
        mDataNascimentoTextView = (TextView) findViewById(R.id.dataNascimentoTextView);
        mSexoTextView = (TextView) findViewById(R.id.sexoTextView);
        mEmailEditText = (EditText) findViewById(R.id.emailEditText);
        mSenhaEditText = (EditText) findViewById(R.id.senhaEditText);
        mNomeCompletoEdiText = (EditText) findViewById(R.id.nomeCompletoEditText);
        mMsgInfoTextView = (TextView) findViewById(R.id.msgInfoTextView);

        initBehaviorAndEvents();
    }

    private void initBehaviorAndEvents()
    {
        mToolbar.setTitle(mResources.getString(R.string.title_activity_perfil));
        mCalendar   =   Calendar.getInstance();
    }

    public void escolherSexo(View v) {

        ArrayList<String> itens = new ArrayList<String>();
        itens.add("Feminino");
        itens.add("Masculino");

        Context context = v.getContext();

        ArrayAdapter adapter = new ArrayAdapter(context, R.layout.item_dialog, itens);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Sexo").setSingleChoiceItems(adapter, 0, dialogSexoTextViewOnClick);

        AlertDialog alerta = builder.create();
        alerta.show();
    }

    DialogInterface.OnClickListener dialogSexoTextViewOnClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            String valorOpcao = (which == 0) ? "Feminino" : "Masculino";
            mSexoTextView.setText(valorOpcao);
            mDataSexo = which;

            if(!mSexoTextView.getText().toString().equalsIgnoreCase("Sexo"))
            {
                mSexoTextView.setError(null);
            }

            dialog.dismiss();
        }
    };

    public void selecionarDataNascimento(View v) {
        switch (v.getId()){
            case R.id.dataNascimentoTextView:

                int mYear=2000,mMonth=0,mDay =1;

                if(mCalendar.getTimeInMillis() == 0)
                    mCalendar   =   Calendar.getInstance();

                mYear       =   mCalendar.get(Calendar.YEAR);
                mMonth      =   mCalendar.get(Calendar.MONTH);
                mDay        =   mCalendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(PerfilActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(Calendar.YEAR, year);
                                calendar.set(Calendar.MONTH, monthOfYear);
                                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                                calendar.set(Calendar.HOUR_OF_DAY,0);
                                calendar.set(Calendar.MINUTE,0);
                                calendar.set(Calendar.SECOND,0);
                                calendar.set(Calendar.MILLISECOND,0);

                                mCalendar = calendar;
                                atualizarData();
                            }
                        }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Data de Nascimento");
                mDatePicker.show();

                break;
        }
    }

    private void atualizarData() {
        SimpleDateFormat dateFormatDate = new SimpleDateFormat("dd/MM/yyyy");
        mDataNascimentoTextView.setText(dateFormatDate.format(mCalendar.getTime()));

        if( !mDataNascimentoTextView.getText().toString().equalsIgnoreCase("Data de nascimento") )
        {
            mDataNascimentoTextView.setError(null);
        }
    }

    public void atualizarPerfil(View v) {

        if (v.getId() == R.id.atualizarPerfilButton) {
            if (validateFields()) {
                Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, mUserId).findFirst();

                try {
                    realm.beginTransaction();
                    String genero = (mDataSexo == 0 ? "female" : "male");

                    if(mNomeCompletoEdiText.getText().toString().length() > 0)
                    {
                        String[] splitNome = ServiceFactory.getInstance().getServiceUtils().splitCompleteName(mNomeCompletoEdiText.getText().toString());
                        usuario.setNome(splitNome[0]);
                        usuario.setSobrenome(splitNome[1]);
                    }

                    if(!mDataNascimentoTextView.getText().toString().equalsIgnoreCase("Data de nascimento"))
                        usuario.setDataNascimento(mCalendar.getTimeInMillis());

                    usuario.setSenha(mSenhaEditText.getText().toString());
                    usuario.setGenero(genero);

                    realm.copyToRealmOrUpdate(usuario);
                    realm.commitTransaction();

                    mMsgInfoTextView.setVisibility(View.GONE);
                    Toast.makeText(PerfilActivity.this, "Perfil atualizado com sucesso!", Toast.LENGTH_LONG).show();
                } catch (Exception e)
                {
                    realm.cancelTransaction();
                }
            }
        }
    }

    private boolean validateFields() {
        String sexo = mSexoTextView.getText().toString().trim();

        return ( !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(mEmailEditText, mResources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().hasSizeValid(mEmailEditText, 5, "Seu e-mail deve conter no mínimo 5 caracteres.")
                && ServiceFactory.getInstance().getServiceValidate().isValidEmail(mEmailEditText)
                && !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(mSenhaEditText, mResources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().hasSizeValid(mSenhaEditText, 4, "Sua senha deve conter no mínimo 4 caracteres.")
                && !isEmptyFields(sexo));
    }

    private boolean isEmptyFields(String sexo) {
        if(sexo.equalsIgnoreCase("Sexo")){
            mSexoTextView.requestFocus();
            mSexoTextView.setError(mResources.getString(R.string.validacao_campo_obrigatorio));
            return true;
        }
        return false;
    }

    private Usuario getPerfilUsuario()
    {
        return realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, mUserId).findFirst();
    }

    private void notifyView()
    {
        if(mUsuarioPerfil != null)
        {
            String email = mUsuarioPerfil.getEmail();
            String senha = mUsuarioPerfil.getSenha();
            String nome = mUsuarioPerfil.getNome();
            String sobreNome = mUsuarioPerfil.getSobrenome();
            long dataNascMilis = mUsuarioPerfil.getDataNascimento();
            String sexo = mUsuarioPerfil.getGenero();

            mEmailEditText.setText(email);

            if(senha != null)
            {
                mSenhaEditText.setText(senha);
                mMsgInfoTextView.setVisibility(View.GONE);
            } else mMsgInfoTextView.setVisibility(View.VISIBLE);

            if(nome != null)
                mNomeCompletoEdiText.setText(nome+" "+sobreNome);
            if(dataNascMilis != 0) {
                SimpleDateFormat dateFormatDate = new SimpleDateFormat("dd/MM/yyyy");
                mDataNascimentoTextView.setText(dateFormatDate.format(dataNascMilis));
                mCalendar.setTimeInMillis(dataNascMilis);
            }
            if(sexo != null) {
                String genero = (mUsuarioPerfil.getGenero().equals("male") ? "Masculino" : "Feminino");
                mSexoTextView.setText(genero);
            }
        }
    }
}
