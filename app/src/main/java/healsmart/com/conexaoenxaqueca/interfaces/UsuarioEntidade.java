package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 17/11/15.
 */
public interface UsuarioEntidade
{
    String ID = "id";
    String EMAIL = "email";
    String SENHA = "senha";
    String NOME = "nome";
    String SOBRENOME = "sobrenome";
    String DATANASC = "dataNascimento";
    String GENERO = "genero";
}
