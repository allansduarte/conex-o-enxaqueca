package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 25/11/15.
 */
public interface ComidaEntidade
{
    String ID = "id";
    String NOME = "nome";
    String DISPLAY = "display";
    String USUARIO = "usuario";
}
