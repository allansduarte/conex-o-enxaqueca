package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 18/11/15.
 */
public interface QualidadeDorEntidade
{
    String ID = "id";
    String NOME = "nome";
    String USUARIO = "usuario";
    String DISPLAY = "display";
}
