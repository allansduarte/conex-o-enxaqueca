package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 17/11/15.
 */
public interface SintomaEntidade
{
    String ID = "id";
    String NOME = "nome";
    String USUARIO = "usuario";
    String DISPLAY = "display";
}