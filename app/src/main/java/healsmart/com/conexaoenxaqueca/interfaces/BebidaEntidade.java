package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 25/11/15.
 */
public interface BebidaEntidade
{
    String ID = "id";
    String NOME = "nome";
    String USUARIO = "usuario";
    String DISPLAY = "display";
}
