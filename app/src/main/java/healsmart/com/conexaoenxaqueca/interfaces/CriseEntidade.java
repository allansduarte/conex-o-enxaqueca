package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 17/11/15.
 */
public interface CriseEntidade
{
    String ID = "id";
    String USUARIO = "usuario";
    String MEDICAMENTOS = "medicamentos";
    String QUALIDADE_DOR = "qualidadeDors";
    String GATILHOS_CRISE = "gatilhosCrise";
    String LOCAL = "locais";
    String SINTOMAS = "sintomas";
    String METODO_ALIVIO = "metodos";
    String CONSEQUENCIAS = "consequencias";
    String INTENSIDADE = "intensidade";
    String DATAINI = "dataIni";
    String DATAFIM = "dataFim";
    String NR_CRISE = "nrCrise";
    String CREATED = "created";
}
