package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 20/11/15.
 */
public interface ConsequenciaEntidade
{
    String ID = "id";
    String USUARIO = "usuario";
    String NOME = "nome";
    String DISPLAY = "display";
}
