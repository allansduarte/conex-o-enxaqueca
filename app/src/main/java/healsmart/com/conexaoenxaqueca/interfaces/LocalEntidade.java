package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 21/11/15.
 */
public interface LocalEntidade
{
    String ID = "id";
    String USUARIO = "usuario";
    String NOME = "nome";
    String DISPLAY = "display";
}
