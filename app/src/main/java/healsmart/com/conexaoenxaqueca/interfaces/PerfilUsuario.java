package healsmart.com.conexaoenxaqueca.interfaces;

public interface PerfilUsuario
{
    String USUARIO_PREFS = "usuario_preferences";
    String USUARIO_PREFS_USUARIO_ID = "usuario_id";
    String USUARIO_PREFS_LOGIN = "login";

    String USUARIO_PREFS_TOUR = "usuario_preferences_tour";
    String USUARIO_PREFS_TOUR_REGISTRAR_CRISE = "usuario_preferences_tour_registrar_crise";
    String USUARIO_PREFS_TOUR_LISTA_CRISE = "usuario_preferences_tour_lista_crise";

    String QUALIDADE_DOR_OPTIONS_ADDED_TAG  = "qa_dor_added";
    String SINTOMAS_OPTIONS_ADDED_TAG       = "sintomas_added";
    String METODO_ALIVIO_OPTIONS_ADDED_TAG  = "metodo_alivio_added";
    String CONSEQUENCIA_OPTIONS_ADDED_TAG  = "consequencia_alivio_added";
    String LOCAL_OPTIONS_ADDED_TAG  = "local_alivio_added";
    String GATILHO_OPTIONS_ADDED_TAG  = "gatilho_alivio_added";
    String GATILHO_BEBIDA_OPTIONS_ADDED_TAG  = "gatilho_bebida_alivio_added";
    String GATILHO_COMIDA_OPTIONS_ADDED_TAG  = "gatilho_comida_alivio_added";
    String GATILHO_ESFORCO_FISICO_OPTIONS_ADDED_TAG  = "gatilho_esforco_fisico_alivio_added";
    String LOCAIS_DOR_OPTIONS_ADDED_TAG  = "locais_dor_added";
}
