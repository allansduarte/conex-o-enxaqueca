package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 26/11/15.
 */
public interface GatilhoCriseEntidade
{
    String GATILHO = "gatilho";
    String BEBIDA = "bebidas";
    String COMIDA = "comidas";
    String ESFORCO_FISICO = "esforcoFisicos";
}
