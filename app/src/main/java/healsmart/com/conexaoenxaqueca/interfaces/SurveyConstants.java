package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 31/10/15.
 */
public interface SurveyConstants
{
    int TYPE_MEDICAMENTO = 0;
    int TYPE_METODO_ALIVIO = 1;

    int MODE_INSERT = 0;
    int MODE_DELETE = 1;

    String QUESTION_ACTIVE = "1";
    String QUESTION_INACTIVE = "0";
    String INTENT_CRISE_ID = "intent_crise_id";
    String INTENT_QUESTAO_NAVIGATION = "intent_questao_id";

    int QUESTION_DURACAO_DOR_ID = 1;
    int QUESTION_INTENSIDADE_ID = 2;
    int QUESTION_MEDICAMENTO_ID = 3;
    int QUESTION_LOCAIS_DOR_ID = 4;
    int QUESTION_QUALIDADE_DOR_ID = 5;
    int QUESTION_SINTOMAS_ID = 6;
    int QUESTION_GATILHOS_ID = 7;
    int QUESTION_GATILHOS_COMIDA_ID = 71;
    int QUESTION_GATILHOS_BEBIDA_ID = 72;
    int QUESTION_GATILHOS_ESFORCO_FISICO_ID = 73;
    int QUESTION_METODO_ALIVIO_ID = 8;
    int QUESTION_METODO_ALIVIO_FUNCIONOU_ID = 9;
    int QUESTION_CONSEQUENCIAS_ID = 10;
    int QUESTION_LOCAL_ID = 11;

    String NULL_OPTION_ID = "null_option_id";
}
