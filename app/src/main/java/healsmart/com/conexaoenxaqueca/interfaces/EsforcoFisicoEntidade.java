package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 25/11/15.
 */
public interface EsforcoFisicoEntidade
{
    String ID = "id";
    String USUARIO = "usuario";
    String NOME = "nome";
    String DISPLAY = "display";
}
