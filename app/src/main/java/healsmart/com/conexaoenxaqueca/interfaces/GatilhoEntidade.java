package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 25/11/15.
 */
public interface GatilhoEntidade
{
    String ID = "id";
    String USUARIO = "usuario";
    String DISPLAY = "display";
    String NOME = "nome";
}
