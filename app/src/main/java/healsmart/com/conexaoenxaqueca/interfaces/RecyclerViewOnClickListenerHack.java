package healsmart.com.conexaoenxaqueca.interfaces;

import android.view.View;

/**
 * Created by allan on 14/10/15.
 */
public interface RecyclerViewOnClickListenerHack
{
    void onClickListener(View view, int position);
}
