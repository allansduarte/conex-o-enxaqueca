package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 20/11/15.
 */
public interface MetodoAlivioEntidade
{
    String ID = "id";
    String USUARIO = "usuario";
    String DISPLAY = "display";
    String NOME = "nome";
}
