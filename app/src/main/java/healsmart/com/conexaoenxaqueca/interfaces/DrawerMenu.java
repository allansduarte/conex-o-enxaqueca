package healsmart.com.conexaoenxaqueca.interfaces;

/**
 * Created by allan on 02/12/15.
 */
public interface DrawerMenu
{
    int PAINEL_CONTROLE = 1;
    int PERFIL_USUARIO = 2;
    int LOGOUT = 4;
}
