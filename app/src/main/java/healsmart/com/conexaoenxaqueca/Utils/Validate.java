package healsmart.com.conexaoenxaqueca.Utils;

import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by allan on 01/10/15.
 */
public class Validate
{
    /**
     * Responsável por fazer a validação semântica de e-mail
     *
     * @param emailEditText EditText é referencia para a lógica do método
     */
    public boolean isValidEmail(EditText emailEditText)
    {
        boolean validate = Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches();

        if(!validate)
        {
            emailEditText.requestFocus();
            emailEditText.setError("E-mail inválido");
        }
        return validate;
    }

    /**
     * Verifica e apresenta erros de tamanho mínimo de campos
     *
     * @param editText EditText é referencia para a lógica do método
     * @param size int é a referência de verificação de tamanho
     * @param message é a mensagem que vai ser apresentada caso erro permanecer
     */
    public boolean maxSizeValid(EditText editText, int size, String message)
    {
        if (editText.length() > size) {
            editText.requestFocus();
            editText.setError(message);
            return false;
        }

        return true;
    }

    /**
     * Verifica e apresenta erros de tamanho mínimo de campos
     *
     * @param editText EditText é referencia para a lógica do método
     * @param size int é a referência de verificação de tamanho
     * @param message é a mensagem que vai ser apresentada caso erro permanecer
     */
    public boolean hasSizeValid(EditText editText, int size, String message)
    {
        if (!(editText.length() >= size)) {
            editText.requestFocus();
            editText.setError(message);
            return false;
        }

        return true;
    }

    /**
     * Verifica se o componente vazio e apresenta erro
     *
     * @param textView TextView é referencia para a lógica do método
     * @param message é a mensagem que vai ser apresentada caso erro permanecer
     */
    public boolean isEmptyFields(TextView textView, String message)
    {
        if (TextUtils.isEmpty(textView.getText().toString())) {
            textView.requestFocus(); //seta o foco para o campo user
            textView.setError(message);
            return true;
        }
        return false;
    }

    /**
     * Verifica se o componente vazio e apresenta erro
     *
     * @param editText EditText é referencia para a lógica do método
     * @param message é a mensagem que vai ser apresentada caso erro permanecer
     */
    public boolean isEmptyFields(EditText editText, String message)
    {
        if (TextUtils.isEmpty(editText.getText().toString())) {
            editText.requestFocus(); //seta o foco para o campo user
            editText.setError(message);
            return true;
        }
        return false;
    }

    /**
     * Verifica e limpa o campo de mensagens de erros
     *
     * @param editText EditText é referencia para a lógica do método
     */
    public void verifyAndClearErrors(EditText editText)
    {
        if (!editText.toString().isEmpty()) {
            clearErrorFields(editText);
        }
    }

    /**
     * Limpa os ícones e as mensagens de erro dos campos desejados
     *
     * @param editText EditText é referência para definir erro como nulo
     */
    private void clearErrorFields(EditText editText)
    {
        editText.setError(null);
    }

}
