package healsmart.com.conexaoenxaqueca.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;

/**
 * Created by allan on 03/10/15.
 */
public class Utils
{
    private static final String TAG = "UtilsService";

    public void hideStatusBar(Activity activity)
    {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public String generateHash(Context context)
    {
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        String  currentDate = String.valueOf(Calendar.getInstance().getTimeInMillis());
        UUID uuid = UUID.randomUUID();

        return deviceId +"-"+ uuid + currentDate;
    }

    public void redirectTo(Context from, Class to)
    {
        Intent intent = new Intent(from, to);
        from.startActivity(intent);
    }

    /**
     * Método responsável por separar o nome completo em duas partes. As partes são primeiro nome e último nome.
     *
     * @return String[]
     */
    public String[] splitCompleteName(String name)
    {
        String[] names = name.trim().split(" ");
        String firstName = names[0];
        String lastName = name.substring(names[0].length()).trim();

        return new String[]{firstName, lastName};
    }

    public long getCurrentDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.get(Calendar.YEAR);
        calendar.get(Calendar.MONTH);
        calendar.get(Calendar.DAY_OF_MONTH);
        calendar.get(Calendar.HOUR_OF_DAY);
        calendar.get(Calendar.MINUTE);
        calendar.get(Calendar.SECOND);
        calendar.get(Calendar.MILLISECOND);

        return calendar.getTimeInMillis();
    }

    public static <K, V> HashMap<K, V> castHash(HashMap input,
                                                Class<K> keyClass,
                                                Class<V> valueClass) {
        HashMap<K, V> output = new HashMap<>();
        if (input == null)
            return output;
        for (Object key: input.keySet().toArray()) {
            if ((key == null) || (keyClass.isAssignableFrom(key.getClass()))) {
                Object value = input.get(key);
                if ((value == null) || (valueClass.isAssignableFrom(value.getClass()))) {
                    K k = keyClass.cast(key);
                    V v = valueClass.cast(value);
                    output.put(k, v);
                } else {
                    throw new AssertionError(
                            "Cannot cast to HashMap<"+ keyClass.getSimpleName()
                                    +", "+ valueClass.getSimpleName() +">"
                                    +", value "+ value +" is not a "+ valueClass.getSimpleName()
                    );
                }
            } else {
                throw new AssertionError(
                        "Cannot cast to HashMap<"+ keyClass.getSimpleName()
                                +", "+ valueClass.getSimpleName() +">"
                                +", key "+ key +" is not a " + keyClass.getSimpleName()
                );
            }
        }
        return output;
    }

    public static <T> List<T> castList(Object obj, Class<T> clazz)
    {
        List<T> result = new ArrayList<>();
        if(obj instanceof List<?>)
        {
            for (Object o : (List<?>) obj)
            {
                try{
                    result.add(clazz.cast(o));
                } catch(ClassCastException e)
                {
                }

            }
            return result;
        }
        return null;
    }

    public String[] getTimeDateDuration(long longDataIni, long longDataFim)
    {
        SimpleDateFormat dateFormatDate = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat horaFormatDate = new SimpleDateFormat("HH:mm");

        Calendar calendarIni = Calendar.getInstance();
        calendarIni.setTimeInMillis(longDataIni);

        String dataFim = null, horaFim = null;
        long dataIniTimeMillis = 0, dataFimTimeMillis = 0;

        if(longDataFim > 0)
        {
            Calendar calendarFim = Calendar.getInstance();
            calendarFim.setTimeInMillis(longDataFim);

            Date dateFim = calendarFim.getTime();
            dataFim = dateFormatDate.format(dateFim);
            horaFim = horaFormatDate.format(dateFim);

            dataIniTimeMillis = calendarIni.getTimeInMillis();
            dataFimTimeMillis = calendarFim.getTimeInMillis();
        }

        Date dateIni = calendarIni.getTime();
        String dataIni = dateFormatDate.format(dateIni);
        String horaIni = horaFormatDate.format(dateIni);

        long minutoAuxiliar = (long) ((dataFimTimeMillis - dataIniTimeMillis) / (float) (1000*60));
        long minutos = minutoAuxiliar%60;
        long horas  = minutoAuxiliar/60;

        return new String[]{dataIni, horaIni, dataFim, horaFim, String.valueOf(minutos), String.valueOf(horas)};

    }

    public boolean validarDialogAddOption(View v, Resources resources) {

        EditText nomeOpcaoEditText = (EditText) v.findViewById(R.id.optionName);

        return ( !ServiceFactory.getInstance().getServiceValidate().isEmptyFields(nomeOpcaoEditText, resources.getString(R.string.validacao_campo_obrigatorio))
                && ServiceFactory.getInstance().getServiceValidate().hasSizeValid(nomeOpcaoEditText, 3, "Opção deve conter no mínimo 3 caracteres.")
                && ServiceFactory.getInstance().getServiceValidate().maxSizeValid(nomeOpcaoEditText, 20, "Opção deve conter no máximo 20 caracteres.")
        );
    }

    public String textToCapitalize(String text)
    {
        text = text.substring(0,1).toUpperCase() + text.substring(1).toLowerCase();

        return text;
    }

    public int getNrCamposNulosUsuario(Activity activity)
    {
        int nrFieldsNull = 0;

        SharedPreferences mPreferences = activity.getSharedPreferences(PerfilUsuario.USUARIO_PREFS, Context.MODE_PRIVATE);
        String userId = mPreferences.getString(PerfilUsuario.USUARIO_PREFS_USUARIO_ID, null);

        if(userId != null)
        {
            Realm realm = Realm.getDefaultInstance();
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, userId).findFirst();

            if(usuario.getNome() == null)
                nrFieldsNull++;
            if(usuario.getDataNascimento() == 0)
                nrFieldsNull++;
            if(usuario.getSenha() == null)
                nrFieldsNull++;

            realm.close();
        }

        return nrFieldsNull;
    }

    public Map.Entry getEntry(int id, Map<Integer, List<Integer>> listMap)
    {
        Iterator iterator = listMap.entrySet().iterator();
        int n = 0;
        while(iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            if(n == id){
                return entry;
            }
            n ++;
        }

        return null;
    }

    public String getNameMonth(int month, Resources resources)
    {
        return resources.getStringArray(R.array.monthNames)[month];
    }
}
