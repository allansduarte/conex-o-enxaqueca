package healsmart.com.conexaoenxaqueca.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Typeface;

import static android.graphics.BitmapFactory.decodeResource;

/**
 * Created by allan on 28/10/15.
 */
public class PaintDraw
{

    private Context mContext;

    private int left;
    private int top;
    private int right;
    private int bottom;

    public PaintDraw(Context context)
    {
        mContext = context;
    }

    public Bitmap drawTextToBitmap(String texto, int icDrawable, int color)
    {
        Bitmap bitmap = Bitmap.createBitmap(decodeResource(mContext.getResources(), icDrawable));

        Bitmap.Config bitmapConfig =   bitmap.getConfig();
        if(bitmapConfig == null) {
            bitmapConfig = Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);

        float scale = mContext.getResources().getDisplayMetrics().density;
        int fontSize = (int) (16 * scale);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(mContext.getResources().getColor(color));
        paint.setTextSize(fontSize);
        paint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

        drawCenter(canvas, paint, texto);

        return bitmap;
    }

    private void drawCenter(Canvas canvas, Paint paint, String text) {
        int cHeight = canvas.getClipBounds().height();
        int cWidth = canvas.getClipBounds().width();
        Rect r = new Rect();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = cHeight / 2f + r.height() / 2f - r.bottom;

        if(getLeft() > 0)
            x = x - getLeft();
        if(getTop() > 0)
            y = y - getTop();
        if(getRight() > 0)
            x = x + getRight();
        if(getBottom() > 0)
            y = y + getBottom();


        canvas.drawText(text, x, y, paint);
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }
}
