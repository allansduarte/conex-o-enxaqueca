package healsmart.com.conexaoenxaqueca.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import healsmart.com.conexaoenxaqueca.SaveUsuarioTask;
import healsmart.com.conexaoenxaqueca.activities.MainActivity;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;

public class LoginManager implements PerfilUsuario
{

    public LoginManager()
    {
        ServiceFactory.iniciar();
    }

    public void salvarUsuario(LoginResult loginResult, final Context context)
    {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject jsonObject,
                            GraphResponse response) {

                        String fbDataNasc,
                                fbEmail = null,
                                fbPrimeiroNome = null,
                                fbUltimoNome = null,
                                fbGenero = null;

                        try {
                            fbDataNasc = jsonObject.get("birthday").toString();
                        } catch (JSONException e) {
                            fbDataNasc = null;
                        }

                        try {
                            fbEmail = jsonObject.get("email").toString();
                        } catch (JSONException e) {
                            fbEmail = null;
                        }

                        try {
                            fbPrimeiroNome = jsonObject.get("first_name").toString();
                            fbUltimoNome = jsonObject.get("last_name").toString();
                        } catch (JSONException e) {
                            fbPrimeiroNome = null;
                            fbUltimoNome = null;
                        }

                        try {
                            fbGenero = jsonObject.get("gender").toString();
                        } catch (JSONException e) {
                            fbGenero = null;
                        }

                        Realm realm = Realm.getDefaultInstance();
                        Usuario usuario =  realm.where(Usuario.class)
                                .equalTo(UsuarioEntidade.EMAIL, fbEmail)
                                .findFirst();

                        Usuario u = null;
                        if(usuario != null)
                            u = Usuario.copy(usuario);

                        if(u == null)
                        {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

                            long dataNasc;
                            try {
                                dataNasc = dateFormat.parse(fbDataNasc).getTime();
                            } catch (ParseException e) {
                                dataNasc = 0;
                            } catch(NullPointerException e){
                                dataNasc = 0;
                            }

                            String userId = ServiceFactory.getInstance().getServiceUtils().generateHash(context);
                            u = ServiceFactory.getInstance().getServiceUsuario();
                            u.setId(userId);
                            u.setEmail(fbEmail);
                            u.setSenha(null);
                            u.setNome(fbPrimeiroNome);
                            u.setSobrenome(fbUltimoNome);
                            u.setDataNascimento(dataNasc);
                            u.setGenero(fbGenero);

                            new SaveUsuarioTask(context).execute(u);
                        } else {
                            ServiceFactory.getInstance().getServiceUtils().redirectTo(context, MainActivity.class);

                            SharedPreferences prefs = context.getSharedPreferences(USUARIO_PREFS, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putBoolean(USUARIO_PREFS_LOGIN, true);
                            editor.putString(USUARIO_PREFS_USUARIO_ID, u.getId());
                            editor.apply();
                        }

                        realm.close();
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,gender,birthday,email");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
