package healsmart.com.conexaoenxaqueca.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.SplashScreen;
import healsmart.com.conexaoenxaqueca.activities.MainActivity;
import healsmart.com.conexaoenxaqueca.activities.PerfilActivity;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.DrawerMenu;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;

public class NavigationDrawer implements PerfilUsuario, DrawerMenu
{
    private Activity mActivity;
    private Toolbar mToolbar;
    private int mItemId;
    private int mDrawerContainer;
    private Bundle mSavedInstanceState;

    public NavigationDrawer(Activity activity, Toolbar toolbar)
    {
        mActivity = activity;
        mToolbar = toolbar;
    }

    public NavigationDrawer(Activity activity, Toolbar toolbar, int itemId, Bundle savedInstanceState, int drawerContainer) {
        mActivity = activity;
        mToolbar = toolbar;
        mItemId = itemId;
        mSavedInstanceState = savedInstanceState;
        mDrawerContainer = drawerContainer;
    }

    public NavigationDrawer(Activity activity, Toolbar toolbar, int drawerContainer) {
        mActivity = activity;
        mToolbar = toolbar;
        mDrawerContainer = drawerContainer;
    }

    private AccountHeader header()
    {
        SharedPreferences mPreferences = mActivity.getSharedPreferences(USUARIO_PREFS, Context.MODE_PRIVATE);
        String userId= mPreferences.getString(USUARIO_PREFS_USUARIO_ID, null);

        AccountHeader accountHeaderBuilder = new AccountHeaderBuilder()
                .withActivity(mActivity)
                .withCompactStyle(true)
                .withThreeSmallProfileImages(false)
                .withHeaderBackground(R.color.verdeEscuro)
                .build();

            if(mSavedInstanceState != null)
                accountHeaderBuilder.saveInstanceState(mSavedInstanceState);

        Realm realm = Realm.getDefaultInstance();
        if(userId != null)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, userId).findFirst();
            usuario = Usuario.copy(usuario);
            String nome = null;
            String sobrenome = null;
            if(usuario.getNome() != null) {
                nome = (usuario.getNome().trim().equals("") ? null :  usuario.getNome());
                sobrenome = (usuario.getSobrenome().trim().equals("") ? null :  usuario.getSobrenome());
            }
            String nomeCompleto = "Conexão enxaqueca";
            if(nome != null && sobrenome != null)
                nomeCompleto = usuario.getNome() + " " + usuario.getSobrenome();

            accountHeaderBuilder.addProfiles(
                    new ProfileDrawerItem().withName(nomeCompleto).withEmail(usuario.getEmail()).withIcon(ContextCompat.getDrawable(mActivity.getApplicationContext(), R.drawable.ic_img_perfil))
            );
        }
        realm.close();

        return accountHeaderBuilder;
    }

    public Drawer drawerMenu()
    {
        Drawer drawer;

        if(mDrawerContainer != 0)
        {
            drawer = new DrawerBuilder()
                    .withActivity(mActivity)
                    .withRootView(mDrawerContainer)
                    .withToolbar(mToolbar)
                    .withActionBarDrawerToggleAnimated(true)
                    .withSelectedItem(-1)
                    .withAccountHeader(header())
                    .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem)
                        {
                            boolean action = false;
                            switch (position) {
                                case PAINEL_CONTROLE:

                                    if (position != mItemId)  {
                                        ServiceFactory.getInstance().getServiceUtils().redirectTo(mActivity, MainActivity.class);
                                        action=true;
                                    }
                                    break;
                                case PERFIL_USUARIO:

                                    if (position != mItemId) {
                                        ServiceFactory.getInstance().getServiceUtils().redirectTo(mActivity, PerfilActivity.class);
                                        action=true;
                                    }
                                    break;
                                case LOGOUT:
                                    sairDoApp();
                                    action=true;
                                    break;
                            }
                            return action;
                        }
                    })
                    .build();
        } else {
            drawer = new DrawerBuilder()
                    .withActivity(mActivity)
                    .withToolbar(mToolbar)
                    .withActionBarDrawerToggleAnimated(true)
                    .withSelectedItem(-1)
                    .withAccountHeader(header())
                    .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem)
                        {
                            boolean action = false;
                            switch (position) {
                                case PAINEL_CONTROLE:

                                    if (position != mItemId) {
                                        ServiceFactory.getInstance().getServiceUtils().redirectTo(mActivity, MainActivity.class);
                                        action=true;
                                    }
                                    break;
                                case PERFIL_USUARIO:

                                    if (position != mItemId) {
                                        ServiceFactory.getInstance().getServiceUtils().redirectTo(mActivity, PerfilActivity.class);
                                        action=true;
                                    }
                                    break;
                                case LOGOUT:
                                    sairDoApp();
                                    action=true;
                                    break;
                            }
                            return action;
                        }
                    })
                    .build();
        }

        drawer.setItems(getItensMenu());

        return drawer;
    }

    private ArrayList<IDrawerItem> getItensMenu()
    {
        ArrayList<IDrawerItem> listItens = new ArrayList<>();

        PrimaryDrawerItem painelControleItem = new PrimaryDrawerItem().withName("Painel de controle").withIcon(mActivity.getResources().getDrawable(R.drawable.ic_action_painel_de_controle));
        painelControleItem.withSetSelected((mItemId==PAINEL_CONTROLE));
        listItens.add(painelControleItem);

        PrimaryDrawerItem perfilItem = new PrimaryDrawerItem().withName("Perfil").withIcon(mActivity.getResources().getDrawable(R.drawable.ic_action_perfil));

        int nrNullFields = ServiceFactory.getInstance().getServiceUtils().getNrCamposNulosUsuario(mActivity);
        if(nrNullFields > 0)
            perfilItem.withBadge(String.valueOf(nrNullFields)).withBadgeStyle(new BadgeStyle().withTextColor(Color.WHITE).withColorRes(R.color.laranja));

        perfilItem.withSetSelected((mItemId==PERFIL_USUARIO));
        listItens.add(perfilItem);

        listItens.add(new DividerDrawerItem());

        SecondaryDrawerItem logOut = new SecondaryDrawerItem().withName("Logout").withIcon(mActivity.getResources().getDrawable(R.drawable.ic_action_logout));
        logOut.withSetSelected(false);

        listItens.add(logOut);

        return listItens;
    }

    public Drawer run()
    {
        return drawerMenu();
    }

    private void sairDoApp() {

        SharedPreferences preferences = mActivity.getSharedPreferences(USUARIO_PREFS, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(USUARIO_PREFS_LOGIN, false);
        editor.putString(USUARIO_PREFS_USUARIO_ID, null);
        editor.putBoolean(QUALIDADE_DOR_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(SINTOMAS_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(METODO_ALIVIO_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(CONSEQUENCIA_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(LOCAL_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(GATILHO_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(GATILHO_BEBIDA_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(GATILHO_COMIDA_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(GATILHO_ESFORCO_FISICO_OPTIONS_ADDED_TAG, false);
        editor.putBoolean(LOCAIS_DOR_OPTIONS_ADDED_TAG, false);
        editor.apply();

        ServiceFactory.getInstance().getServiceUtils().redirectTo(mActivity, SplashScreen.class);
    }
}
