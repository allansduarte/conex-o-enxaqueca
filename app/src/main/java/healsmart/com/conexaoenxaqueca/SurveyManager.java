package healsmart.com.conexaoenxaqueca;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Bebida;
import healsmart.com.conexaoenxaqueca.domain.Comida;
import healsmart.com.conexaoenxaqueca.domain.Consequencia;
import healsmart.com.conexaoenxaqueca.domain.EsforcoFisico;
import healsmart.com.conexaoenxaqueca.domain.Gatilho;
import healsmart.com.conexaoenxaqueca.domain.LocaisDeDor;
import healsmart.com.conexaoenxaqueca.domain.Local;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivio;
import healsmart.com.conexaoenxaqueca.domain.QualidadeDor;
import healsmart.com.conexaoenxaqueca.domain.Sintoma;
import healsmart.com.conexaoenxaqueca.domain.Usuario;
import healsmart.com.conexaoenxaqueca.interfaces.BebidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.ComidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.ConsequenciaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.EsforcoFisicoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.GatilhoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.LocaisDeDorEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.LocalEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.MetodoAlivioEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.QualidadeDorEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SintomaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;

public class SurveyManager implements SurveyConstants, PerfilUsuario, Parcelable
{
    private Context mContext;
    private String mCriseId;
    private String mUserId;
    private int mMode;
    private boolean mUpdateMode;

    public SurveyManager(Context context)
    {
        mContext = context;
        SharedPreferences preferences = mContext.getSharedPreferences(USUARIO_PREFS, Context.MODE_PRIVATE);
        setUserId(preferences.getString(USUARIO_PREFS_USUARIO_ID, null));
    }

    public int alterarEstado(int mode, String msg, String title, TextView surveyTitleTextView, View v)
    {
        if(mode != MODE_DELETE)
        {
            mode = MODE_DELETE;
            changeMode(mode, msg, title, surveyTitleTextView);
            v.setSelected(true);
        }
        else
        {
            mode = MODE_INSERT;
            changeMode(mode, msg, title, surveyTitleTextView);
            v.setSelected(false);
        }

        return mode;
    }

    private void changeMode(int mode, String msg, String title, TextView surveyTitleTextView)
    {
        Resources mResources = mContext.getResources();

        if(mode == MODE_DELETE){
            surveyTitleTextView.setText(msg);
            surveyTitleTextView.setBackgroundColor(mResources.getColor(R.color.laranja));
        } else if(mode == MODE_INSERT){
            surveyTitleTextView.setText(title);
            surveyTitleTextView.setBackgroundColor(mResources.getColor(R.color.verdeClaro));
        }
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.fade_in);
        surveyTitleTextView.startAnimation(animation);
    }

    public void insertInitDataQualidadeDor()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(QualidadeDor.class)
                .equalTo(QualidadeDorEntidade.USUARIO+"."+UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            QualidadeDor qualidadeDorPulsatil = new QualidadeDor();
            qualidadeDorPulsatil.setId(new Utils().generateHash(mContext));
            qualidadeDorPulsatil.setNome("pulsátil");
            qualidadeDorPulsatil.setUsuario(usuario);
            qualidadeDorPulsatil.setDisplay(QUESTION_ACTIVE);
            qualidadeDorPulsatil.setCreated(dataAtual);

            QualidadeDor qualidadeDorPressao = new QualidadeDor();
            qualidadeDorPressao.setId(new Utils().generateHash(mContext));
            qualidadeDorPressao.setNome("pressão");
            qualidadeDorPressao.setUsuario(usuario);
            qualidadeDorPressao.setDisplay(QUESTION_ACTIVE);
            qualidadeDorPressao.setCreated(dataAtual);

            QualidadeDor qualidadeDorPeso = new QualidadeDor();
            qualidadeDorPeso.setId(new Utils().generateHash(mContext));
            qualidadeDorPeso.setNome("peso");
            qualidadeDorPeso.setUsuario(usuario);
            qualidadeDorPeso.setDisplay(QUESTION_ACTIVE);
            qualidadeDorPeso.setCreated(dataAtual);

            QualidadeDor qualidadeDorPontada = new QualidadeDor();
            qualidadeDorPontada.setId(new Utils().generateHash(mContext));
            qualidadeDorPontada.setNome("pontada");
            qualidadeDorPontada.setUsuario(usuario);
            qualidadeDorPontada.setDisplay(QUESTION_ACTIVE);
            qualidadeDorPontada.setCreated(dataAtual);

            QualidadeDor qualidadeDorExplosao = new QualidadeDor();
            qualidadeDorExplosao.setId(new Utils().generateHash(mContext));
            qualidadeDorExplosao.setNome("explosão");
            qualidadeDorExplosao.setUsuario(usuario);
            qualidadeDorExplosao.setDisplay(QUESTION_ACTIVE);
            qualidadeDorExplosao.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_QUALIDADE_DOR_ID).execute(
                    qualidadeDorPulsatil,
                    qualidadeDorPressao,
                    qualidadeDorPeso,
                    qualidadeDorPontada,
                    qualidadeDorExplosao
            );
        }
    }

    public void insertInitDataSintomas()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrSintomas =  realm.where(Sintoma.class)
                                .equalTo(SintomaEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                                .count();

        if(nrSintomas == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            Sintoma sintomaNausea = new Sintoma();
            sintomaNausea.setId(new Utils().generateHash(mContext));
            sintomaNausea.setNome("náusea");
            sintomaNausea.setUsuario(usuario);
            sintomaNausea.setDisplay(QUESTION_ACTIVE);
            sintomaNausea.setCreated(dataAtual);

            Sintoma sintomaVomito = new Sintoma();
            sintomaVomito.setId(new Utils().generateHash(mContext));
            sintomaVomito.setNome("vômito");
            sintomaVomito.setUsuario(usuario);
            sintomaVomito.setDisplay(QUESTION_ACTIVE);
            sintomaVomito.setCreated(dataAtual);

            Sintoma sintomaIluz = new Sintoma();
            sintomaIluz.setId(new Utils().generateHash(mContext));
            sintomaIluz.setNome("intolerância a luz");
            sintomaIluz.setUsuario(usuario);
            sintomaIluz.setDisplay(QUESTION_ACTIVE);
            sintomaIluz.setCreated(dataAtual);

            Sintoma sintomaIBarulho = new Sintoma();
            sintomaIBarulho.setId(new Utils().generateHash(mContext));
            sintomaIBarulho.setNome("intolerância ao barulho");
            sintomaIBarulho.setUsuario(usuario);
            sintomaIBarulho.setDisplay(QUESTION_ACTIVE);
            sintomaIBarulho.setCreated(dataAtual);

            Sintoma sintomaVisao = new Sintoma();
            sintomaVisao.setId(new Utils().generateHash(mContext));
            sintomaVisao.setNome("alterações na visão");
            sintomaVisao.setUsuario(usuario);
            sintomaVisao.setDisplay(QUESTION_ACTIVE);
            sintomaVisao.setCreated(dataAtual);

            Sintoma sintomaCongestaoNasal = new Sintoma();
            sintomaCongestaoNasal.setId(new Utils().generateHash(mContext));
            sintomaCongestaoNasal.setNome("congestão nasal");
            sintomaCongestaoNasal.setUsuario(usuario);
            sintomaCongestaoNasal.setDisplay(QUESTION_ACTIVE);
            sintomaCongestaoNasal.setCreated(dataAtual);

            Sintoma sintomaTontura = new Sintoma();
            sintomaTontura.setId(new Utils().generateHash(mContext));
            sintomaTontura.setNome("tontura");
            sintomaTontura.setUsuario(usuario);
            sintomaTontura.setDisplay(QUESTION_ACTIVE);
            sintomaTontura.setCreated(dataAtual);

            Sintoma sintomaNenhum = new Sintoma();
            sintomaNenhum.setId(NULL_OPTION_ID);
            sintomaNenhum.setNome("nenhum");
            sintomaNenhum.setUsuario(usuario);
            sintomaNenhum.setDisplay(QUESTION_ACTIVE);
            sintomaNenhum.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_SINTOMAS_ID).execute(
                    sintomaNausea,
                    sintomaVomito,
                    sintomaIluz,
                    sintomaIBarulho,
                    sintomaVisao,
                    sintomaCongestaoNasal,
                    sintomaTontura,
                    sintomaNenhum
            );
        }
    }

    public void insertInitDataMetodoAlivio()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(MetodoAlivio.class)
                .equalTo(MetodoAlivioEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            MetodoAlivio metodoAlivioRepouso = new MetodoAlivio();
            metodoAlivioRepouso.setId(new Utils().generateHash(mContext));
            metodoAlivioRepouso.setNome("repouso");
            metodoAlivioRepouso.setUsuario(usuario);
            metodoAlivioRepouso.setDisplay(QUESTION_ACTIVE);
            metodoAlivioRepouso.setCreated(dataAtual);

            MetodoAlivio metodoAlivioRelaxamento = new MetodoAlivio();
            metodoAlivioRelaxamento.setId(new Utils().generateHash(mContext));
            metodoAlivioRelaxamento.setNome("relaxamento");
            metodoAlivioRelaxamento.setUsuario(usuario);
            metodoAlivioRelaxamento.setDisplay(QUESTION_ACTIVE);
            metodoAlivioRelaxamento.setCreated(dataAtual);

            MetodoAlivio metodoAlivioQuartoEscuro = new MetodoAlivio();
            metodoAlivioQuartoEscuro.setId(new Utils().generateHash(mContext));
            metodoAlivioQuartoEscuro.setNome("quarto escuro");
            metodoAlivioQuartoEscuro.setUsuario(usuario);
            metodoAlivioQuartoEscuro.setDisplay(QUESTION_ACTIVE);
            metodoAlivioQuartoEscuro.setCreated(dataAtual);

            MetodoAlivio metodoAlivioMassagem = new MetodoAlivio();
            metodoAlivioMassagem.setId(new Utils().generateHash(mContext));
            metodoAlivioMassagem.setNome("massagem");
            metodoAlivioMassagem.setUsuario(usuario);
            metodoAlivioMassagem.setDisplay(QUESTION_ACTIVE);
            metodoAlivioMassagem.setCreated(dataAtual);

            MetodoAlivio metodoAlivioBanho = new MetodoAlivio();
            metodoAlivioBanho.setId(new Utils().generateHash(mContext));
            metodoAlivioBanho.setNome("banho");
            metodoAlivioBanho.setUsuario(usuario);
            metodoAlivioBanho.setDisplay(QUESTION_ACTIVE);
            metodoAlivioBanho.setCreated(dataAtual);

            MetodoAlivio metodoAlivioCompressas = new MetodoAlivio();
            metodoAlivioCompressas.setId(new Utils().generateHash(mContext));
            metodoAlivioCompressas.setNome("compressas");
            metodoAlivioCompressas.setUsuario(usuario);
            metodoAlivioCompressas.setDisplay(QUESTION_ACTIVE);
            metodoAlivioCompressas.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_METODO_ALIVIO_ID).execute(
                    metodoAlivioRepouso,
                    metodoAlivioRelaxamento,
                    metodoAlivioQuartoEscuro,
                    metodoAlivioMassagem,
                    metodoAlivioBanho,
                    metodoAlivioCompressas
            );
        }
    }

    public void insertInitDataConsequencia()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(Consequencia.class)
                .equalTo(ConsequenciaEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            Consequencia consequenciaFaltaTrabalho = new Consequencia();
            consequenciaFaltaTrabalho.setId(new Utils().generateHash(mContext));
            consequenciaFaltaTrabalho.setNome("falta no trabalho");
            consequenciaFaltaTrabalho.setUsuario(usuario);
            consequenciaFaltaTrabalho.setDisplay(QUESTION_ACTIVE);
            consequenciaFaltaTrabalho.setCreated(dataAtual);

            Consequencia consequenciaFaltaEscola = new Consequencia();
            consequenciaFaltaEscola.setId(new Utils().generateHash(mContext));
            consequenciaFaltaEscola.setNome("falta na escola");
            consequenciaFaltaEscola.setUsuario(usuario);
            consequenciaFaltaEscola.setDisplay(QUESTION_ACTIVE);
            consequenciaFaltaEscola.setCreated(dataAtual);

            Consequencia consequenciaAtividadeSociais = new Consequencia();
            consequenciaAtividadeSociais.setId(new Utils().generateHash(mContext));
            consequenciaAtividadeSociais.setNome("atividades sociais");
            consequenciaAtividadeSociais.setUsuario(usuario);
            consequenciaAtividadeSociais.setDisplay(QUESTION_ACTIVE);
            consequenciaAtividadeSociais.setCreated(dataAtual);

            Consequencia consequenciaNaoAfetou = new Consequencia();
            consequenciaNaoAfetou.setId(NULL_OPTION_ID);
            consequenciaNaoAfetou.setNome("não afetou");
            consequenciaNaoAfetou.setUsuario(usuario);
            consequenciaNaoAfetou.setDisplay(QUESTION_ACTIVE);
            consequenciaNaoAfetou.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_CONSEQUENCIAS_ID).execute(
                    consequenciaFaltaTrabalho,
                    consequenciaFaltaEscola,
                    consequenciaAtividadeSociais,
                    consequenciaNaoAfetou
            );
        }
    }

    public void insertInitDataLocal()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(Local.class)
                .equalTo(LocalEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            Local localEmCasa = new Local();
            localEmCasa.setId(new Utils().generateHash(mContext));
            localEmCasa.setNome("em casa");
            localEmCasa.setUsuario(usuario);
            localEmCasa.setDisplay(QUESTION_ACTIVE);
            localEmCasa.setCreated(dataAtual);

            Local localNoTrabalho = new Local();
            localNoTrabalho.setId(new Utils().generateHash(mContext));
            localNoTrabalho.setNome("no trabalho");
            localNoTrabalho.setUsuario(usuario);
            localNoTrabalho.setDisplay(QUESTION_ACTIVE);
            localNoTrabalho.setCreated(dataAtual);

            Local localEscolaUniversidade = new Local();
            localEscolaUniversidade.setId(new Utils().generateHash(mContext));
            localEscolaUniversidade.setNome("escola");
            localEscolaUniversidade.setUsuario(usuario);
            localEscolaUniversidade.setDisplay(QUESTION_ACTIVE);
            localEscolaUniversidade.setCreated(dataAtual);

            Local localEmTransito = new Local();
            localEmTransito.setId(new Utils().generateHash(mContext));
            localEmTransito.setNome("em trânsito");
            localEmTransito.setUsuario(usuario);
            localEmTransito.setDisplay(QUESTION_ACTIVE);
            localEmTransito.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_LOCAL_ID).execute(
                    localEmCasa,
                    localNoTrabalho,
                    localEscolaUniversidade,
                    localEmTransito
            );
        }
    }

    public void insertInitDataGatilho()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(Gatilho.class)
                .equalTo(GatilhoEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            Gatilho gatilhoEstresse = new Gatilho();
            gatilhoEstresse.setId(new Utils().generateHash(mContext));
            gatilhoEstresse.setNome("estresse");
            gatilhoEstresse.setDisplay(QUESTION_ACTIVE);
            gatilhoEstresse.setUsuario(usuario);
            gatilhoEstresse.setCreated(dataAtual);

            Gatilho gatilhoFaltaSono = new Gatilho();
            gatilhoFaltaSono.setId(new Utils().generateHash(mContext));
            gatilhoFaltaSono.setNome("falta de sono");
            gatilhoFaltaSono.setDisplay(QUESTION_ACTIVE);
            gatilhoFaltaSono.setUsuario(usuario);
            gatilhoFaltaSono.setCreated(dataAtual);

            Gatilho gatilhoMenstruacao = new Gatilho();
            gatilhoMenstruacao.setId(new Utils().generateHash(mContext));
            gatilhoMenstruacao.setNome("menstruação");
            gatilhoMenstruacao.setDisplay(QUESTION_ACTIVE);
            gatilhoMenstruacao.setUsuario(usuario);
            gatilhoMenstruacao.setCreated(dataAtual);

            Gatilho gatilhoEsforcoFisico = new Gatilho();
            gatilhoEsforcoFisico.setId(new Utils().generateHash(mContext));
            gatilhoEsforcoFisico.setNome("esforço físico");
            gatilhoEsforcoFisico.setDisplay(QUESTION_ACTIVE);
            gatilhoEsforcoFisico.setUsuario(usuario);
            gatilhoEsforcoFisico.setCreated(dataAtual);

            Gatilho gatilhoAnsiedade = new Gatilho();
            gatilhoAnsiedade.setId(new Utils().generateHash(mContext));
            gatilhoAnsiedade.setNome("ansiedade");
            gatilhoAnsiedade.setDisplay(QUESTION_ACTIVE);
            gatilhoAnsiedade.setUsuario(usuario);
            gatilhoAnsiedade.setCreated(dataAtual);

            Gatilho gatilhoJejum = new Gatilho();
            gatilhoJejum.setId(new Utils().generateHash(mContext));
            gatilhoJejum.setNome("jejum");
            gatilhoJejum.setDisplay(QUESTION_ACTIVE);
            gatilhoJejum.setUsuario(usuario);
            gatilhoJejum.setCreated(dataAtual);

            Gatilho gatilhoBebida = new Gatilho();
            gatilhoBebida.setId(new Utils().generateHash(mContext));
            gatilhoBebida.setNome("bebida");
            gatilhoBebida.setDisplay(QUESTION_ACTIVE);
            gatilhoBebida.setUsuario(usuario);
            gatilhoBebida.setCreated(dataAtual);

            Gatilho gatilhoComida = new Gatilho();
            gatilhoComida.setId(new Utils().generateHash(mContext));
            gatilhoComida.setNome("comida");
            gatilhoComida.setDisplay(QUESTION_ACTIVE);
            gatilhoComida.setUsuario(usuario);
            gatilhoComida.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_GATILHOS_ID).execute(
                    gatilhoEstresse,
                    gatilhoFaltaSono,
                    gatilhoMenstruacao,
                    gatilhoEsforcoFisico,
                    gatilhoAnsiedade,
                    gatilhoJejum,
                    gatilhoBebida,
                    gatilhoComida
            );
        }
    }

    public void insertInitDataBebidas()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(Bebida.class)
                .equalTo(BebidaEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            Bebida bebidaCafe = new Bebida();
            bebidaCafe.setId(new Utils().generateHash(mContext));
            bebidaCafe.setNome("café");
            bebidaCafe.setDisplay(QUESTION_ACTIVE);
            bebidaCafe.setUsuario(usuario);
            bebidaCafe.setCreated(dataAtual);

            Bebida bebidaVinho = new Bebida();
            bebidaVinho.setId(new Utils().generateHash(mContext));
            bebidaVinho.setNome("vinho");
            bebidaVinho.setDisplay(QUESTION_ACTIVE);
            bebidaVinho.setUsuario(usuario);
            bebidaVinho.setCreated(dataAtual);

            Bebida bebidaCerveja = new Bebida();
            bebidaCerveja.setId(new Utils().generateHash(mContext));
            bebidaCerveja.setNome("cerveja");
            bebidaCerveja.setDisplay(QUESTION_ACTIVE);
            bebidaCerveja.setUsuario(usuario);
            bebidaCerveja.setCreated(dataAtual);

            Bebida bebidaDestilados = new Bebida();
            bebidaDestilados.setId(new Utils().generateHash(mContext));
            bebidaDestilados.setNome("destilados");
            bebidaDestilados.setDisplay(QUESTION_ACTIVE);
            bebidaDestilados.setUsuario(usuario);
            bebidaDestilados.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_GATILHOS_BEBIDA_ID).execute(
                    bebidaCafe,
                    bebidaVinho,
                    bebidaCerveja,
                    bebidaDestilados
            );
        }
    }

    public void insertInitDataComida()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(Comida.class)
                .equalTo(ComidaEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            Comida comidaQueijo = new Comida();
            comidaQueijo.setId(new Utils().generateHash(mContext));
            comidaQueijo.setNome("queijo");
            comidaQueijo.setDisplay(QUESTION_ACTIVE);
            comidaQueijo.setUsuario(usuario);
            comidaQueijo.setCreated(dataAtual);

            Comida comidaChocolate = new Comida();
            comidaChocolate.setId(new Utils().generateHash(mContext));
            comidaChocolate.setNome("chocolate");
            comidaChocolate.setDisplay(QUESTION_ACTIVE);
            comidaChocolate.setUsuario(usuario);
            comidaChocolate.setCreated(dataAtual);

            Comida comidaChinesa = new Comida();
            comidaChinesa.setId(new Utils().generateHash(mContext));
            comidaChinesa.setNome("comida chinesa");
            comidaChinesa.setDisplay(QUESTION_ACTIVE);
            comidaChinesa.setUsuario(usuario);
            comidaChinesa.setCreated(dataAtual);

            Comida comidaSalsichaEmbutidos = new Comida();
            comidaSalsichaEmbutidos.setId(new Utils().generateHash(mContext));
            comidaSalsichaEmbutidos.setNome("salsicha e embutidos");
            comidaSalsichaEmbutidos.setDisplay(QUESTION_ACTIVE);
            comidaSalsichaEmbutidos.setUsuario(usuario);
            comidaSalsichaEmbutidos.setCreated(dataAtual);

            Comida comidaFrituras = new Comida();
            comidaFrituras.setId(new Utils().generateHash(mContext));
            comidaFrituras.setNome("frituras");
            comidaFrituras.setDisplay(QUESTION_ACTIVE);
            comidaFrituras.setUsuario(usuario);
            comidaFrituras.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_GATILHOS_COMIDA_ID).execute(
                    comidaQueijo,
                    comidaChocolate,
                    comidaChinesa,
                    comidaSalsichaEmbutidos,
                    comidaFrituras
            );
        }
    }

    public void insertInitDataEsforcoFisico()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(EsforcoFisico.class)
                .equalTo(EsforcoFisicoEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            EsforcoFisico esforcoFisicoCorrida = new EsforcoFisico();
            esforcoFisicoCorrida.setId(new Utils().generateHash(mContext));
            esforcoFisicoCorrida.setNome("corrida");
            esforcoFisicoCorrida.setDisplay(QUESTION_ACTIVE);
            esforcoFisicoCorrida.setUsuario(usuario);
            esforcoFisicoCorrida.setCreated(dataAtual);

            EsforcoFisico esforcoFisicoAcademia = new EsforcoFisico();
            esforcoFisicoAcademia.setId(new Utils().generateHash(mContext));
            esforcoFisicoAcademia.setNome("academia");
            esforcoFisicoAcademia.setDisplay(QUESTION_ACTIVE);
            esforcoFisicoAcademia.setUsuario(usuario);
            esforcoFisicoAcademia.setCreated(dataAtual);

            EsforcoFisico esforcoFisicoEsportes = new EsforcoFisico();
            esforcoFisicoEsportes.setId(new Utils().generateHash(mContext));
            esforcoFisicoEsportes.setNome("esportes");
            esforcoFisicoEsportes.setDisplay(QUESTION_ACTIVE);
            esforcoFisicoEsportes.setUsuario(usuario);
            esforcoFisicoEsportes.setCreated(dataAtual);

            EsforcoFisico esforcoFisicoSexo = new EsforcoFisico();
            esforcoFisicoSexo.setId(new Utils().generateHash(mContext));
            esforcoFisicoSexo.setNome("sexo");
            esforcoFisicoSexo.setDisplay(QUESTION_ACTIVE);
            esforcoFisicoSexo.setUsuario(usuario);
            esforcoFisicoSexo.setCreated(dataAtual);

            EsforcoFisico esforcoFisicoTosse = new EsforcoFisico();
            esforcoFisicoTosse.setId(new Utils().generateHash(mContext));
            esforcoFisicoTosse.setNome("tosse");
            esforcoFisicoTosse.setDisplay(QUESTION_ACTIVE);
            esforcoFisicoTosse.setUsuario(usuario);
            esforcoFisicoTosse.setCreated(dataAtual);

            new AddSurveyOptionsTask(mContext, QUESTION_GATILHOS_ESFORCO_FISICO_ID).execute(
                    esforcoFisicoCorrida,
                    esforcoFisicoAcademia,
                    esforcoFisicoEsportes,
                    esforcoFisicoSexo,
                    esforcoFisicoTosse
            );
        }
    }

    public void insertInitDataLocaisDeDor()
    {
        Utils utils = ServiceFactory.getInstance().getServiceUtils();
        long dataAtual = utils.getCurrentDate();

        Realm realm = Realm.getDefaultInstance();

        long nrOpcoes =  realm.where(LocaisDeDor.class)
                .equalTo(LocaisDeDorEntidade.USUARIO + "." + UsuarioEntidade.ID, getUserId())
                .count();

        if(nrOpcoes == 0)
        {
            Usuario usuario = realm.where(Usuario.class).equalTo(UsuarioEntidade.ID, getUserId()).findFirst();

            LocaisDeDor cervicalOuNucalE = new LocaisDeDor();
            cervicalOuNucalE.setId(1);
            cervicalOuNucalE.setNome("cervical ou nucal esquerda");
            cervicalOuNucalE.setUsuario(usuario);

            LocaisDeDor occipitalE = new LocaisDeDor();
            occipitalE.setId(2);
            occipitalE.setNome("occipital esquerda");
            occipitalE.setUsuario(usuario);

            LocaisDeDor frontalE = new LocaisDeDor();
            frontalE.setId(3);
            frontalE.setNome("frontal esquerda");
            frontalE.setUsuario(usuario);

            LocaisDeDor parietalE = new LocaisDeDor();
            parietalE.setId(4);
            parietalE.setNome("parietal esquerda");
            parietalE.setUsuario(usuario);

            LocaisDeDor orbitariaE = new LocaisDeDor();
            orbitariaE.setId(5);
            orbitariaE.setNome("orbitária esquerda");
            orbitariaE.setUsuario(usuario);

            LocaisDeDor arteriaTemporalProfundaPosteriorE = new LocaisDeDor();
            arteriaTemporalProfundaPosteriorE.setId(6);
            arteriaTemporalProfundaPosteriorE.setNome("artéria temporal profunda posterior esquerda");
            arteriaTemporalProfundaPosteriorE.setUsuario(usuario);

            LocaisDeDor temporalE = new LocaisDeDor();
            temporalE.setId(7);
            temporalE.setNome("temporal esquerda");
            temporalE.setUsuario(usuario);

            LocaisDeDor maxilarE = new LocaisDeDor();
            maxilarE.setId(8);
            maxilarE.setNome("maxilar esquerda");
            maxilarE.setUsuario(usuario);

            LocaisDeDor auricularEATME = new LocaisDeDor();
            auricularEATME.setId(9);
            auricularEATME.setNome("auricular  e ATM esquerda");
            auricularEATME.setUsuario(usuario);

            LocaisDeDor vertex = new LocaisDeDor();
            vertex.setId(10);
            vertex.setNome("vértex");
            vertex.setUsuario(usuario);

            LocaisDeDor cervicalOuNucalD = new LocaisDeDor();
            cervicalOuNucalD.setId(11);
            cervicalOuNucalD.setNome("cervical ou nucal direita");
            cervicalOuNucalD.setUsuario(usuario);

            LocaisDeDor occipitalD = new LocaisDeDor();
            occipitalD.setId(12);
            occipitalD.setNome("occipital direita");
            occipitalD.setUsuario(usuario);

            LocaisDeDor frontalD = new LocaisDeDor();
            frontalD.setId(13);
            frontalD.setNome("frontal direita");
            frontalD.setUsuario(usuario);

            LocaisDeDor parietalD = new LocaisDeDor();
            parietalD.setId(14);
            parietalD.setNome("parietal direita");
            parietalD.setUsuario(usuario);

            LocaisDeDor orbitariaD = new LocaisDeDor();
            orbitariaD.setId(15);
            orbitariaD.setNome("orbitária direita");
            orbitariaD.setUsuario(usuario);

            LocaisDeDor arteriaTemporalProfundaPosteriorD = new LocaisDeDor();
            arteriaTemporalProfundaPosteriorD.setId(16);
            arteriaTemporalProfundaPosteriorD.setNome("artéria temporal profunda posterior direita");
            arteriaTemporalProfundaPosteriorD.setUsuario(usuario);

            LocaisDeDor temporalD = new LocaisDeDor();
            temporalD.setId(17);
            temporalD.setNome("temporal direita");
            temporalD.setUsuario(usuario);

            LocaisDeDor maxilarD = new LocaisDeDor();
            maxilarD.setId(18);
            maxilarD.setNome("maxilar direita");
            maxilarD.setUsuario(usuario);

            LocaisDeDor auricularEATMD = new LocaisDeDor();
            auricularEATMD.setId(19);
            auricularEATMD.setNome("auricular  e ATM direita");
            auricularEATMD.setUsuario(usuario);

            new AddSurveyOptionsTask(mContext, QUESTION_LOCAIS_DOR_ID).execute(
                    cervicalOuNucalE,
                    occipitalE,
                    frontalE,
                    parietalE,
                    orbitariaE,
                    arteriaTemporalProfundaPosteriorE,
                    temporalE,
                    maxilarE,
                    auricularEATME,
                    vertex,
                    cervicalOuNucalD,
                    occipitalD,
                    frontalD,
                    parietalD,
                    orbitariaD,
                    arteriaTemporalProfundaPosteriorD,
                    temporalD,
                    maxilarD,
                    auricularEATMD
            );
        }
    }

    public String getCriseId() {
        return mCriseId;
    }

    public void setCriseId(String mCriseId) {
        this.mCriseId = mCriseId;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mCriseId);
        dest.writeString(mUserId);
    }

    public static final Creator CREATOR = new Creator() {
        public SurveyManager createFromParcel(Parcel in) {
            return new SurveyManager(in);
        }
        public SurveyManager[] newArray(int size) {
            return new SurveyManager[size];
        }
    };

    private SurveyManager(Parcel in) {
        mCriseId = in.readString();
        mUserId = in.readString();
    }

    public boolean isUpdateMode() {
        return mUpdateMode;
    }

    public void setUpdateMode(boolean mUpdateMode) {
        this.mUpdateMode = mUpdateMode;
    }

    public int getMode() {
        return mMode;
    }

    public void setMode(int mode) {
        this.mMode = mode;
    }
}
