package healsmart.com.conexaoenxaqueca.adapters;

import android.os.Parcel;
import android.os.Parcelable;

public class SurveyOptions implements Parcelable
{
    private String id;
    private int drawableOption;
    private String titleOption;
    private int quantidadeMedicamentoOption;
    private int type;
    private int color;

    public SurveyOptions() {
    }

    public SurveyOptions(String titleOption, int drawableOption) {
        this.drawableOption = drawableOption;
        this.titleOption = titleOption;
    }

    public SurveyOptions(String titleOption, int quantidadeMedicamentoOption, int drawableOption) {
        this.drawableOption = drawableOption;
        this.titleOption = titleOption;
        this.quantidadeMedicamentoOption = quantidadeMedicamentoOption;
    }

    public SurveyOptions(String titleOption, int drawableOption, String id) {
        this.drawableOption = drawableOption;
        this.titleOption = titleOption;
        this.id = id;
    }

    public SurveyOptions(String titleOption, int drawableOption, String id, int type) {
        this.drawableOption = drawableOption;
        this.titleOption = titleOption;
        this.id = id;
        this.type = type;
    }

    public int getQuantidadeMedicamentoOption() {
        return quantidadeMedicamentoOption;
    }

    public void setQuantidadeMedicamentoOption(int quantidadeMedicamentoOption) {
        this.quantidadeMedicamentoOption = quantidadeMedicamentoOption;
    }

    public int getDrawableOption() {
        return drawableOption;
    }

    public void setDrawableOption(int drawableOption) {
        this.drawableOption = drawableOption;
    }

    public String getTitleOption() {
        return titleOption;
    }

    public void setTitleOption(String titleOption) {
        this.titleOption = titleOption;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeInt(drawableOption);
        dest.writeString(titleOption);
        dest.writeInt(quantidadeMedicamentoOption);
        dest.writeString(id);
        dest.writeInt(type);
        dest.writeInt(color);
    }

    public static final Creator CREATOR = new Creator() {
        public SurveyOptions createFromParcel(Parcel in) {
            return new SurveyOptions(in);
        }
        public SurveyOptions[] newArray(int size) {
            return new SurveyOptions[size];
        }
    };

    private SurveyOptions(Parcel in) {
        drawableOption = in.readInt();
        titleOption = in.readString();
        quantidadeMedicamentoOption = in.readInt();
        id = in.readString();
        type = in.readInt();
        color = in.readInt();
    }
}
