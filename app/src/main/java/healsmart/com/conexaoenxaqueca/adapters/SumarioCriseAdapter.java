package healsmart.com.conexaoenxaqueca.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.Utils.Utils;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Bebida;
import healsmart.com.conexaoenxaqueca.domain.Comida;
import healsmart.com.conexaoenxaqueca.domain.Consequencia;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.EsforcoFisico;
import healsmart.com.conexaoenxaqueca.domain.Gatilho;
import healsmart.com.conexaoenxaqueca.domain.LocaisDeDor;
import healsmart.com.conexaoenxaqueca.domain.Local;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivio;
import healsmart.com.conexaoenxaqueca.domain.QualidadeDor;
import healsmart.com.conexaoenxaqueca.domain.Sintoma;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;

/**
 * Created by allan on 04/11/15.
 */
public class SumarioCriseAdapter extends BaseAdapter implements SurveyConstants
{
    private Map<Double, Object> mListSumario = new TreeMap<>();
    private Context mContext;

    public SumarioCriseAdapter(Context context, Map<Double, Object> sumarioList)
    {
        mContext = context;
        mListSumario = sumarioList;
    }

    @Override
    public int getCount() {
        return mListSumario.size();
    }

    @Override
    public Object getItem(int position) {
        Map.Entry entry = getEntry(position);
        double key = (double) (entry != null ? entry.getKey() : 0);
        return mListSumario.get(key);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_sumario_crise_lista, null);
        }

        Map.Entry entry = getEntry(position);
        double key = (double) (entry != null ? entry.getKey() : 0);

        buildView(key, convertView, entry);

        return convertView;
    }

    private void buildView(double key, View convertView, Map.Entry entry)
    {
        String textoConteudoQuestao;
        StringBuilder textoBuilder = new StringBuilder();
        String textoFaltouResponder = "Clique aqui para responder.";

        if(key == 0)
        {
            Crise crise = (Crise) entry.getValue();
            if(crise.getIntensidade() > 0)
                textoConteudoQuestao = String.valueOf(crise.getIntensidade());
            else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "INTENSIDADE", R.drawable.ic_intensidade_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_MEDICAMENTO_ID)
        {
            List<Medicamento> medicamentos = Utils.castList(entry.getValue(), Medicamento.class);
            int sizeMedicamento = (medicamentos == null ? 0 : medicamentos.size());

            if(sizeMedicamento > 0)
            {
                for(int i=0; i < sizeMedicamento; i++)
                {
                    String sTexto = medicamentos.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);
                    textoBuilder.append(" ");
                    textoBuilder.append("(");
                    textoBuilder.append(medicamentos.get(i).getQuantidade());
                    textoBuilder.append(")");

                    if(i == (sizeMedicamento-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "MEDICAMENTO", R.drawable.ic_medicamento_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_LOCAIS_DOR_ID)
        {
            List<LocaisDeDor> locaisDeDorList = Utils.castList(entry.getValue(), LocaisDeDor.class);
            int sizeLocaisDeDor = (locaisDeDorList == null ? 0 : locaisDeDorList.size());

            if(sizeLocaisDeDor > 0)
            {
                for(int i=0; i < sizeLocaisDeDor; i++)
                {
                    String sTexto = locaisDeDorList.get(i).getNome();
                    String sTextoCapitalize = sTexto.substring(0,1).toUpperCase() + sTexto.substring(1);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeLocaisDeDor-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "LOCALIZAÇÃO DA DOR", R.drawable.ic_localizacao_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_QUALIDADE_DOR_ID)
        {
            List<QualidadeDor> qualidadeDorList = Utils.castList(entry.getValue(), QualidadeDor.class);
            int sizeQualidadeDor = (qualidadeDorList == null ? 0 : qualidadeDorList.size());

            if(sizeQualidadeDor > 0)
            {
                for(int i=0; i < sizeQualidadeDor; i++)
                {
                    String sTexto = qualidadeDorList.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeQualidadeDor-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "QUALIDADE DA DOR", R.drawable.ic_qualidade_dor_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_SINTOMAS_ID)
        {
            List<Sintoma> sintomasList = Utils.castList(entry.getValue(), Sintoma.class);
            int sizeSintoma = (sintomasList == null ? 0 : sintomasList.size());

            if(sizeSintoma > 0)
            {
                for(int i=0; i < sizeSintoma; i++)
                {
                    String sTexto = sintomasList.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeSintoma-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "SINTOMAS", R.drawable.ic_sintomas_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_GATILHOS_ID)
        {
            List<Gatilho> gatilhosList = Utils.castList(entry.getValue(), Gatilho.class);
            int sizeGatilho = (gatilhosList == null ? 0 : gatilhosList.size());

            if(sizeGatilho > 0)
            {
                for(int i=0; i < sizeGatilho; i++)
                {
                    String sTexto = gatilhosList.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeGatilho-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "GATILHOS", R.drawable.ic_fator_precipitante_24, textoConteudoQuestao);
        }
        else if(key == 7.1)
        {
            List<Comida> gatilhoComidaList = Utils.castList(entry.getValue(), Comida.class);
            int sizeGatilhoComida = (gatilhoComidaList == null ? 0 : gatilhoComidaList.size());

            if(sizeGatilhoComida > 0)
            {
                for(int i=0; i < sizeGatilhoComida; i++)
                {
                    String sTexto = gatilhoComidaList.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeGatilhoComida-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();

            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "GATILHOS - COMIDA", R.drawable.ic_comida_24, textoConteudoQuestao);
        }
        else if(key == 7.2)
        {
            List<Bebida> gatilhoBebidaList = Utils.castList(entry.getValue(), Bebida.class);
            int sizeGatilhoBebida = (gatilhoBebidaList == null ? 0 : gatilhoBebidaList.size());

            if(sizeGatilhoBebida > 0)
            {
                for(int i=0; i < sizeGatilhoBebida; i++)
                {
                    String sTexto = gatilhoBebidaList.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeGatilhoBebida-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "GATILHOS - BEBIDA", R.drawable.ic_bebida_24, textoConteudoQuestao);
        }
        else if(key == 7.3)
        {
            List<EsforcoFisico> gatilhoEsforcoFisicoList = Utils.castList(entry.getValue(), EsforcoFisico.class);
            int sizeGatilhoEsforcoFisico = (gatilhoEsforcoFisicoList == null ? 0 : gatilhoEsforcoFisicoList.size());

            if(sizeGatilhoEsforcoFisico > 0)
            {
                for(int i=0; i < sizeGatilhoEsforcoFisico; i++)
                {
                    String sTexto = gatilhoEsforcoFisicoList.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeGatilhoEsforcoFisico-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "GATILHOS - ESFORÇO FÍSICO", R.drawable.ic_esforco_fisico_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_METODO_ALIVIO_ID)
        {
            List<MetodoAlivio> metodoList = Utils.castList(entry.getValue(), MetodoAlivio.class);
            int sizeMetodo = (metodoList == null ? 0 : metodoList.size());

            if(sizeMetodo > 0)
            {
                for(int i=0; i < sizeMetodo; i++)
                {
                    String sTexto = metodoList.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeMetodo-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "MÉTODOS DE ALÍVIO", R.drawable.ic_metodos_de_alivio_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_METODO_ALIVIO_FUNCIONOU_ID)
        {
            List<Object> objectList = Utils.castList(entry.getValue(), Object.class);
            int sizeList = (objectList == null ? 0 : objectList.size());

            if(sizeList > 0)
            {
                List<MetodoAlivio> metodoFuncionouList = Utils.castList(objectList.get(0), MetodoAlivio.class);
                int sizeMetodoFuncionouList = (metodoFuncionouList == null ? 0 : metodoFuncionouList.size());

                List<Medicamento> medicamentoList = Utils.castList(objectList.get(1), Medicamento.class);
                int sizeMedicamentoList = (medicamentoList == null ? 0 : medicamentoList.size());
                if(sizeMedicamentoList > 0)
                {
                    for(int i=0; i < sizeMedicamentoList; i++)
                    {
                        String sTexto = medicamentoList.get(i).getNome();
                        String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                        textoBuilder.append(sTextoCapitalize);

                        if(sizeMetodoFuncionouList == 0 ) textoBuilder.append(".");
                        else textoBuilder.append(", ");
                    }
                }

                if(sizeMetodoFuncionouList > 0)
                {
                    for(int i=0; i < sizeMetodoFuncionouList; i++)
                    {
                        String sTexto = metodoFuncionouList.get(i).getNome();
                        String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                        textoBuilder.append(sTextoCapitalize);

                        if(i == (sizeMetodoFuncionouList-1) ) textoBuilder.append(".");
                        else textoBuilder.append(", ");
                    }
                }

                if(sizeMedicamentoList == 0 && sizeMetodoFuncionouList == 0)
                    textoConteudoQuestao = textoFaltouResponder;
                else
                    textoConteudoQuestao = textoBuilder.toString();

            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "MÉTODOS DE ALÍVIO EFICIENTES", R.drawable.ic_metodos_de_alivio_eficientes_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_CONSEQUENCIAS_ID)
        {
            List<Consequencia> consequenciaList = Utils.castList(entry.getValue(), Consequencia.class);
            int sizeConsequencia = (consequenciaList == null ? 0 : consequenciaList.size());

            if(sizeConsequencia > 0)
            {
                for(int i=0; i < sizeConsequencia; i++)
                {
                    String sTexto = consequenciaList.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeConsequencia-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "CONSEQUÊNCIAS", R.drawable.ic_consequencias_24, textoConteudoQuestao);
        }
        else if(key == QUESTION_LOCAL_ID)
        {
            List<Local> locais = Utils.castList(entry.getValue(), Local.class);
            int sizeLocais = (locais == null ? 0 : locais.size());

            if(sizeLocais > 0)
            {
                for(int i=0; i < sizeLocais; i++)
                {
                    String sTexto = locais.get(i).getNome();
                    String sTextoCapitalize = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(sTexto);

                    textoBuilder.append(sTextoCapitalize);

                    if(i == (sizeLocais-1) ) textoBuilder.append(".");
                    else textoBuilder.append(", ");
                }

                textoConteudoQuestao = textoBuilder.toString();
            } else textoConteudoQuestao = textoFaltouResponder;

            buildItemView(convertView, "LOCAL", R.drawable.ic_local_24, textoConteudoQuestao);
        }
    }

    private void buildItemView(View v, String tituloItem, int drawableICTituloItem, String textoConteudoQuestao)
    {
        TextView itemTitleTextView = (TextView) v.findViewById(R.id.itemTitleTextView);
        itemTitleTextView.setText(tituloItem);
        itemTitleTextView.setCompoundDrawablesWithIntrinsicBounds(0, drawableICTituloItem, 0, 0);

        TextView sumarioContentTextView = (TextView) v.findViewById(R.id.sumarioContentTextView);
        sumarioContentTextView.setText(textoConteudoQuestao);

        if(textoConteudoQuestao.equals("Clique aqui para responder."))
            sumarioContentTextView.setTextColor(ContextCompat.getColor(mContext, R.color.laranja));
        else
            sumarioContentTextView.setTextColor(ContextCompat.getColor(mContext, R.color.preto));
    }

    public Map.Entry getEntry(int id)
    {
        Iterator iterator = mListSumario.entrySet().iterator();
        int n = 0;
        while(iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            if(n == id){
                return entry;
            }
            n ++;
        }

        return null;
    }

    public void setListSumario(Map<Double, Object> sumarioList)
    {
        mListSumario.clear();
        mListSumario.putAll(sumarioList);
        notifyDataSetChanged();
    }

    public Map<Double, Object> getmListSumario() {
        return mListSumario;
    }
}
