package healsmart.com.conexaoenxaqueca.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.activities.SumarioCriseActivity;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;

/**
 * Created by allan on 27/10/15.
 */
public class ListaCriseAdapter extends BaseAdapter implements SurveyConstants
{
    private List<Crise> mListCrise = new ArrayList<>();
    private LayoutInflater mInflater;
    private Context mContext;
    private Class mClazz;

    public ListaCriseAdapter(Context context, List<Crise> crises, Class clazz) {

        mListCrise = crises;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mClazz = clazz;
        ServiceFactory.iniciar();
    }

    public ListaCriseAdapter(Context context, List<Crise> crises) {

        mListCrise = crises;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        ServiceFactory.iniciar();
    }

    @Override
    public int getCount() {
        return mListCrise.size();
    }

    @Override
    public Object getItem(int position) {
        return mListCrise.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Crise crise = (Crise) getItem(position);
        if (view == null) {
            view = mInflater.inflate(R.layout.item_list_crise, null);
        }

        int intensidade = crise.getIntensidade();

        ImageView intensidadeImageView = (ImageView) view.findViewById(R.id.intensidadeImageView);
        Bitmap bitmap = null;
        if(intensidade > 0 && intensidade <= 3){
            bitmap = ServiceFactory.getInstance().getServicePaintDraw(mContext).drawTextToBitmap(String.valueOf(intensidade), R.drawable.ic_cabeca_intensidade_fraca, R.color.brancoGelo);
        } else if(intensidade > 3 && intensidade <= 7)
        {
            bitmap = ServiceFactory.getInstance().getServicePaintDraw(mContext).drawTextToBitmap(String.valueOf(intensidade), R.drawable.ic_cabeca_intensidade_media, R.color.brancoGelo);
        } else if(intensidade > 7 && intensidade <= 10)
        {
            bitmap = ServiceFactory.getInstance().getServicePaintDraw(mContext).drawTextToBitmap(String.valueOf(intensidade), R.drawable.ic_cabeca_intensidade_forte, R.color.brancoGelo);
        } else{
            intensidadeImageView.setVisibility(View.GONE);
        }

        if(bitmap != null)
            intensidadeImageView.setImageBitmap(bitmap);

        TextView dataCriseTextView = (TextView) view.findViewById(R.id.dataCriseTextView);

        dataCriseTextView.setText(buildTextoDataCrise(crise.getDataIni(), crise.getDataFim()));

        TextView duracaoCriseTextView = (TextView) view.findViewById(R.id.duracaoCriseTextView);
        duracaoCriseTextView.setText(buildTextoDuracaoCrise(crise.getDataIni(), crise.getDataFim()));

        final int indice = position;
        View.OnClickListener onClickItem = new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intentSumarioCrise = new Intent(mContext, SumarioCriseActivity.class);
                intentSumarioCrise.putExtra(INTENT_CRISE_ID, mListCrise.get(indice).getId());

                if(mClazz != null)
                    intentSumarioCrise.putExtra(mClazz.getName(), mClazz.getName());

                mContext.startActivity(intentSumarioCrise);
            }
        };

        view.setOnClickListener(onClickItem);

        return view;
    }

    private String buildTextoDataCrise(long dataIni, long dataFim)
    {
        String texto;

        String[] duracaoCrise = ServiceFactory.getInstance().getServiceUtils().getTimeDateDuration(dataIni, dataFim);

        if(duracaoCrise[3] != null)
        {
            texto = "De: "+duracaoCrise[1]+" até "+duracaoCrise[3]+" - "+duracaoCrise[2];
        } else {
            texto = "De: "+duracaoCrise[1]+" até ? - "+duracaoCrise[0];
        }

        return texto;
    }

    private String buildTextoDuracaoCrise(long dataIni, long dataFim)
    {
        String[] duracaoCrise = ServiceFactory.getInstance().getServiceUtils().getTimeDateDuration(dataIni, dataFim);

        long horas = Long.parseLong(duracaoCrise[5]);
        long minutos = Long.parseLong(duracaoCrise[4]);

        String texto;

        texto = "Duração de ";

        if(horas > 0)
        {
            texto += String.valueOf(horas);
            if(horas == 1)
                texto += " hora";
            else
                texto += " horas";
        }

        if(minutos > 0)
        {
            texto += " "+minutos;
            if(minutos == 1)
                texto += " minuto";
            else
                texto += " minutos";
        }

        if(horas == 0 && minutos == 0)
            texto = "Crise ocorrendo";

        return texto;
    }

    public void setCrises(List<Crise> data)
    {
        mListCrise.clear();
        mListCrise.addAll(data);
        notifyDataSetChanged();
    }
}
