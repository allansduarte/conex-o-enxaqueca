package healsmart.com.conexaoenxaqueca.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import healsmart.com.conexaoenxaqueca.ExpandableHeightGridView;
import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.activities.ListaCriseMedicamentoActivity;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;

/**
 * Created by allan on 03/12/15.
 */
public class RelatorioCriseMedicamentoExpandableAdapter extends BaseExpandableListAdapter{

    private Map<Integer, List<Integer>> mListMapGroup;
    private Map<Integer, List<Medicamento>> mListMapGroupChild;
    private Activity mActivity;
    private Context mContext;

    public RelatorioCriseMedicamentoExpandableAdapter(Activity activity, Map<Integer, List<Integer>> listMapGroup, Map<Integer, List<Medicamento>> listMapGroupChild)
    {
        mListMapGroup = listMapGroup;
        mActivity = activity;
        mContext = activity.getApplicationContext();
        mListMapGroupChild = listMapGroupChild;
    }

    @Override
    public int getGroupCount() {
        return mListMapGroup.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCountSize = 0;

        if(mListMapGroupChild.get(groupPosition).size() > 0)
            childCountSize = 1;

        return childCountSize;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mListMapGroup.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mListMapGroupChild.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        ViewHolderGroup holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.expandable_item_relatorio_crise_medicamento, null);
            holder = new ViewHolderGroup();
            convertView.setTag(holder);

            holder.titleTextView = (TextView) convertView.findViewById(R.id.titleTextView);
            holder.nrCriseTextView = (TextView) convertView.findViewById(R.id.nrCriseTextView);
            holder.nrCriseComMedicamentoTextView = (TextView) convertView.findViewById(R.id.nrCriseComMedicamentoTextView);
        }
        else{
            holder = (ViewHolderGroup) convertView.getTag();
        }

        ExpandableListView expandableListView = (ExpandableListView) parent;
        expandableListView.setGroupIndicator(null);

        LinearLayout ll = ((LinearLayout) convertView);
        List<Integer> groupContent = mListMapGroup.get(groupPosition);
        List<Medicamento> childContent = mListMapGroupChild.get(groupPosition);

        int sizeGroupContent = (groupContent == null ? 0 : groupContent.size());
        if(sizeGroupContent > 0)  {
            for(int i=0; i < ll.getChildCount(); i++)
            {
                ll.getChildAt(i).setBackgroundColor(ContextCompat.getColor(mContext, R.color.verdeClaro));
            }
            holder.nrCriseTextView.setText(String.valueOf(groupContent.get(0)));
            holder.nrCriseComMedicamentoTextView.setText(String.valueOf(groupContent.get(1)));

            holder.nrCriseTextView.setTextColor(ContextCompat.getColor(mContext, R.color.brancoGelo));
            holder.nrCriseComMedicamentoTextView.setTextColor(ContextCompat.getColor(mContext, R.color.brancoGelo));
            holder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.brancoGelo));

            int sizeChildContent = (childContent == null ? 0 : childContent.size());
            if(sizeChildContent == 0)
            {
                holder.titleTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            } else {
                holder.titleTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.expandable_indicator, 0, 0, 0);
            }
        }
        else  {
            for(int i=0; i < ll.getChildCount(); i++)
            {
                ll.getChildAt(i).setBackgroundColor(ContextCompat.getColor(mContext, R.color.brancoGelo));
            }

            holder.nrCriseTextView.setText("--");
            holder.nrCriseComMedicamentoTextView.setText("---");

            holder.nrCriseTextView.setTextColor(ContextCompat.getColor(mContext, R.color.preto));
            holder.nrCriseComMedicamentoTextView.setTextColor(ContextCompat.getColor(mContext, R.color.preto));
            holder.titleTextView.setTextColor(ContextCompat.getColor(mContext, R.color.preto));
            holder.titleTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        holder.titleTextView.setText(ServiceFactory.getInstance().getServiceUtils().getNameMonth(groupPosition, mContext.getResources()));

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent)
    {
        ViewHolderChild holder;

        List<Medicamento> medicamentos = mListMapGroupChild.get(groupPosition);

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.expandable_child_item_relatorio_crise_medicamento, null);
            holder = new ViewHolderChild();
            convertView.setTag(holder);

            holder.medicamentosGridView = (ExpandableHeightGridView) convertView.findViewById(R.id.medicamentosGridView);
        }
        else{
            holder = (ViewHolderChild) convertView.getTag();
        }

        final int GROUP_POSITION = groupPosition;

        holder.medicamentosGridView.setExpanded(true);
        RelatorioCriseMedicamentoGridAdapter adapter = new RelatorioCriseMedicamentoGridAdapter(parent.getContext(), medicamentos);
        holder.medicamentosGridView.setAdapter(adapter);
        holder.medicamentosGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Medicamento medicamento = mListMapGroupChild.get(GROUP_POSITION).get(position);

                String jsonMedicamento = Medicamento.toJSON(medicamento);

                Intent intentAlterarQuestao = new Intent(mContext, ListaCriseMedicamentoActivity.class);
                intentAlterarQuestao.putExtra(RelatorioCriseMedicamentoExpandableAdapter.class.toString(), jsonMedicamento);

                mActivity.startActivity(intentAlterarQuestao);
                mActivity.overridePendingTransition(R.anim.slide_up_d500, R.anim.fade_in);
            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }

    class ViewHolderGroup {
        TextView titleTextView;
        TextView nrCriseTextView;
        TextView nrCriseComMedicamentoTextView;

    }

    class ViewHolderChild {
        ExpandableHeightGridView medicamentosGridView;
    }

    public void setContentData(Map<Integer, List<Integer>> dataParentList, Map<Integer, List<Medicamento>> dataChildList)
    {
        mListMapGroup.clear();
        mListMapGroupChild.clear();

        mListMapGroup.putAll(dataParentList);
        mListMapGroupChild.putAll(dataChildList);

        notifyDataSetChanged();
    }
}
