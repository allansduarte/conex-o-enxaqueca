package healsmart.com.conexaoenxaqueca.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.Utils.PaintDraw;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Medicamento;

/**
 * Created by allan on 04/12/15.
 */
public class RelatorioCriseMedicamentoGridAdapter extends BaseAdapter
{
    private List<Medicamento> mMedicamentos;
    private Context mContext;

    public RelatorioCriseMedicamentoGridAdapter(Context context, List<Medicamento> medicamentos)
    {
        this.mMedicamentos = medicamentos;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mMedicamentos.size();
    }

    @Override
    public Object getItem(int position) {
        return mMedicamentos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RelatorioCriseMedicamentoViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_medicamento_opcao, null);
            holder = new RelatorioCriseMedicamentoViewHolder(convertView);
            convertView.setTag(holder);
        }
        else{
            holder = (RelatorioCriseMedicamentoViewHolder) convertView.getTag();
        }

        String nomeMedicamento = ServiceFactory.getInstance().getServiceUtils().textToCapitalize(mMedicamentos.get(position).getNome());
        int quantidadeMedicamento = mMedicamentos.get(position).getQuantidade();

        if(quantidadeMedicamento > 0)
        {
            changeMedicamentoImage(holder, position);
        } else{
            holder.ivOption.setImageResource(R.drawable.ic_frasco);
        }

        holder.tvTitle.setText(nomeMedicamento);

        return convertView;
    }

    private void changeMedicamentoImage(RelatorioCriseMedicamentoViewHolder view, int position) {

        PaintDraw paintDraw = ServiceFactory.getInstance().getServicePaintDraw(mContext);
        paintDraw.setBottom(25);

        int color = R.color.verdeClaro;
        Bitmap bitmap = paintDraw.drawTextToBitmap(String.valueOf(mMedicamentos.get(position).getQuantidade()), R.drawable.ic_frasco, color);

        view.ivOption.setImageBitmap(bitmap);
    }

    class RelatorioCriseMedicamentoViewHolder {

        public ImageView ivOption;
        public TextView tvTitle;

        public RelatorioCriseMedicamentoViewHolder(View itemView) {

            ivOption = (ImageView) itemView.findViewById(R.id.icImageView);
            ivOption.setAdjustViewBounds(true);

            tvTitle = (TextView) itemView.findViewById(R.id.titleTextView);
        }
    }
}
