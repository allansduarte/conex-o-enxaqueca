package healsmart.com.conexaoenxaqueca.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.Utils.PaintDraw;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.interfaces.RecyclerViewOnClickListenerHack;

public class SurveyOptionsAdapter extends RecyclerView.Adapter<SurveyOptionsAdapter.SurveyViewHolder>
{
    private LayoutInflater mLayoutInflater;
    private RecyclerViewOnClickListenerHack mRecyclerViewOnClickListenerHack;
    private Context mContext;
    private List<String> mOptionsSelected;
    private List<String> mOptionsMedicamentoSelected;
    private List<String> mOptionsMetodoAlivioSelected;

    private List<SurveyOptions> mOptions;

    public SurveyOptionsAdapter(Context context, List<SurveyOptions> options)
    {
        this.mOptions = options;
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = context;
    }

    @Override
    public SurveyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mLayoutInflater.inflate(R.layout.item_survey_options, viewGroup, false);
        return new SurveyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SurveyViewHolder surveyViewHolder, int position) {

        int quantidadeOption = mOptions.get(position).getQuantidadeMedicamentoOption();
        String nomeOption = mOptions.get(position).getTitleOption();

        if(quantidadeOption != 0)
        {
            changeMedicamentoImage(surveyViewHolder, position);
        } else{
            surveyViewHolder.ivOption.setImageResource(mOptions.get(position).getDrawableOption());
        }

        surveyViewHolder.tvTitle.setText(nomeOption);

        boolean isSelected = false;
        if(getOptionsSelected() != null && getOptionsSelected().contains(mOptions.get(position).getId()))
        {
            isSelected = true;
            surveyViewHolder.itemView.setSelected(true);
        } else {
            surveyViewHolder.itemView.setSelected(false);
        }

        if(getOptionsMedicamentoSelected() != null && getOptionsMedicamentoSelected().contains(mOptions.get(position).getId()))
        {
            isSelected = true;
            surveyViewHolder.itemView.setSelected(true);

            mOptions.get(position).setDrawableOption(R.drawable.ic_frasco_press);
            mOptions.get(position).setColor(R.color.laranja);
            changeMedicamentoImage(surveyViewHolder, position);
        } else if(!isSelected && getOptionsMedicamentoSelected() != null && getOptionsMedicamentoSelected().contains(mOptions.get(position).getId())) {
            surveyViewHolder.itemView.setSelected(false);
            mOptions.get(position).setDrawableOption(R.drawable.ic_frasco);
            mOptions.get(position).setColor(R.color.verdeClaro);
            changeMedicamentoImage(surveyViewHolder, position);
        }

        if(getOptionsMetodoAlivioSelected() != null && getOptionsMetodoAlivioSelected().contains(mOptions.get(position).getId()))
        {
            surveyViewHolder.itemView.setSelected(true);
        } else if(!isSelected && getOptionsMetodoAlivioSelected() != null && getOptionsMetodoAlivioSelected().contains(mOptions.get(position).getId())) {
            surveyViewHolder.itemView.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return mOptions.size();
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListenerHack r){
        mRecyclerViewOnClickListenerHack = r;
    }

    public void addListItem(SurveyOptions option, int position){
        mOptions.add(position, option);
        notifyItemInserted(position);
    }


    public void removeListItem(int position){
        mOptions.remove(position);
        notifyItemRemoved(position);
    }

    public class SurveyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView ivOption;
        public TextView tvTitle;

        public SurveyViewHolder(View itemView) {
            super(itemView);

            ivOption = (ImageView) itemView.findViewById(R.id.icImageView);
            ivOption.setAdjustViewBounds(true);

            tvTitle = (TextView) itemView.findViewById(R.id.titleTextView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerViewOnClickListenerHack != null){
                mRecyclerViewOnClickListenerHack.onClickListener(v, getAdapterPosition());
            }
        }
    }

    public void changeMedicamentoImage(SurveyViewHolder view, int position)
    {
        PaintDraw paintDraw = ServiceFactory.getInstance().getServicePaintDraw(mContext);
        paintDraw.setBottom(25);

        int color = (mOptions.get(position).getColor() == 0 ? R.color.verdeClaro : mOptions.get(position).getColor());
        Bitmap bitmap = paintDraw.drawTextToBitmap(String.valueOf(mOptions.get(position).getQuantidadeMedicamentoOption()), mOptions.get(position).getDrawableOption(), color);

        view.ivOption.setImageBitmap(bitmap);
    }

    public void changeMedicamentoImage(View view, int position)
    {
        PaintDraw paintDraw = ServiceFactory.getInstance().getServicePaintDraw(mContext);
        paintDraw.setBottom(25);

        int color = (mOptions.get(position).getColor() == 0 ? R.color.verdeClaro : mOptions.get(position).getColor());
        Bitmap bitmap = paintDraw.drawTextToBitmap(String.valueOf(mOptions.get(position).getQuantidadeMedicamentoOption()), mOptions.get(position).getDrawableOption(), color);

        ImageView icImageView = (ImageView) view.findViewById(R.id.icImageView);

        icImageView.setImageBitmap(bitmap);
    }

    public List<String> getOptionsMedicamentoSelected() {
        return mOptionsMedicamentoSelected;
    }

    public void setOptionsMedicamentoSelected(List<String> mOptionsMedicamentoSelected) {
        this.mOptionsMedicamentoSelected = mOptionsMedicamentoSelected;
    }

    public List<String> getOptionsMetodoAlivioSelected() {
        return mOptionsMetodoAlivioSelected;
    }

    public void setOptionsMetodoAlivioSelected(List<String> mOptionsMetodoAlivioSelected) {
        this.mOptionsMetodoAlivioSelected = mOptionsMetodoAlivioSelected;
    }

    public void setItens(List<SurveyOptions> surveyOptions)
    {
        mOptions.clear();
        mOptions.addAll(surveyOptions);
        notifyDataSetChanged();
    }

    public List<SurveyOptions> getItens()
    {
        return mOptions;
    }

    public List<String> getOptionsSelected() {
        return mOptionsSelected;
    }

    public void setOptionsSelected(List<String> mOptionsSelected) {
        this.mOptionsSelected = mOptionsSelected;
    }
}
