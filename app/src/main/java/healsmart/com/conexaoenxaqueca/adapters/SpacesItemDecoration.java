package healsmart.com.conexaoenxaqueca.adapters;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {

    private int top;
    private int bottom;
    private int left;
    private int right;

    public SpacesItemDecoration() {
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = getLeft();
        outRect.right = getRight();
        outRect.bottom = getBottom();
        outRect.top = getTop();

        // Add top margin only for the first item to avoid double space between items
        //if(parent.getChildLayoutPosition(view) == 0)
            //outRect.top = space;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top){
        this.top = top;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }
}
