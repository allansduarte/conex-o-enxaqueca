package healsmart.com.conexaoenxaqueca.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import healsmart.com.conexaoenxaqueca.R;
import healsmart.com.conexaoenxaqueca.activities.IntensidadeDorActivity;
import healsmart.com.conexaoenxaqueca.activities.SumarioCriseActivity;
import healsmart.com.conexaoenxaqueca.core.ServiceFactory;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;

/**
 * Created by allan on 24/12/15.
 */
public class DashboardCardsAdapter extends RecyclerView.Adapter<DashboardCardsAdapter.CardsViewHolder> {

    private Context mContext;
    private Map<Integer, Object> mContentCards;

    public DashboardCardsAdapter(Context context, Map<Integer, Object> contentCards) {
        mContext = context;
        mContentCards = contentCards;
    }

    @Override
    public CardsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        Map.Entry entry = getEntry(viewType);
        int key = (int) entry.getKey();
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_dashboard_card, viewGroup, false);
        return new CardsViewHolder(view, key);
    }

    @Override
    public void onBindViewHolder(CardsViewHolder holder, int position) {

        Map.Entry entry = getEntry(position);
        int key = (int) entry.getKey();

        if(key == 0) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_card_crise_andamento, null, false);
            holder.rlMaster.addView(view);
            holder.initComponentsCardCriseEmAndamento(view);
            Crise crise = (Crise) entry.getValue();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(crise.getDataIni());

            StringBuilder subTituloTexto = holder.getSubTituloTexto(calendar);

            holder.tvSubTitulo.setText(subTituloTexto.toString());
            holder.setOnClick();
        }
        else if(key == 1) {

            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_card_tempo_sem_dor, null, false);
            holder.rlMaster.addView(view);

            long initialTimeDuration = (long) entry.getValue();
            holder.initComponentsCardTempoSemDor(view);
            holder.updateTimeDurationCard(initialTimeDuration);
        }
        else if(key == 2) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.layout_card_intensidade, null, false);
            holder.rlMaster.addView(view);
            holder.initComponentsCardIntensidade(view);

            long[] nrCriseIntensidade = (long[]) entry.getValue();
            Bitmap bitmapCriseFraca = ServiceFactory.getInstance().getServicePaintDraw(mContext).drawTextToBitmap(String.valueOf(nrCriseIntensidade[0]), R.drawable.ic_cabeca_intensidade_fraca, R.color.verdeEscuro);
            holder.ivCriseFraca.setImageBitmap(bitmapCriseFraca);

            Bitmap bitmapCriseMedia = ServiceFactory.getInstance().getServicePaintDraw(mContext).drawTextToBitmap(String.valueOf(nrCriseIntensidade[1]), R.drawable.ic_cabeca_intensidade_media, R.color.verdeEscuro);
            holder.ivCriseMedia.setImageBitmap(bitmapCriseMedia);

            Bitmap bitmapCriseForte = ServiceFactory.getInstance().getServicePaintDraw(mContext).drawTextToBitmap(String.valueOf(nrCriseIntensidade[2]), R.drawable.ic_cabeca_intensidade_forte, R.color.verdeEscuro);
            holder.ivCriseForte.setImageBitmap(bitmapCriseForte);

            Calendar calendar = Calendar.getInstance();
            int month = calendar.get(Calendar.MONTH);
            holder.tvNomeMes.setText(ServiceFactory.getInstance().getServiceUtils().getNameMonth(month, mContext.getResources()));
            holder.setOnClick();
        }
    }

    @Override
    public int getItemCount() {
        return mContentCards.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class CardsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public int viewType;

        public TextView tvTitulo;
        public TextView tvSubTitulo;
        public TextView tvNomeMes;
        public ImageView ivCriseFraca;
        public ImageView ivCriseMedia;
        public ImageView ivCriseForte;
        public RelativeLayout rlMaster;
        public CardView cvContent;

        public CardsViewHolder(View itemView, int viewType) {
            super(itemView);

            setViewType(viewType);
            rlMaster = (RelativeLayout) itemView.findViewById(R.id.masterLayout);
        }

        private void setOnClick() {
            cvContent.setOnClickListener(this);
        }

        private void initComponentsCardIntensidade(View view) {
            cvContent = (CardView) view.findViewById(R.id.contentCardView);
            tvNomeMes = (TextView) view.findViewById(R.id.nomeMesTextView);
            ivCriseFraca = (ImageView) view.findViewById(R.id.criseFracaImageView);
            ivCriseMedia = (ImageView) view.findViewById(R.id.criseMediaImageView);
            ivCriseForte = (ImageView) view.findViewById(R.id.criseForteImageView);
        }

        private void initComponentsCardTempoSemDor(View view) {
            cvContent = (CardView) view.findViewById(R.id.contentCardView);
            tvTitulo = (TextView) view.findViewById(R.id.tituloTextView);
        }

        private void initComponentsCardCriseEmAndamento(View view) {
            cvContent = (CardView) view.findViewById(R.id.contentCardView);
            tvSubTitulo = (TextView) view.findViewById(R.id.subtituloTextView);
        }

        @Override
        public void onClick(View v) {
            if(getViewType() == 0) {
                Map.Entry entry = getEntry(getViewType());
                Crise crise = (Crise) entry.getValue();

                String paramIntentMainActivity = "MainActivity";
                try {
                    Intent intentSumarioCrise = new Intent(mContext, SumarioCriseActivity.class);
                    intentSumarioCrise.putExtra(SurveyConstants.INTENT_CRISE_ID, crise.getId());
                    intentSumarioCrise.putExtra(paramIntentMainActivity, paramIntentMainActivity);

                    mContext.startActivity(intentSumarioCrise);
                } catch(IllegalStateException e) {
                    removeCardByIndex(0);
                }
            }
            else if(getViewType() == 2) {
                ServiceFactory.getInstance().getServiceUtils().redirectTo(mContext, IntensidadeDorActivity.class);
            }
        }

        private void updateTimeDurationCard(long dataIniTimeMillis) {

            if(dataIniTimeMillis == 0) {
                tvTitulo.setText("Registre sua primeira crise.");
            } else {
                long dataCurrentTimeMillis = ServiceFactory.getInstance().getServiceUtils().getCurrentDate();

                long minutoAuxiliar = (long) ((dataCurrentTimeMillis - dataIniTimeMillis) / (float) (1000*60));
                long minutos = minutoAuxiliar%60;

                long horaAuxiliar  = minutoAuxiliar/60;
                long horas  = horaAuxiliar%24;

                long diaAuxiliar = horaAuxiliar/24;
                long dias = diaAuxiliar%7;

                long semanaAuxiliar = diaAuxiliar/7;
                long semanas = semanaAuxiliar%4;

                long meses = (12*diaAuxiliar)/365;

                if(diaAuxiliar >= 365) {
                    tvTitulo.setText("Parabéns! Você está a mais de 1 ano sem dor de cabeça.");
                } else {

                    long[] dataTimeLong = {meses, semanas, dias, horas, minutos};
                    String[][] dataTimeString = {{"mês", "meses"}, {"semana", "semanas"}, {"dia", "dias"}, {"hora", "horas"}, {"minuto", "minutos"}};
                    StringBuilder texto = generateTextTimeDurationCard(dataTimeLong, dataTimeString);

                    tvTitulo.setText(texto.toString());
                }
            }
        }

        private StringBuilder generateTextTimeDurationCard(long[] times, String[][] nameTimes) {

            boolean auxTempoAdicionado = false;
            StringBuilder texto = new StringBuilder();

            for(int i=0; i < times.length; i++) {
                long time = times[i];

                if(time > 0)
                {
                    if(auxTempoAdicionado)
                        texto.append(" ");

                    texto.append(time);
                    if(time == 1) {
                        texto.append(" ");
                        texto.append(nameTimes[i][0]);
                    }
                    else {
                        texto.append(" ");
                        texto.append(nameTimes[i][1]);
                    }

                    auxTempoAdicionado = true;
                }
            }

            if(times[0] ==0 && times[1] == 0 && times[2] == 0 && times[3] == 0 && times[4] == 0)
                texto.append("0 minutos");

            return texto;
        }

        private StringBuilder getSubTituloTexto(Calendar calendar) {
            int dia = calendar.get(Calendar.DAY_OF_MONTH);
            int mes = calendar.get(Calendar.MONTH) +1;
            int ano = calendar.get(Calendar.YEAR);
            int hora = calendar.get(Calendar.HOUR_OF_DAY);
            int minuto = calendar.get(Calendar.MINUTE);

            StringBuilder texto = new StringBuilder();

            texto.append("Início: ");
            if(dia < 10)
                texto.append("0").append(dia);
            else texto.append(dia);

            texto.append("/");

            if(mes < 10)
                texto.append("0").append(mes);
            else texto.append(mes);

            texto.append("/");
            texto.append(ano);

            texto.append(" - ");

            texto.append(hora).append("h");
            texto.append(" ");
            texto.append(minuto).append("min");

            return texto;
        }

        public int getViewType() {
            return viewType;
        }

        public void setViewType(int viewType) {
            this.viewType = viewType;
        }
    }

    private void removeCardByIndex(int index) {
        mContentCards.remove(index);
        notifyItemRemoved(index);
    }

    public Map.Entry getEntry(int id)
    {
        Iterator iterator = mContentCards.entrySet().iterator();
        int n = 0;
        while(iterator.hasNext()){
            Map.Entry entry = (Map.Entry) iterator.next();
            if(n == id){
                return entry;
            }
            n ++;
        }

        return null;
    }
}