package healsmart.com.conexaoenxaqueca;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

import healsmart.com.conexaoenxaqueca.domain.Bebida;
import healsmart.com.conexaoenxaqueca.domain.Comida;
import healsmart.com.conexaoenxaqueca.domain.Consequencia;
import healsmart.com.conexaoenxaqueca.domain.Crise;
import healsmart.com.conexaoenxaqueca.domain.EsforcoFisico;
import healsmart.com.conexaoenxaqueca.domain.Gatilho;
import healsmart.com.conexaoenxaqueca.domain.Local;
import healsmart.com.conexaoenxaqueca.domain.MetodoAlivio;
import healsmart.com.conexaoenxaqueca.domain.QualidadeDor;
import healsmart.com.conexaoenxaqueca.domain.Sintoma;
import healsmart.com.conexaoenxaqueca.interfaces.BebidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.ComidaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.ConsequenciaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.CriseEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.EsforcoFisicoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.GatilhoEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.LocalEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.MetodoAlivioEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.PerfilUsuario;
import healsmart.com.conexaoenxaqueca.interfaces.QualidadeDorEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SintomaEntidade;
import healsmart.com.conexaoenxaqueca.interfaces.SurveyConstants;
import healsmart.com.conexaoenxaqueca.interfaces.UsuarioEntidade;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by allan on 05/11/15.
 */
public class SurveyOpcoesLoader  extends AsyncTaskLoader<Object>
    implements  PerfilUsuario,
                SurveyConstants
{
    private int mType;
    private String mCriseId;
    private Object mData;

    public SurveyOpcoesLoader(Context context, int type)
    {
        super(context);

        mType = type;
    }

    public SurveyOpcoesLoader(Context context, int type, String criseId) {
        super(context);

        mType = type;
        mCriseId = criseId;
    }

    @Override
    public Object loadInBackground()
    {
        Realm realm = Realm.getDefaultInstance();

        Object result = null;

        SharedPreferences preferences = getContext().getSharedPreferences(USUARIO_PREFS, Context.MODE_PRIVATE);
        String userId = preferences.getString(USUARIO_PREFS_USUARIO_ID, null);

        Crise crise;

        switch (mType)
        {
            case QUESTION_DURACAO_DOR_ID:

                if(mCriseId != null) {
                     crise = realm.where(Crise.class)
                            .equalTo(CriseEntidade.ID, mCriseId)
                            .findFirst();

                    result = Crise.copy(crise);
                }
                break;

            case QUESTION_INTENSIDADE_ID:

                crise = realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mCriseId)
                        .findFirst();

                result = (crise == null ? null : Crise.copy(crise));
                break;

            case QUESTION_MEDICAMENTO_ID:

                crise = realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mCriseId)
                        .findFirst();

                result = (crise == null ? null : Crise.copy(crise));
                break;

            case QUESTION_QUALIDADE_DOR_ID:

                RealmResults<QualidadeDor> opcoesQD = realm.where(QualidadeDor.class)
                        .equalTo(QualidadeDorEntidade.USUARIO+"."+ UsuarioEntidade.ID, userId)
                        .equalTo(QualidadeDorEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesQD != null)
                {
                    List<QualidadeDor> qualidadeDorList = new ArrayList<>();
                    for(QualidadeDor opcao : opcoesQD)
                    {
                        qualidadeDorList.add(QualidadeDor.copy(opcao));
                    }
                    result = qualidadeDorList;
                }

                break;

            case QUESTION_GATILHOS_ID:

                RealmResults<Gatilho> opcoesGatilho = realm.where(Gatilho.class)
                        .equalTo(GatilhoEntidade.USUARIO+"."+UsuarioEntidade.ID, userId)
                        .equalTo(GatilhoEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesGatilho != null)
                {
                    List<Gatilho> gatilhos = new ArrayList<>();
                    for(Gatilho gatilho : opcoesGatilho)
                    {
                        gatilhos.add(Gatilho.copy(gatilho));
                    }
                    result = gatilhos;
                }
                break;

            case QUESTION_GATILHOS_COMIDA_ID:

                RealmResults<Comida> opcoesComida = realm.where(Comida.class)
                        .equalTo(ComidaEntidade.USUARIO+"."+UsuarioEntidade.ID, userId)
                        .equalTo(ComidaEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesComida != null)
                {
                    List<Comida> comidas = new ArrayList<>();
                    for(Comida comida : opcoesComida)
                    {
                        comidas.add(Comida.copy(comida));
                    }
                    result = comidas;
                }
                break;

            case QUESTION_GATILHOS_BEBIDA_ID:

                RealmResults<Bebida> opcoesBebida = realm.where(Bebida.class)
                        .equalTo(BebidaEntidade.USUARIO+"."+UsuarioEntidade.ID, userId)
                        .equalTo(BebidaEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesBebida != null)
                {
                    List<Bebida> bebidas = new ArrayList<>();
                    for(Bebida bebida : opcoesBebida)
                    {
                        bebidas.add(Bebida.copy(bebida));
                    }
                    result = bebidas;
                }
                break;

            case QUESTION_GATILHOS_ESFORCO_FISICO_ID:

                RealmResults<EsforcoFisico> opcoesEsforcoFisico = realm.where(EsforcoFisico.class)
                        .equalTo(EsforcoFisicoEntidade.USUARIO+"."+UsuarioEntidade.ID, userId)
                        .equalTo(EsforcoFisicoEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesEsforcoFisico != null)
                {
                    List<EsforcoFisico> esforcoFisicos = new ArrayList<>();
                    for(EsforcoFisico esforcoFisico : opcoesEsforcoFisico)
                    {
                        esforcoFisicos.add(EsforcoFisico.copy(esforcoFisico));
                    }
                    result = esforcoFisicos;
                }
                break;

            case QUESTION_SINTOMAS_ID:

                RealmResults<Sintoma> opcoesSintoma = realm.where(Sintoma.class)
                        .equalTo(SintomaEntidade.USUARIO+"."+UsuarioEntidade.ID, userId)
                        .equalTo(SintomaEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesSintoma != null)
                {
                    List<Sintoma> sintomasList = new ArrayList<>();
                    for(Sintoma opcao : opcoesSintoma)
                    {
                        sintomasList.add(Sintoma.copy(opcao));
                    }
                    result = sintomasList;
                }
                break;

            case QUESTION_METODO_ALIVIO_ID:

                RealmResults<MetodoAlivio> opcoesMetodoAlivio = realm.where(MetodoAlivio.class)
                        .equalTo(MetodoAlivioEntidade.USUARIO+"."+UsuarioEntidade.ID, userId)
                        .equalTo(MetodoAlivioEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesMetodoAlivio != null)
                {
                    List<MetodoAlivio> metodosList = new ArrayList<>();
                    for(MetodoAlivio opcao : opcoesMetodoAlivio)
                    {
                        metodosList.add(MetodoAlivio.copy(opcao));
                    }
                    result = metodosList;
                }
                break;

            case QUESTION_METODO_ALIVIO_FUNCIONOU_ID:

                crise = realm.where(Crise.class)
                        .equalTo(CriseEntidade.ID, mCriseId)
                        .findFirst();

                result = (crise == null ? null : Crise.copy(crise));
                break;

            case QUESTION_CONSEQUENCIAS_ID:

                RealmResults<Consequencia> opcoesConsequencia = realm.where(Consequencia.class)
                        .equalTo(ConsequenciaEntidade.USUARIO+"."+UsuarioEntidade.ID, userId)
                        .contains(ConsequenciaEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesConsequencia != null)
                {
                    List<Consequencia> consequenciaList = new ArrayList<>();
                    for(Consequencia opcao : opcoesConsequencia)
                    {
                        consequenciaList.add(Consequencia.copy(opcao));
                    }
                    result = consequenciaList;
                }
                break;

            case QUESTION_LOCAL_ID:

                RealmResults<Local> opcoesLocal = realm.where(Local.class)
                        .equalTo(LocalEntidade.USUARIO+"."+UsuarioEntidade.ID, userId)
                        .contains(LocalEntidade.DISPLAY, QUESTION_ACTIVE)
                        .findAll();

                if(opcoesLocal != null)
                {
                    List<Local> localList = new ArrayList<>();
                    for(Local opcao : opcoesLocal)
                    {
                        localList.add(Local.copy(opcao));
                    }
                    result = localList;
                }
                break;
        }

        realm.refresh();
        realm.close();

        return result;
    }

    @Override
    public void deliverResult(Object data) {
        if (isReset()) {
            if(mData != null){
                releaseResources(mData);
                return;
            }
        }

        Object oldData = data;
        mData = data;

        if (isStarted())
            super.deliverResult(data);

        if(oldData != null && oldData != data)
            releaseResources(oldData);
    }

    @Override
    protected void onStartLoading() {
        if(mData != null)
            deliverResult(mData);

        if (takeContentChanged())
            forceLoad();
        else if(mData == null)
            forceLoad();
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        onStopLoading();

        if(mData != null)
            releaseResources(mData);
    }

    @Override
    public void onCanceled(Object data) {
        super.onCanceled(data);

        releaseResources(data);
    }

    @Override
    public void forceLoad() {
        super.forceLoad();
    }

    private void releaseResources(Object data)
    {
        mData = null;
        mCriseId = null;
    }
}
